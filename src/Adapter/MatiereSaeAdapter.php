<?php
/*
 * Copyright (c) 2021. | David Annebicque | IUT de Troyes  - All Rights Reserved
 * @file /Users/davidannebicque/htdocs/intranetV3/src/Adapter/MatiereSaeAdapter.php
 * @author davidannebicque
 * @project intranetV3
 * @lastUpdate 26/10/2021 10:36
 */

namespace App\Adapter;

use App\DTO\Matiere;
use App\DTO\MatiereCollection;
use App\DTO\Ue;
use App\Entity\ApcRessource;
use App\Entity\ApcSae;

class MatiereSaeAdapter extends AbstractMatiereAdapter implements MatiereAdapterInterface
{
    public function collection(array $matieres): MatiereCollection
    {
        $collection = new MatiereCollection();

        foreach ($matieres as $matiere) {
            $collection->add($this->single($matiere));
        }

        return $collection;
    }

    public function single(mixed $matiere): ?Matiere
    {
        if (null === $matiere) {
            return null;
        }
        $m = parent::single($matiere);
        if (null !== $m) {
            $m->id = $matiere->getId();
            $m->projetFormation = $matiere->getProjetFormation();
            $m->projetPpn = $matiere->getProjetPpn();
            $m->semestre = $matiere->getSemestre();
            $m->apc = true;

            foreach ($matiere->getCompetences() as $competence) {
                $ue = new Ue();
                $ue->ue_id = $competence->getId();
                $ue->ue_display = $competence->getNomCourt();
                //$ue->ue_coefficient = $competence->getE();
                $ue->ue_numero = (int)$competence->getCouleur()[1];
                $ue->ue_couleur = $competence->getCouleur();
                $m->tab_ues[] = $ue;
            }
        }

        return $m;
    }
}
