<?php
/*
 * Copyright (c) 2021. | David Annebicque | IUT de Troyes  - All Rights Reserved
 * @file /Users/davidannebicque/htdocs/intranetV3/src/Controller/ButMmiController.php
 * @author davidannebicque
 * @project intranetV3
 * @lastUpdate 08/10/2021 07:01
 */

namespace App\Controller;

use App\Classes\Apc\ApcCoefficient;
use App\Classes\Apc\ApcStructure;
use App\Entity\ApcRessource;
use App\Entity\ApcSae;
use App\Entity\Semestre;
use App\Repository\ApcNiveauRepository;
use App\Repository\ApcRessourceRepository;
use App\Repository\ApcSaeRepository;
use App\Repository\DiplomeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ButMmiController.
 */
#[Route(path: '/but')]
class ButMmiController extends AbstractController
{
    public function __construct(private DiplomeRepository $diplomeRepository)
    {
    }

    #[Route(path: '/', name: 'but_homepage')]
    public function homePage(): Response
    {
        return $this->render('but_mmi/index.html.twig', [
            'diplomes' => $this->diplomeRepository->findBy(['typeDiplome' => 4]),
        ]);
    }

    #[Route(path: '/{diplome}/referentiel-comptences', name: 'but_referentiel_competences')]
    public function referentielCompetences(ApcStructure $apcStructure, $diplome): Response
    {
        $diplome = $this->diplomeRepository->findOneBy(['typeDiplome' => 4, 'sigle' => strtoupper($diplome)]);
        $tParcours = $apcStructure->parcoursNiveaux($diplome);

        return $this->render('but_mmi/referentielCompetences.html.twig', [
            'diplome' => $diplome,
            'competences' => $diplome->getApcComptences(),
            'parcours' => $diplome->getApcParcours(),
            'parcoursNiveaux' => $tParcours,
        ]);
    }

    #[Route(path: '/{diplome}/recherche', name: 'but_recherche')]
    public function recherche(
        Request $request,
        ApcRessourceRepository $apcRessourceRepository,
        ApcSaeRepository $apcSaeRepository,
        $diplome
    ): Response {
        $diplome = $this->diplomeRepository->findOneBy(['typeDiplome' => 4, 'sigle' => strtoupper($diplome)]);
        $search = $request->query->get('s');
        $saes = $apcSaeRepository->search($search, $diplome);
        $ressources = $apcRessourceRepository->search($search, $diplome);

        return $this->render('but_mmi/resultats.html.twig', [
            'saes' => $saes,
            'ressources' => $ressources,
            'diplome' => $diplome,
        ]);
    }

    #[Route(path: '/{diplome}/fiche-sae/{apcSae}', name: 'but_fiche_sae')]
    public function ficheSae(ApcSae $apcSae, $diplome): Response
    {
        $diplome = $this->diplomeRepository->findOneBy(['typeDiplome' => 4, 'sigle' => strtoupper($diplome)]);

        return $this->render('but_mmi/ficheSae.html.twig', [
            'apc_sae' => $apcSae,
            'diplome' => $diplome,
        ]);
    }

    #[Route(path: '/{diplome}/sae', name: 'but_sae')]
    public function sae(ApcSaeRepository $apcSaeRepository, $diplome): Response
    {
        $diplome = $this->diplomeRepository->findOneBy(['typeDiplome' => 4, 'sigle' => strtoupper($diplome)]);

        return $this->render('but_mmi/sae.html.twig', [
            'saes' => $apcSaeRepository->findByDiplome($diplome),
            'diplome' => $diplome,
        ]);
    }

    #[Route(path: '/{diplome}/fiche-ressource/{apcRessource}', name: 'but_fiche_ressource')]
    public function ficheRessource($diplome, ApcRessource $apcRessource): Response
    {
        $diplome = $this->diplomeRepository->findOneBy(['typeDiplome' => 4, 'sigle' => strtoupper($diplome)]);

        return $this->render('but_mmi/ficheRessource.html.twig', [
            'apc_ressource' => $apcRessource,
            'diplome' => $diplome,
        ]);
    }

    #[Route(path: '/{diplome}/repartition-horaire', name: 'but_repartition_horaire')]
    public function repartitionHoraire(
        ApcRessourceRepository $apcRessourceRepository,
        ApcSaeRepository $apcSaeRepository,
        $diplome
    ): Response {
        $diplome = $this->diplomeRepository->findOneBy(['typeDiplome' => 4, 'sigle' => strtoupper($diplome)]);
        $ressources = $apcRessourceRepository->findByDiplomeToSemestreArray($diplome);
        $saes = $apcSaeRepository->findByDiplomeToSemestreArray($diplome);

        return $this->render('but_mmi/preconisations.html.twig', [
            'ressources' => $ressources,
            'saes' => $saes,
            'diplome' => $diplome,
        ]);
    }

    #[Route(path: '/{diplome}/coefficients', name: 'but_coefficients')]
    public function coefficients(
        ApcCoefficient $apcCoefficient,
        ApcRessourceRepository $apcRessourceRepository,
        ApcSaeRepository $apcSaeRepository,
        ApcNiveauRepository $apcNiveauRepository,
        $diplome
    ): Response {
        $diplome = $this->diplomeRepository->findOneBy(['typeDiplome' => 4, 'sigle' => strtoupper($diplome)]);
        $ressources = $apcRessourceRepository->findByDiplomeToSemestreArray($diplome);
        $saes = $apcSaeRepository->findByDiplomeToSemestreArray($diplome);
        $tab = [];
        foreach ($diplome->getSemestres() as $semestre) {
            $semestreid = $semestre->getId();
            $tab[$semestre->getId()] = [];
            $tab[$semestre->getId()]['niveaux'] = $apcNiveauRepository->findBySemestre($semestre);
            $tab[$semestre->getId()]['ressources'] = $ressources[$semestreid];
            $tab[$semestre->getId()]['saes'] = $saes[$semestreid];
            $tab[$semestre->getId()]['coefficients'] = $apcCoefficient->calculsCoefficients($tab[$semestreid]['saes'],
                $tab[$semestreid]['ressources']);
        }

        return $this->render('but_mmi/coefficients.html.twig', [
            'tab' => $tab,
            'diplome' => $diplome,
        ]);
    }

    #[Route(path: '/{diplome}/ressource/{semestre}', name: 'but_ressource')]
    public function ressource(ApcRessourceRepository $apcRessourceRepository, $diplome, Semestre $semestre): Response
    {
        $diplome = $this->diplomeRepository->findOneBy(['typeDiplome' => 4, 'sigle' => strtoupper($diplome)]);

        return $this->render('but_mmi/ressources.html.twig', [
            'ressources' => $apcRessourceRepository->findBySemestre($semestre),
            'semestre' => $semestre,
            'diplome' => $diplome,
        ]);
    }
}
