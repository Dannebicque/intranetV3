<?php
/*
 * Copyright (c) 2021. | David Annebicque | IUT de Troyes  - All Rights Reserved
 * @file /Users/davidannebicque/htdocs/intranetV3/src/Entity/ScolaritePromo.php
 * @author davidannebicque
 * @project intranetV3
 * @lastUpdate 06/06/2021 09:12
 */

namespace App\Entity;

use Carbon\CarbonInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ScolaritePromoRepository")
 */
class ScolaritePromo extends BaseEntity
{
    //todo: ajouter horodatage ?
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Semestre", inversedBy="scolaritePromos")
     */
    private ?Semestre $semestre;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\AnneeUniversitaire", inversedBy="scolaritePromos")
     */
    private ?AnneeUniversitaire $anneeUniversitaire;

    /**
     * @ORM\Column(type="float")
     */
    private float $min = -0.01;

    /**
     * @ORM\Column(type="float")
     */
    private float $max = -0.01;

    /**
     * @ORM\Column(type="integer")
     */
    private int $nbEtudiants = 0;

    /**
     * @ORM\Column(type="float")
     */
    private float $moyenne = -0.01;

    /**
     * @ORM\OneToMany(targetEntity=Scolarite::class, mappedBy="scolaritePromo")
     */
    private Collection $scolarites;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?CarbonInterface $datePublication;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $publie = false;

    /**
     * @ORM\Column(type="array")
     */
    private array $moyenneUes = [];

    /**
     * @ORM\Column(type="array")
     */
    private array $moyenneMatieres = [];

    public function __construct()
    {
        $this->scolarites = new ArrayCollection();
    }

    public function getSemestre(): ?Semestre
    {
        return $this->semestre;
    }

    public function setSemestre(?Semestre $semestre): self
    {
        $this->semestre = $semestre;

        return $this;
    }

    public function getAnneeUniversitaire(): ?AnneeUniversitaire
    {
        return $this->anneeUniversitaire;
    }

    public function setAnneeUniversitaire(?AnneeUniversitaire $anneeUniversitaire): self
    {
        $this->anneeUniversitaire = $anneeUniversitaire;

        return $this;
    }

    public function getMin(): ?float
    {
        return $this->min;
    }

    public function setMin(float $min): self
    {
        $this->min = $min;

        return $this;
    }

    public function getMax(): ?float
    {
        return $this->max;
    }

    public function setMax(float $max): self
    {
        $this->max = $max;

        return $this;
    }

    public function getNbEtudiants(): ?int
    {
        return $this->nbEtudiants;
    }

    public function setNbEtudiants(int $nbEtudiants): self
    {
        $this->nbEtudiants = $nbEtudiants;

        return $this;
    }

    public function getMoyenne(): ?float
    {
        return $this->moyenne;
    }

    public function setMoyenne(float $moyenne): self
    {
        $this->moyenne = $moyenne;

        return $this;
    }

    /**
     * @return Collection|Scolarite[]
     */
    public function getScolarites(): Collection
    {
        return $this->scolarites;
    }

    public function addScolarite(Scolarite $scolarite): self
    {
        if (!$this->scolarites->contains($scolarite)) {
            $this->scolarites[] = $scolarite;
            $scolarite->setScolaritePromo($this);
        }

        return $this;
    }

    public function removeScolarite(Scolarite $scolarite): self
    {
        if ($this->scolarites->contains($scolarite)) {
            $this->scolarites->removeElement($scolarite);
            // set the owning side to null (unless already changed)
            if ($scolarite->getScolaritePromo() === $this) {
                $scolarite->setScolaritePromo(null);
            }
        }

        return $this;
    }

    public function getDatePublication(): ?CarbonInterface
    {
        return $this->datePublication;
    }

    public function setDatePublication(?CarbonInterface $datePublication): self
    {
        $this->datePublication = $datePublication;

        return $this;
    }

    public function getPublie(): ?bool
    {
        return $this->publie;
    }

    public function setPublie(bool $publie): self
    {
        $this->publie = $publie;

        return $this;
    }

    public function getMoyenneUes(): ?array
    {
        return $this->moyenneUes;
    }

    public function setMoyenneUes(array $moyenneUes): self
    {
        $this->moyenneUes = $moyenneUes;

        return $this;
    }

    public function getMoyenneMatieres(): ?array
    {
        return $this->moyenneMatieres;
    }

    public function setMoyenneMatieres(array $moyenneMatieres): self
    {
        $this->moyenneMatieres = $moyenneMatieres;

        return $this;
    }
}
