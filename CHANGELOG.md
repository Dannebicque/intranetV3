# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [3.5.1](https://github.com/Dannebicque/intranetV3/compare/v3.5.0...v3.5.1) (2022-01-03)


### Features

* Planning d'altenance ([a391b03](https://github.com/Dannebicque/intranetV3/commit/a391b0347609455f09e2d6621d9cd40e8f4816ee))
* Première version des sous-commissions en live avec le BUT ([33736a6](https://github.com/Dannebicque/intranetV3/commit/33736a676787d7500d10b467506640b028f27cce))


### Bug Fixes

* Amélioration d'une requete sur les ressources/SAE ([a597b65](https://github.com/Dannebicque/intranetV3/commit/a597b655b61ddcf0834006c160c9590fe3c61912))
* demi-journée d'absence ([f7dfb8c](https://github.com/Dannebicque/intranetV3/commit/f7dfb8cc4d99c6006416356c9f228c8818653107))
* Export des documents générés depuis les modal avec stimulus ([f67ffea](https://github.com/Dannebicque/intranetV3/commit/f67ffeaebb6a469af4750c0a0146d79f8f8c8439))
* Format téléphone sur chaîne vide. ([4dbbd83](https://github.com/Dannebicque/intranetV3/commit/4dbbd83f0deb048dc5e101b31c33bbc29ce294be))
* Import des notes ([526513d](https://github.com/Dannebicque/intranetV3/commit/526513d8aa33e7c5ee78bbcee247c044d3d760b9))
* js ([3f67346](https://github.com/Dannebicque/intranetV3/commit/3f6734656733897f559bdcea1b5d1cf3526d1bdc))
* Typo sur AppExtension ([6c23429](https://github.com/Dannebicque/intranetV3/commit/6c23429766e353e7bb0838e0cd590cd55a011478))
* Typo sur AppExtension ([a6430c7](https://github.com/Dannebicque/intranetV3/commit/a6430c75704f353392118a696723fc4119632c33))

## [3.5.0](https://github.com/Dannebicque/intranetV3/compare/v3.4.10...v3.5.0) (2022-01-02)


### Features

* Nouveau composant de Breadcrumb ([03c2cba](https://github.com/Dannebicque/intranetV3/commit/03c2cba545c6a10e301ee385e9b853321e8b1ce3))


### Bug Fixes

* Affichage des bilans juste pour les permanents. ([25892ea](https://github.com/Dannebicque/intranetV3/commit/25892ea15d0b9ff7649a249d0294f45da81a1504))
* Allégement secu trop restrictive StageEtudiantController.php ([15d26ae](https://github.com/Dannebicque/intranetV3/commit/15d26aead3d44f58cc0d3b570b688994847754d7))
* Amélioration et optimisation de la page trombinoscope. Utilisation de Stimulus. ([e343aa2](https://github.com/Dannebicque/intranetV3/commit/e343aa24f686b1be12ca222b8e7372d6ebc0f216))
* Améliorations import de notes ([15b2324](https://github.com/Dannebicque/intranetV3/commit/15b2324719f4dd39aa2acf5e388a47a17d291fd5))
* ARetour ancien editjs. Nouveau pas suffisament testé ([508454a](https://github.com/Dannebicque/intranetV3/commit/508454a7595ed5c715eafe16a1e853d626b27413))
* assets ([8ab6aa6](https://github.com/Dannebicque/intranetV3/commit/8ab6aa68ee2989c90e4569a640dd1a7e27d85bfb))
* Borne et cross-origin ([4ef46e3](https://github.com/Dannebicque/intranetV3/commit/4ef46e3699b08a702cff5540f64de72434496f7d))
* Bug sauvegarde réponse questionnaires ([e23b739](https://github.com/Dannebicque/intranetV3/commit/e23b7394bdcf19de9d9067fa990dab8d6e13e358))
* Composants Tableau ([6ce3858](https://github.com/Dannebicque/intranetV3/commit/6ce3858765df28920bfaa9734cf2421ccd6fb118))
* Configuration d'un questionnaire, et modal avec validation ([6960b08](https://github.com/Dannebicque/intranetV3/commit/6960b08796653fa003112e361bb7d3e008db0bf1))
* Divers correctifs, améliorations, typos, code quality ([4c9ddf2](https://github.com/Dannebicque/intranetV3/commit/4c9ddf220af079f4d3d16c6de75884c507382aac))
* Divers correctifs, améliorations, typos, code quality ([bb7f076](https://github.com/Dannebicque/intranetV3/commit/bb7f0763aaad0b2d5a9b7060f25a3bce764877b5))
* Divers correctifs, améliorations, typos, code quality ([025514a](https://github.com/Dannebicque/intranetV3/commit/025514a038d217835fffc45552ff6310715c7025))
* Export questionnaires ([4387abf](https://github.com/Dannebicque/intranetV3/commit/4387abf69e789371731051d1fc43cfd27e2b6ae9))
* Export questionnaires ([2dcdea7](https://github.com/Dannebicque/intranetV3/commit/2dcdea7fb3595b7a4f02d9ca179f950f1236b9ff))
* Fin de questionnaire avec redirection. ([362e908](https://github.com/Dannebicque/intranetV3/commit/362e9085297a733043ffde61876aef8b4b7ecf00))
* Groupe par défaut pour l'appel ([1e8a6bd](https://github.com/Dannebicque/intranetV3/commit/1e8a6bde55f8cab41b52cccb0e999531b0c11be8))
* Groupe par défaut pour l'appel ([aab4bd2](https://github.com/Dannebicque/intranetV3/commit/aab4bd270148c3fb1d9a23a7eec408036ea055f0))
* Message RDD ([07f068b](https://github.com/Dannebicque/intranetV3/commit/07f068bdd7644d12159b36ad45c770dab5f60321))
* Nettoyage des tests ([06eb837](https://github.com/Dannebicque/intranetV3/commit/06eb837a7ae2685478e57a2b4b8b764a9cef4e11))
* Questions chainées et configurables ([1a4a90c](https://github.com/Dannebicque/intranetV3/commit/1a4a90cfbfe3acd41b4b8080c452c2b359cb8ff3))
* Questions chainées et configurables ([aba41c5](https://github.com/Dannebicque/intranetV3/commit/aba41c58524568bb9d295c5a0921b1f32947fc32))
* Questions chainées et configurables ([804beaf](https://github.com/Dannebicque/intranetV3/commit/804beafca465dc8f823460741c82615fcf0dc240))
* Suppression de l'easter-eggs ([f5116c7](https://github.com/Dannebicque/intranetV3/commit/f5116c7c7c181fb323bb878de3939f8307e8c7e6))
* Suppression des debugs ([48b1529](https://github.com/Dannebicque/intranetV3/commit/48b15291f6182f5acb1a24b5fb85cd558c1ee96e))
* Suspension de la validation automatique pour le moment ([073f058](https://github.com/Dannebicque/intranetV3/commit/073f05849c491726bcb838969e887a27fe25908e))
* Système de notification avec Stimulus ([ff4d901](https://github.com/Dannebicque/intranetV3/commit/ff4d901f0fafff0eb91bd462ba6c26d17095fa36))
* Système de notification avec Stimulus ([56ea828](https://github.com/Dannebicque/intranetV3/commit/56ea82813d548c4bdf4aac853a8c8384fae3cd22))
* traductions et typos ([7f6bd0a](https://github.com/Dannebicque/intranetV3/commit/7f6bd0a8b98a6685a413886839ddd9b49e8b0da7))
* Trombinoscope sans type de groupe ([6dda073](https://github.com/Dannebicque/intranetV3/commit/6dda073ebf4fe45059dde17afa78c99bcceb02a9))
* typos, améliorations, typage, code quality ([7bb1066](https://github.com/Dannebicque/intranetV3/commit/7bb10665f31488ff3ab99e091d0cc4b815071891))
* typos, améliorations, typage, code quality ([a1fc231](https://github.com/Dannebicque/intranetV3/commit/a1fc2310e79568fa92ddd98692a183a01112b051))
* typos, améliorations, typage, code quality ([431db7a](https://github.com/Dannebicque/intranetV3/commit/431db7a57c6657a775a315926b0593c539bc8037))
* URL de retour après le questionnaire ([5645a3f](https://github.com/Dannebicque/intranetV3/commit/5645a3fc3a29362102d37c9cb1c6d3a52a0936e2))

### [3.4.10](https://github.com/Dannebicque/intranetV3/compare/v3.4.9...v3.4.10) (2021-12-12)


### Features

* Affichage du bilan de semestre dans le profil étudiant. ([f0339a3](https://github.com/Dannebicque/intranetV3/commit/f0339a3d259722229e80c0cd5dc79b6b52ce4b60))
* Modal gérée avec Stimulus et BS5 ([1870ebe](https://github.com/Dannebicque/intranetV3/commit/1870ebe0f886980da9d1e63f680e0581b693c5eb))
* Theme noel... ([96080ba](https://github.com/Dannebicque/intranetV3/commit/96080babfcc446974010b4ff2408048982d11922))
* Utilisation des icônes définis globalements dans le tableau. ([8878a74](https://github.com/Dannebicque/intranetV3/commit/8878a749452e4bf56719f0dfb454737a2c6428cb))


### Bug Fixes

* Assets + composer ([aceb170](https://github.com/Dannebicque/intranetV3/commit/aceb170ee0e5c6db1c83e7ec415fc0b9b6851e25))
* Bouton aperçu des justificatifs absence ([d58cd38](https://github.com/Dannebicque/intranetV3/commit/d58cd38ddb0f20d53dfefc4eae564ce27da33b18))
* Config ([7efbd08](https://github.com/Dannebicque/intranetV3/commit/7efbd0874ddee1bf907e510d46c50849014af0be))
* Export RDD et pourcentage ([6e7d3a0](https://github.com/Dannebicque/intranetV3/commit/6e7d3a0c86637c5d54072b5f719cb90fdbe5414e))
* Justificatif absence, contraites trop fortes ([e067b8a](https://github.com/Dannebicque/intranetV3/commit/e067b8acd39e63b121fdd75feefc9b688f84d923))
* Justificatif absence, contraites trop fortes ([7c2ca86](https://github.com/Dannebicque/intranetV3/commit/7c2ca868110f68dcc40b71fadf23dbebea84e1af))
* Questionnaires qualités ([51e3ab4](https://github.com/Dannebicque/intranetV3/commit/51e3ab43aa75debe44339d18142ad607b50438f5))
* Security ([48a3766](https://github.com/Dannebicque/intranetV3/commit/48a3766a21c4be6cc8486a59d82e9d3d5070cfa6))
* Suppression du type de groupe. Typo. ([28e3b5f](https://github.com/Dannebicque/intranetV3/commit/28e3b5f718330af8fb7b01342fb1ced0d083d885))
* Test sur les dates de justificatif ([f9d545c](https://github.com/Dannebicque/intranetV3/commit/f9d545caa389d89850c70efd1c0327900aad1fde))

### [3.4.9](https://github.com/Dannebicque/intranetV3/compare/v3.4.8...v3.4.9) (2021-12-06)


### Bug Fixes

* Export des justificatifs d'absences ([6248c67](https://github.com/Dannebicque/intranetV3/commit/6248c67c9c68b85f0bfbba31bc27ccac81c7c3c3))
* Export données supplémentaires pour les étudiants ([161e9bb](https://github.com/Dannebicque/intranetV3/commit/161e9bb627d9536723ac703c1f2baeb399fdccd1))
* Security avec Cas ([80cf7a7](https://github.com/Dannebicque/intranetV3/commit/80cf7a71c0d9a32b9e2c6893597daa79fc044a4d))
* Security avec Cas ([7322f15](https://github.com/Dannebicque/intranetV3/commit/7322f1518de04d3226f75b275893ce6a14953a06))
* Security avec Cas ([d3adf07](https://github.com/Dannebicque/intranetV3/commit/d3adf074b156afc9131a82b3e6aa46e960bd3cf8))
* Security avec Cas ([97b9315](https://github.com/Dannebicque/intranetV3/commit/97b9315fa90bc9c265ab34455bef1de669c1d627))

### [3.4.8](https://github.com/Dannebicque/intranetV3/compare/v3.4.7...v3.4.8) (2021-12-05)


### Features

* [Questionnaire] reprise des réponses dans le questionnaire ([8c78d62](https://github.com/Dannebicque/intranetV3/commit/8c78d62ca264393600c91601048f1d35cfeaf71b))
* [RDD] Mail de confirmation ([b21f5b0](https://github.com/Dannebicque/intranetV3/commit/b21f5b05c95316bad69349fc6bfb211899130d85))
* Passage au nouveau système de sécurité de SF ([1e734fb](https://github.com/Dannebicque/intranetV3/commit/1e734fb57d753074dab8011ca7512c8ab450a4dd))
* Suppression de Toastyfy, et utilisation du toast de BS. ([a4bbd47](https://github.com/Dannebicque/intranetV3/commit/a4bbd4734394c4792072a1d2da752519f84c37aa))


### Bug Fixes

* Assets. Nettoyages des dépendances inutiles ([0d335bf](https://github.com/Dannebicque/intranetV3/commit/0d335bf5741a3aa62746f904a18bedfde77f6ae7))
* Bug sur la suppression rattrapage ou justificatig ([15d27c2](https://github.com/Dannebicque/intranetV3/commit/15d27c286a23676a4422426e6735088ede9cc1db))
* Deprecation Session et HashPassword ([e94b822](https://github.com/Dannebicque/intranetV3/commit/e94b8226fb2d16c00afc31f08ec355cf58e5d0c7))
* Email RDD ([4578020](https://github.com/Dannebicque/intranetV3/commit/4578020aa510a0f7c3e44b76c61b4def8bd77194))
* Stage étudiant ([fb58451](https://github.com/Dannebicque/intranetV3/commit/fb58451a547b14c4e7226bb724597db00c0dc656))
* Texte de fin des questionnaires ([f285579](https://github.com/Dannebicque/intranetV3/commit/f285579d0f3b6e0e70c47e05faafc709809d87b2))

### [3.4.7](https://github.com/Dannebicque/intranetV3/compare/v3.4.6...v3.4.7) (2021-11-30)


### Features

* Composant Questionnaire. Affichage des questions. Administration des questions avec le formulaire. ([b602e37](https://github.com/Dannebicque/intranetV3/commit/b602e372cdf728ce1e712d6c772c255dfb484f6a))
* Enquete et RDD en se basant sur le composant Questionnaire ([b088a7b](https://github.com/Dannebicque/intranetV3/commit/b088a7b0e955fae762e58b56950c5310df792fe9))
* Suivi de l'appel depuis l'EDT ([3d0e7b6](https://github.com/Dannebicque/intranetV3/commit/3d0e7b694c020a0a587cd44486385b68eea032e9))
* Tache cron pour l'envoi de mails de rappels dans le cadre de la démarche qualité ([c7cba7b](https://github.com/Dannebicque/intranetV3/commit/c7cba7be5c8aa1832d2cea556fca962f5ca85b68))
* Wizard avec Stimulus ([ed709d7](https://github.com/Dannebicque/intranetV3/commit/ed709d7f1aa5617c8ecee9b9ec21de1c6a729afc))


### Bug Fixes

* Correctif JS et nettoyage ([0a42e72](https://github.com/Dannebicque/intranetV3/commit/0a42e7213cd24eeaf5e164aa336afade60629a46))
* Edt et appel ([4e8d03d](https://github.com/Dannebicque/intranetV3/commit/4e8d03d8f9e8dc7f918000443dec3d03ea53753c))
* EDT Manager ([8761971](https://github.com/Dannebicque/intranetV3/commit/8761971cd4dd56a22fef206384e8571882157826))
* Enquete ([b66448e](https://github.com/Dannebicque/intranetV3/commit/b66448e9e05eb90a005d094ef30c929e46c51577))
* Problème variable autorise dans la saisie depuis Admin. ([a49d176](https://github.com/Dannebicque/intranetV3/commit/a49d17676c75a53a49b0c713ddc5c63552b11c19))
* Questionnaires ([f7bcbe4](https://github.com/Dannebicque/intranetV3/commit/f7bcbe4c9de6fcefa1b05d91e043a1b349fd1180))
* Requête rattrapage ([97cf37c](https://github.com/Dannebicque/intranetV3/commit/97cf37c37293d8ce092b5cc960d81f047dd1e6ce))
* Toast avec Stimulus ([72f3c14](https://github.com/Dannebicque/intranetV3/commit/72f3c14dc28395033712b69f382b9625c7df229c))
* Traductions ([93ffbdd](https://github.com/Dannebicque/intranetV3/commit/93ffbdd5de15c5c80bf15b41ec21b94d144ef292))

### [3.4.6](https://github.com/Dannebicque/intranetV3/compare/v3.4.5...v3.4.6) (2021-11-28)


### Features

* Ajout du numéro étudiant et du code matière/SAE/Ressource sur l'export des absences ([24e1dbb](https://github.com/Dannebicque/intranetV3/commit/24e1dbb3539e38dba333f093516590499cada68e))
* RDD formulaire de confirmation d'adresse ([fb04d5b](https://github.com/Dannebicque/intranetV3/commit/fb04d5bf62b7375d7864e17b6f2d69b3562cdbe2))


### Bug Fixes

* affichage uniquement des semestres actifs pour les salles d'examens ([ae53fb7](https://github.com/Dannebicque/intranetV3/commit/ae53fb7475bf39211b5ca44a61d4f0bec50db6c1))

### [3.4.5](https://github.com/Dannebicque/intranetV3/compare/v3.4.4...v3.4.5) (2021-11-22)


### Bug Fixes

* Fix semestre pour le justificatif d'absence ([a86290d](https://github.com/Dannebicque/intranetV3/commit/a86290d6fedeaaa89d8b7a49fae1b4cc3de1b559))
* Lien vers les images ([3d687e2](https://github.com/Dannebicque/intranetV3/commit/3d687e23ae68472de4e21ed74c6c41360e0f90b3))
* Lien vers les images ([e9e3be1](https://github.com/Dannebicque/intranetV3/commit/e9e3be1290ff8e6e2462a15b02fdcfa101bb923c))
* Lien vers les images ([b986cf5](https://github.com/Dannebicque/intranetV3/commit/b986cf5f8eb1a4d68feb76e64d6a3eb2143d0d70))

### [3.4.4](https://github.com/Dannebicque/intranetV3/compare/v3.4.3...v3.4.4) (2021-11-22)


### Features

* Ajout d'un mail lors du dépot d'un justificatif d'absence ([b8c6d42](https://github.com/Dannebicque/intranetV3/commit/b8c6d424513e9bbbc62db550f9107f0484be8065))
* Gestion des types de cours (partie gestion) à lier avec le prévsionnel ([786490a](https://github.com/Dannebicque/intranetV3/commit/786490ad198abd082f7d6dc2a1586990beb1f32a))


### Bug Fixes

* Affichage des boutons d'actions sur l'EDT correctements positionnés ([d518970](https://github.com/Dannebicque/intranetV3/commit/d5189702871269343c9016bd4cae426efc7818cd))
* Boutons sur le choix des exports ([2a151d5](https://github.com/Dannebicque/intranetV3/commit/2a151d5591e13ab9054006ca69e615a4002c813c))
* Bug timeline des projets ([e6da23a](https://github.com/Dannebicque/intranetV3/commit/e6da23a5ffe71bb97a8c0409b561c5ba2f84f58e))
* Correctif avatar sur le trombinoscope + CSS ([f0a3af9](https://github.com/Dannebicque/intranetV3/commit/f0a3af9ec00eda6b8fc6d5bf01f6a2f178952597))
* Correction des fixtures avec le nouvel encoder de mot de passe ([64bee77](https://github.com/Dannebicque/intranetV3/commit/64bee7763992aa3ddc5f5edafec674bf2d658427))
* CSS et lisibilité des formulaires ([d5d3f11](https://github.com/Dannebicque/intranetV3/commit/d5d3f11cad43fd58202bae0c9c69bf82eb0f14ac))
* datafixtures pour configuration ([4a96703](https://github.com/Dannebicque/intranetV3/commit/4a96703b2697bd76086c122d96651693f59b3074))
* Desactive l'analyse de la structure JS en production ([c34164c](https://github.com/Dannebicque/intranetV3/commit/c34164c29663e428fd0f8197a1aa4c85080c1ace))
* Formulaire de dépot des justificatifs ([b0766be](https://github.com/Dannebicque/intranetV3/commit/b0766be20890af9c79818149c4bfea624de04e15))
* Lien vers les images ([54a9700](https://github.com/Dannebicque/intranetV3/commit/54a970026c64c7ffb64179ab96962076dd301f89))
* Mise à jour des repository pour la CS ([919c9dd](https://github.com/Dannebicque/intranetV3/commit/919c9ddd320760fe7487f51572f8adc0befa7dbd))
* Modification des scripts pour Celcat au format Windows ([1bf324e](https://github.com/Dannebicque/intranetV3/commit/1bf324ecdf84926de935384df2df56790e9c75e7))
* Optimisations requetes messagerie ([240eebd](https://github.com/Dannebicque/intranetV3/commit/240eebdd48aba7e6c1871727605f43e812afb2d1))
* Page d'aide sur la saisie des absences ([d13c9b6](https://github.com/Dannebicque/intranetV3/commit/d13c9b6d325162af414afc425d2e562e123ecbf0))
* Taille de la borne contrainte en hauteur. Police adaptée, affichage optimisé ([48e36ac](https://github.com/Dannebicque/intranetV3/commit/48e36ac163755e0b8d00311eb6fd897711154f9e))
* Traduction des formulaires pour les projets étudiants ([9db5d0a](https://github.com/Dannebicque/intranetV3/commit/9db5d0acf004c3d6e0b3c878ce85428538c67128))
* Traduction des formulaires pour les projets étudiants ([14f0a01](https://github.com/Dannebicque/intranetV3/commit/14f0a016a287418ec9cae38ad941f3b6c4007f2c))
* Traduction des tableaux ([6b10df2](https://github.com/Dannebicque/intranetV3/commit/6b10df260eabc82b90da278a52d9cdfa4ac49433))
* Traduction sur les formulaires ([fb0fc68](https://github.com/Dannebicque/intranetV3/commit/fb0fc684bb81f5dc847ce13acd0b422cfbdf7cb1))
* traductions ([3cfe18e](https://github.com/Dannebicque/intranetV3/commit/3cfe18eaa318b8fbbecf1d04428f0c7ec4c5ce50))
* Traductions ([fb05577](https://github.com/Dannebicque/intranetV3/commit/fb055771b018987c263744b9c9de950a1ade0870))

### [3.4.3](https://github.com/Dannebicque/intranetV3/compare/v3.4.2...v3.4.3) (2021-11-18)


### Bug Fixes

* bug JS avec forEach ([5f07bbe](https://github.com/Dannebicque/intranetV3/commit/5f07bbe6b902116fb0e61d9ab2faee0925444178))
* Commande pour RCC ([310bcb3](https://github.com/Dannebicque/intranetV3/commit/310bcb3fa4a157b8297a307316797e0909bd8f4c))
* Erreur dans la requête ([606cab0](https://github.com/Dannebicque/intranetV3/commit/606cab0d7833b88ab6bd3eb792ae9ef7335e2fcc))
* Suppression des Warning sur les 403 pour bugsnag.yaml ([11923bf](https://github.com/Dannebicque/intranetV3/commit/11923bf0a73868e757853eece38cd5b98d6bbc50))
* Validation du formulaire de justificatif d'absence avec message d'erreur. ([465172d](https://github.com/Dannebicque/intranetV3/commit/465172da306b5ebfa7ba992daadb47887d28d094))

### [3.4.2](https://github.com/Dannebicque/intranetV3/compare/v3.4.1...v3.4.2) (2021-11-16)


### Bug Fixes

* Affichage du texte si pas de matière ([fb288e2](https://github.com/Dannebicque/intranetV3/commit/fb288e2f5e9eb80c3c1b4c424aea70a73a06443b))
* Bugs de CSS suite migration de BS5 ([e909023](https://github.com/Dannebicque/intranetV3/commit/e909023b3bc65a5ddcc8c0df65b9501a0643fb0f))
* Bugs de CSS suite migration de BS5 ([432d796](https://github.com/Dannebicque/intranetV3/commit/432d796c11821085d927a48ac143ad9cdcf3fe72))
* Bugs de CSS suite migration de BS5 ([f8f5a11](https://github.com/Dannebicque/intranetV3/commit/f8f5a114a8bafa05aaf1b33aac9c06949a5d3f4d))
* Dépendances JS ([32357ac](https://github.com/Dannebicque/intranetV3/commit/32357ac33d2964ecf12e54d60caf016c08764d4d))
* Dépendances JS ([809f4ac](https://github.com/Dannebicque/intranetV3/commit/809f4ac788d09d22c1837c337c2df6186701536c))
* Export des absences (format dégradé) avec les SAE/Ressource. A améliorer. ([6e8a3da](https://github.com/Dannebicque/intranetV3/commit/6e8a3da3773b666a1b22d5e956565539c71d278d))
* Export des absences (format dégradé) avec les SAE/Ressource. A améliorer. ([5342358](https://github.com/Dannebicque/intranetV3/commit/53423589acfcfe6098f0f00287b294dc4b74c3be))
* Export EDT S3 et S4D ([bf24683](https://github.com/Dannebicque/intranetV3/commit/bf246832344fecec646f4eb32e24684c4b694f54))
* Fiches suivi alternance. Problème de mise en page, ajout d'une option non applicable. Traductions. ([691b37c](https://github.com/Dannebicque/intranetV3/commit/691b37cf8df37ae489b049bad7c1a18d15b5a128))
* Petites améliorations de mise en page ([7a96c6a](https://github.com/Dannebicque/intranetV3/commit/7a96c6a86af4d81c2f23aa9ee797e4d9ee49c9cc))
* Refonte CSS des avatars ([513db4e](https://github.com/Dannebicque/intranetV3/commit/513db4e1817b7013455a046e0bf3ebfe2e2ca01f))
* Refonte CSS des avatars ([2d4ca51](https://github.com/Dannebicque/intranetV3/commit/2d4ca5101823ce21c713c85cf03ffd97755b1885))

### [3.4.1](https://github.com/Dannebicque/intranetV3/compare/v3.4.0...v3.4.1) (2021-11-11)


### Bug Fixes

* Adresse parentale optionnelle ([14ab976](https://github.com/Dannebicque/intranetV3/commit/14ab97665e821d134a20d4cb77c4712dc7b2b66b))
* En tête des tableaux ([1018410](https://github.com/Dannebicque/intranetV3/commit/1018410b38c8677fa24c7256ec9ddc465ed4da7e))
* Lien vers la modification de ses données dans le profil. ([adc782f](https://github.com/Dannebicque/intranetV3/commit/adc782f5007580a37f63863d998d808c3affe48c))
* Mise en page de la page de résumé du stage ([5365ab0](https://github.com/Dannebicque/intranetV3/commit/5365ab058ffe726b009949bf22fea962a8d8f4fc))
* Texte vide si pas de matière. ([d58cf4c](https://github.com/Dannebicque/intranetV3/commit/d58cf4c75437a86f6841d1015079839def527ed9))
* Typo ([40a85d5](https://github.com/Dannebicque/intranetV3/commit/40a85d5cb8c9657a40d48fc75cfec9c38a315d33))

## [3.4.0](https://github.com/Dannebicque/intranetV3/compare/v3.3.4...v3.4.0) (2021-11-06)


### Features

* Affichage des articles, dates et documents en fonction du destinaitaire. Un personnel peut tout voir, un étudiant juste ses éléments. ([121d068](https://github.com/Dannebicque/intranetV3/commit/121d068e5e837ed4e2539b4ef97558d65a4e3e11))
* Ajout d'un filtre par semestre actif ([7dd3203](https://github.com/Dannebicque/intranetV3/commit/7dd3203ca4366e3f9186ad3da5dc6f0631f54f36))
* Ajout des tags sur les questions ([259927c](https://github.com/Dannebicque/intranetV3/commit/259927c33e8089b3194212efdf784826aecddb3e))
* Ajout du type de destinataire (personnel ou etudiant) sur les dates, articles et documents. Modification des formulaires et des tableaux d'admin. (pas fait: affichage.) ([5c7041a](https://github.com/Dannebicque/intranetV3/commit/5c7041aca6e7f5d40fae13a511976a7d4c58e7ce))
* Commande de synchro Celcat->EDT avec récupération des modifications d'une journée. ([696e248](https://github.com/Dannebicque/intranetV3/commit/696e248ffdbf8db7f15bc286f6001da448a5f834))
* Composant Questionnaire. Ajout des types de sections, divers correctifs ([65b36e5](https://github.com/Dannebicque/intranetV3/commit/65b36e5319df6eb03e733af24d854462fdb4a86f))
* Composant Questionnaire. Traitement des différentes questions, des sections paramètrables, nombreux fix. ([6eba3d8](https://github.com/Dannebicque/intranetV3/commit/6eba3d8ce974398776faf0cdcdb1b05b5b868515))
* Composant Stimulus pour gérer les collections Symfony ([391f57a](https://github.com/Dannebicque/intranetV3/commit/391f57adb49b807c39fcdfd563dbeda79ad3109f))
* Export personnalisé (sur le modèle de "par matière") dans la partie trombinoscope ([aa71915](https://github.com/Dannebicque/intranetV3/commit/aa719150e5b76b929308e08af868d27dadd3d5e9))
* Gestion de la synchro Celcat et de l'option au niveau du diplôme plutôt que du département. ([0e4cec6](https://github.com/Dannebicque/intranetV3/commit/0e4cec68eb94384ea097725173badd810bbc414a))
* Gestion des sections et des réponses pour les questionnaires ([ec62750](https://github.com/Dannebicque/intranetV3/commit/ec62750b30188c17dc5e0d85c4049ee54c9d6b49))
* Javascript et stimulus pour la création de questionnaire çou de section ([9cc8ced](https://github.com/Dannebicque/intranetV3/commit/9cc8cedc0c6a4f993ab1df9ab2531ea2722fc39c))
* Mails automatiques à j-1 et j-2 pour les questionnaires qualités ([bc3cc7e](https://github.com/Dannebicque/intranetV3/commit/bc3cc7e80fd06c506c9d85731c835b1aaa8bdaa2))
* Refonte partie questionnaire (gestion des questions, section, et questionnaire) + modification enquête ([05658ed](https://github.com/Dannebicque/intranetV3/commit/05658eda01bf6c599ffa6e951569b7dcf972ec4e))
* Script de mise à jour de la structure d'absence, rattrapage et absence justificatif avec le semestre. ([e034fee](https://github.com/Dannebicque/intranetV3/commit/e034feee5260bc1bb38c4b4b77dc2f16cdf3e1f4))
* Stimulus Wizard. ([08e585d](https://github.com/Dannebicque/intranetV3/commit/08e585d26544333dfcc77a98785d901653afe139))


### Bug Fixes

* Correctifs sur les composants Twig (icone, typo) ([bacc12d](https://github.com/Dannebicque/intranetV3/commit/bacc12d54853a145733e68b68979d9980a362172))
* Divers correctifs et nettoyage de code ([a3c7378](https://github.com/Dannebicque/intranetV3/commit/a3c73780600d63a7c49ea1036829d61c9923dccc))
* Export script celcat ([bb9c373](https://github.com/Dannebicque/intranetV3/commit/bb9c373c28fb159f1c62e5ebd78d8136f5d3971b))
* Fichiers de configuration ([671df68](https://github.com/Dannebicque/intranetV3/commit/671df68c6e1ab697ef60527e321f134f3843acdd))
* Icone de police FA6.Pro ([a377856](https://github.com/Dannebicque/intranetV3/commit/a37785617b14d090db6fe6a0780ab85f820d7fb4))
* Liens pour l'aide ([900330f](https://github.com/Dannebicque/intranetV3/commit/900330f7b300c2252d338383cccb83d5043e1324))
* Récupération des absences sur les demandes de rattrapage ([3ba22db](https://github.com/Dannebicque/intranetV3/commit/3ba22dbd9fc32a2928c9c46f1f38a60d9dca49b0))
* Sauvegarde du semestre dans l'absence, pour ne pas dépendre de l'étudiant. Et l'année universitaire en fonction de l'utilisateur connecté ([d8483a5](https://github.com/Dannebicque/intranetV3/commit/d8483a54f6093da04e642a4ae023c6211d6af54f))
* Sauvegarde du semestre dans la demande de rattrapage, pour ne pas dépendre de l'étudiant. ([759ea00](https://github.com/Dannebicque/intranetV3/commit/759ea00090c6d7bd123495a000bc40da20743ce3))
* Traductions ([a61b2d7](https://github.com/Dannebicque/intranetV3/commit/a61b2d7eeda069d819c2d3843ce8f56bd3164a92))
* Traductions ([a7575bd](https://github.com/Dannebicque/intranetV3/commit/a7575bd757f4d4fdffa465863cecb78793a0e197))

### [3.3.4](https://github.com/Dannebicque/intranetV3/compare/v3.3.3...v3.3.4) (2021-11-01)


### Features

* Affichages des compétences dans les MCC des étudiants ([2925148](https://github.com/Dannebicque/intranetV3/commit/29251480786047f143681c641f900c8223e2145f))
* Ajout du code matière dans le DTO d'EDT pour affichage sur les bornes ([3a20e16](https://github.com/Dannebicque/intranetV3/commit/3a20e1634e1195de8705d9dcb43beebd17d54df3))
* Refonte de la page messagerie pour les étudiants ([44f4a3c](https://github.com/Dannebicque/intranetV3/commit/44f4a3c9299c456736d67cb2387282712bca00f8))


### Bug Fixes

* Amélioration du voter, lorsque plusieurs rôles cochés. ([ba5c25b](https://github.com/Dannebicque/intranetV3/commit/ba5c25be0fab4e5c784c1f61eadc7d9237dd6bce))
* Bouton ajout enseignant supprimé par erreur ([7016390](https://github.com/Dannebicque/intranetV3/commit/7016390e04b4c1a62729861eccd9e126167e8740))

### [3.3.3](https://github.com/Dannebicque/intranetV3/compare/v3.3.2...v3.3.3) (2021-10-25)


### Features

* Personnalisation des icônes pour des events ([6dbf4d9](https://github.com/Dannebicque/intranetV3/commit/6dbf4d9e7f7c1aa8fe63962e59873f7179c5eaab))
* Personnalisation des icônes pour des events ([8beb95a](https://github.com/Dannebicque/intranetV3/commit/8beb95aabd5931772f57becbe42fbe1390fbdcea))


### Bug Fixes

* [BS5] Correctif badge ([c0f1916](https://github.com/Dannebicque/intranetV3/commit/c0f19168db860c926d8dca7d3628f0d22d025e4e))
* Avenant de stage et date vide ([83b187a](https://github.com/Dannebicque/intranetV3/commit/83b187a307865dc2df523df26b311622dc5765fb))
* Colonnes dans l'export de toutes les notes ([786b230](https://github.com/Dannebicque/intranetV3/commit/786b2309646fb6dd369a0ee10675ae2eb8237760))
* Gestion des étudiants (semestre/département) en super admin ([bb8d21a](https://github.com/Dannebicque/intranetV3/commit/bb8d21af68e2e009f85531857f187f15fd6744e3))
* Nouvelles bornes ([16ff126](https://github.com/Dannebicque/intranetV3/commit/16ff12674ee16b82407d17d3099ac3020f411f86))
* Retour ancienne page de liste des absences ([7ea0760](https://github.com/Dannebicque/intranetV3/commit/7ea076012b69a433de171c2824291a994c68a71f))
* Retour anciennes bornes ([184ffec](https://github.com/Dannebicque/intranetV3/commit/184ffecc0f1bc65ca3f48b06b1b17e9fc851f300))
* Script ClickClick ([f8a7502](https://github.com/Dannebicque/intranetV3/commit/f8a75021ca3fc1f2e5c4479879f6da2501fe944c))
* selection du groupe lors de la saisie d'une absence depuis l'EDT ([08ec200](https://github.com/Dannebicque/intranetV3/commit/08ec200c2fd59697ccdd68a0f8b933f831d1f71b))
* Type destinataire sur la création d'un document ([6e81e45](https://github.com/Dannebicque/intranetV3/commit/6e81e455237c789bc1c0e167e87883fd57023f5b))
* Typo dans le role stage et projet ([d662ee3](https://github.com/Dannebicque/intranetV3/commit/d662ee33b3f7f3ae226c39347b8d6e8470e9afa1))

### [3.3.2](https://github.com/Dannebicque/intranetV3/compare/v3.3.1...v3.3.2) (2021-10-25)


### Bug Fixes

* Correctifs mineurs ([fae3a7d](https://github.com/Dannebicque/intranetV3/commit/fae3a7d0deba67356b827459ae23507607ed1b85))
* Typage des notes problématique ([e988095](https://github.com/Dannebicque/intranetV3/commit/e98809553eecdd207129bd9a4dc781963c81b166))
* Upload des photos. ([e85830b](https://github.com/Dannebicque/intranetV3/commit/e85830b0b264cdb7fa5603c876ed7e589ed95874))

### [3.3.1](https://github.com/Dannebicque/intranetV3/compare/v3.3.0...v3.3.1) (2021-10-25)


### Features

* Message d'information si l'utilisateur n'est pas sur la bonne année universitaire. ([d42ae4a](https://github.com/Dannebicque/intranetV3/commit/d42ae4a7d21f3da0d15da24fe70aa8e3c9d88e9b))


### Bug Fixes

* [BS5] Badge pour les dates ([93a97a6](https://github.com/Dannebicque/intranetV3/commit/93a97a6d50cd2571152af34e9747ef0729fc3e99))
* [BS5] correctifs CSS ([52e9705](https://github.com/Dannebicque/intranetV3/commit/52e970598f463df62c3b3c7a1b56b97fb6bec68c))
* [BS5] Fix mise en page absences ([a551580](https://github.com/Dannebicque/intranetV3/commit/a5515803bf2ced67699b0bb52b0ffab00ffcd2ed))
* [BS5] Hauteurs des boutons ([0e3fc2b](https://github.com/Dannebicque/intranetV3/commit/0e3fc2b6c770727fc16adcf2b21ace9fb7c29358))
* [BS5] Menu "grid" ([9728435](https://github.com/Dannebicque/intranetV3/commit/9728435ebdca0bb63b5708d4a6a4210c822ff36d))
* [BS5] Menu "grid" et menu EDT ([2a7b0c4](https://github.com/Dannebicque/intranetV3/commit/2a7b0c488837151d6ff4ffffd47185d25f800680))
* [SF-UX] Stimulus réparé en attendant la publication officielle ([25c0bc4](https://github.com/Dannebicque/intranetV3/commit/25c0bc444dec79f0e770b07ffa2648f96705ca23))
* Amélioration du fil d'Ariane ([c0faa1b](https://github.com/Dannebicque/intranetV3/commit/c0faa1b9f91e37781482a89f89507dd3c2574db2))
* Correctifs mineurs ([fef83bf](https://github.com/Dannebicque/intranetV3/commit/fef83bf69c28cfad285664c1ad251863f0be0b18))
* L'nnée universitaire état en "dur" dans les conventions de stage ([c3b1caf](https://github.com/Dannebicque/intranetV3/commit/c3b1cafa657e8eeb4b3b831d467f52d93c74caa1))
* Mise à jour des liens vers la documentation ([db1eb5e](https://github.com/Dannebicque/intranetV3/commit/db1eb5ee4fe217f1a9fbb9e26b7aad47a286c363))

## [3.3.0](https://github.com/Dannebicque/intranetV3/compare/v3.2.1...v3.3.0) (2021-10-24)


### Features

* A11Y Amélioration de l'accessibilité avec focus sur les formulaire plus lisible ([d3fe415](https://github.com/Dannebicque/intranetV3/commit/d3fe41551834975066647d5b27b262e215d11027))
* Ajout d'un fil d'ariane ([5cd62b7](https://github.com/Dannebicque/intranetV3/commit/5cd62b747111c9d2593fbdb3c8438aac348fe33e))
* Amélioration des relations avec certaines tables pour faciliter les requêtes et manipulations avec le BUT (SAE/Ressources/Matires) et ne pas dépendre de la position de l'étudiant, mais bien du semestre associé. ([929ceb5](https://github.com/Dannebicque/intranetV3/commit/929ceb5bd3e635b87ee23efb3a134de88a1ff5b1))
* Composant Exporter ([b5381ae](https://github.com/Dannebicque/intranetV3/commit/b5381ae38d0e983325ddc6674daa3fdfae732a7a))
* Composant questionnaire + création en super Admin ([4c730f1](https://github.com/Dannebicque/intranetV3/commit/4c730f1a52fd23e35e5cbaa6dda074a0e8f06215))
* Composant Table sur les HRS ([2afe995](https://github.com/Dannebicque/intranetV3/commit/2afe995a08cd03b5bb9b0c710e9cc34cac430488))
* Début du table pour les justificatifs d'absence. ([7546aae](https://github.com/Dannebicque/intranetV3/commit/7546aaed4abf2fe9c5f0dc72fd745e6d09152cf2))
* Détection de déconnexion Ajax ([37edc41](https://github.com/Dannebicque/intranetV3/commit/37edc41e32e5cf19cdb546e2c65afdfff95c3606))
* Documentation BDD ([2875e00](https://github.com/Dannebicque/intranetV3/commit/2875e00d2c0964cc849cab46a956cdb53dbb10ba))
* Nouvelle structure pour la partie création des quesitonnaires. Mise en place des fichiers controller/classe/CRUD ([057ccf6](https://github.com/Dannebicque/intranetV3/commit/057ccf64580b5f93269839c784b7258d77e7f559))
* Possibilité d'ajouter un justificatif d'absence depuis l'administration ([722ce8d](https://github.com/Dannebicque/intranetV3/commit/722ce8da1c0e8ada075a43b23822b7d9ee594068))
* Refresh de la page après changement d'année universitaire ([92e2f6e](https://github.com/Dannebicque/intranetV3/commit/92e2f6eba10161f254f4f6178f13b88b23912b9b))
* Table pour les personnels + refonte de la mise en page ([bd50e56](https://github.com/Dannebicque/intranetV3/commit/bd50e56193da989c7352cd9ab4a5a14497ee8c00))


### Bug Fixes

* [BS5] Btn-block sur les pages administration, semestre et administratif ([565a14b](https://github.com/Dannebicque/intranetV3/commit/565a14b9c231d118889d4ea8dc9ac881c00c6c2c))
* [BS5] Correction de l'usage de tooltip ([4d2324f](https://github.com/Dannebicque/intranetV3/commit/4d2324fefe2944297d04bfa18ab191d4eae73359))
* [BS5] Correction de l'usage des dropdown et tab ([cbc4a13](https://github.com/Dannebicque/intranetV3/commit/cbc4a1311cd4a4bfd10a43a948d2b567eea4a84d))
* [BS5] Formulaire de login et suppression de dépendance à matérial pour le float ([d6287c0](https://github.com/Dannebicque/intranetV3/commit/d6287c0c65a7423c35ab3141b21cfc41de087e63))
* [BS5] Modification pour s'adapter à BS5 ([3eb8abd](https://github.com/Dannebicque/intranetV3/commit/3eb8abda99d24bd4a92d72427cac5d50f4e87758))
* a11y de la liste du nombres d'éléments par page ([4b0caeb](https://github.com/Dannebicque/intranetV3/commit/4b0caeb425e7b4f97e967a2df8405d469bec4118))
* Amélioration du formulaire de dépot d'un justificatif d'absence ([e044894](https://github.com/Dannebicque/intranetV3/commit/e04489404ed15741d605e787209091c72ffe2b56))
* BS5 corrections de template ([07811bd](https://github.com/Dannebicque/intranetV3/commit/07811bd6e68f885a40eb14ed013d700b5a4ff4bc))
* Comparaison du prévisionnel avec l'EDT ([4287f0d](https://github.com/Dannebicque/intranetV3/commit/4287f0dbde7e5459ac9ebf60db4d22a343855ea9))
* Composant toolbar ([f91b5c2](https://github.com/Dannebicque/intranetV3/commit/f91b5c2027d5099ac71fdf4cf91780b8f5387dd8))
* Configuration, traductions manquantes, typos CSS ([81d622d](https://github.com/Dannebicque/intranetV3/commit/81d622df3637dafd8f0ff163af2b278393a8b26b))
* Correctifs de mise en page + filtre sur les groupes étudiants ([3629616](https://github.com/Dannebicque/intranetV3/commit/3629616813cf04c133d561bcfdd1eeb20dca8a0b))
* Correctifs de mise en page + filtre sur les groupes étudiants ([6a1505a](https://github.com/Dannebicque/intranetV3/commit/6a1505ad7e691016d5a016af465b8634184ee133))
* Dépendances + PHPStan ([54e77c2](https://github.com/Dannebicque/intranetV3/commit/54e77c262a8aad3ac348309848315179b2edd5e6))
* Filtre sur les tableaux avec le composant table en JS ([0d09c12](https://github.com/Dannebicque/intranetV3/commit/0d09c120882d05e4cfb723f1f2d8d48f71cd8697))
* gitignore ([2a3535f](https://github.com/Dannebicque/intranetV3/commit/2a3535f39fd84d355b093529a99734598f6ca119))
* Mise à jour du script de déploiement pour générer les assets avec yarn ([cec5766](https://github.com/Dannebicque/intranetV3/commit/cec5766256988705b7ccd6bf74fcf4ee8d6e1409))
* mise en page BS5 + nettoyage de code ([0640aa8](https://github.com/Dannebicque/intranetV3/commit/0640aa8d5f7a69409fa3634729036e772c59977d))
* Modal avec BS5. Suppression de la dépendance à MatérialJs ([b4b996e](https://github.com/Dannebicque/intranetV3/commit/b4b996ebfd84740c24b455bf39dfcc5d28294366))
* Navigation avec BS5 ([22fb272](https://github.com/Dannebicque/intranetV3/commit/22fb272b4c3b958475ca73e4e73295fecb86749c))
* Ouverture des droits, trop restrictifs ([b9cadb0](https://github.com/Dannebicque/intranetV3/commit/b9cadb05f80341451b35e40cddfe1efe1aa26574))
* Ouverture des droits, trop restrictifs ([2552b42](https://github.com/Dannebicque/intranetV3/commit/2552b420a85f77a365547f492fb57b1ee7a4762a))
* Ouverture des droits, trop restrictifs et type dans un rôle ([0a1b2b6](https://github.com/Dannebicque/intranetV3/commit/0a1b2b6869f6e19b0a505c89dc5b4537f798f75b))
* remove dépendance inutilisée ([34ee7d9](https://github.com/Dannebicque/intranetV3/commit/34ee7d97994ff97471363e08e47d0d398db1d37e))
* remove fichiers configs inutiles ([ca9ade7](https://github.com/Dannebicque/intranetV3/commit/ca9ade7a5ce08602dcfad2fb680ee0f668899f30))
* Suppression de build sur git ([1ad4378](https://github.com/Dannebicque/intranetV3/commit/1ad43787294e7e18b01710ab79408292a25b81ee))
* Tentative d'utiliser Stimulus, mais erreur... A suivre. ([cbeb1c0](https://github.com/Dannebicque/intranetV3/commit/cbeb1c064d9c942a3fdb600151565900ad6fe641))

### [3.2.1](https://github.com/Dannebicque/intranetV3/compare/v3.2.0...v3.2.1) (2021-10-08)


### Features

* AbsenceNoteVoter pour la sécurité des app Personnels en fonction du prévisionnel ([b1eecf4](https://github.com/Dannebicque/intranetV3/commit/b1eecf4d45637b0d450b3bfe8e8547be968c3031))


### Bug Fixes

* AbstractVoter et typage ([063c353](https://github.com/Dannebicque/intranetV3/commit/063c353ad3bf6749574a4d3d174f5f6b1827dcdd))
* AbstractVoter et typage ([c0b3883](https://github.com/Dannebicque/intranetV3/commit/c0b3883cda5c7410ac31e196fce76b9ec7ffbbad))
* AbstractVoter et typage ([7b75028](https://github.com/Dannebicque/intranetV3/commit/7b75028d706cc82d1817e9561e15c818aec735bc))
* AbstractVoter exclusion du cas étudiant ([067f651](https://github.com/Dannebicque/intranetV3/commit/067f651a0509fc8eb9c14589418b6a7e92a64e45))
* Corrections CSS suite mise à jour FA6 ([4ce5c19](https://github.com/Dannebicque/intranetV3/commit/4ce5c1976041ab223dc10ca0ed4c0fa52db5c3ad))
* Style sur MyEdit avec FA6 ([b6d0860](https://github.com/Dannebicque/intranetV3/commit/b6d086061f33b6be2ec600f40102be3902793dac))

## [3.2.0](https://github.com/Dannebicque/intranetV3/compare/v3.1.29...v3.2.0) (2021-10-07)


### Features

* Affichage du bouton "Appel réalisé, aucun absent" En haut et en bas de la page avec nombre d'étudiants ([cb61bd5](https://github.com/Dannebicque/intranetV3/commit/cb61bd59f09168c4e5a000f5700b8c0c0ff28ce1))
* Modification d'un étudiant depuis Super Admin ([32a3223](https://github.com/Dannebicque/intranetV3/commit/32a32231848e9fbe4c0965a686169a0551a023eb))
* Nombre de jour pour justifier une absence en paramètre ([d1e4d45](https://github.com/Dannebicque/intranetV3/commit/d1e4d452f56b86a65430f0b7850d58822d251069))
* Page d'erreur 403 ([4c6cb2b](https://github.com/Dannebicque/intranetV3/commit/4c6cb2bd85326b9bc7b267605a32449f94938f05))


### Bug Fixes

* Affichage matière complet dans profil. ([898adeb](https://github.com/Dannebicque/intranetV3/commit/898adeb56f5195defb9085bfd4eb5fb6c9c763f9))
* Améliorations de sécurités sur les accès de la partie administration avec les départements. ([3c2784d](https://github.com/Dannebicque/intranetV3/commit/3c2784d94ffcb7492efa3bcb1c5d1f2eeffa12f0))
* Année universitaire par défaut lors de la création d'un nouveau personnel. ([15330e2](https://github.com/Dannebicque/intranetV3/commit/15330e2a73aa0ee05160927703069d96b6d6b96c))
* Build ([2b5d79c](https://github.com/Dannebicque/intranetV3/commit/2b5d79cdd4820d6d3f307c70aeed79fbca89160b))
* Cohérence du département si changement de semestre ([7ae191c](https://github.com/Dannebicque/intranetV3/commit/7ae191ca83b07ecdd8cacd97a11a8fee4442cc49))
* Correctifs sur la base + Toast + Meta ([6edf273](https://github.com/Dannebicque/intranetV3/commit/6edf273557927bbbceb40bcecda6104c36b888b7))
* majoration des CM ([1c0ac3c](https://github.com/Dannebicque/intranetV3/commit/1c0ac3c9c548de1f3f0ff668b33446e7bdc98daa))
* Modification d'un étudiant. ([93e1dd4](https://github.com/Dannebicque/intranetV3/commit/93e1dd4401729bb92446a823e0daa9aea54da4ca))
* Stimulus controller pour gérer les toasts des pages. ([1e1960e](https://github.com/Dannebicque/intranetV3/commit/1e1960e9d6e7ce5130fcf5fd3b70330c626f882e))

### [3.1.29](https://github.com/Dannebicque/intranetV3/compare/v3.1.28...v3.1.29) (2021-10-04)


### Features

* Suppression du rotating de la gestion des logs. Déchargée à Syslog ([5f49867](https://github.com/Dannebicque/intranetV3/commit/5f4986796260f55f15927c3540fd888a77b95930))
* Suppression du rotating de la gestion des logs. Déchargée à Syslog ([46330c8](https://github.com/Dannebicque/intranetV3/commit/46330c805f6b93e23e2f9ea245f32f9fe00d094b))


### Bug Fixes

* Prévisionnels même si pas de collègue pour les exports et listes globales ([1c7fae5](https://github.com/Dannebicque/intranetV3/commit/1c7fae557c8c4100bae27a5da95a793e90a2e0df))

### [3.1.28](https://github.com/Dannebicque/intranetV3/compare/v3.1.27...v3.1.28) (2021-10-01)


### Features

* Affichage du libellé de l'évalation en complément de la matière ([1af7fa7](https://github.com/Dannebicque/intranetV3/commit/1af7fa730ba1ea82dd58dca5d5fb5097971d45d6))
* Affichage du libellé de l'évalation en complément de la matière ([7d28c64](https://github.com/Dannebicque/intranetV3/commit/7d28c64058a311fee5c18fd301477eda20726bed))
* Suppression du rotating de la gestion des logs. Déchargée à Syslog ([244ca76](https://github.com/Dannebicque/intranetV3/commit/244ca765f48de2a2cb21456eb73abb51de62bd77))
* Suppression du rotating de la gestion des logs. Déchargée à Syslog ([422e987](https://github.com/Dannebicque/intranetV3/commit/422e987dc0a4481e58a9f41e1a9104913a8abd8e))
* Utilisation de FontAwesome 6 ([9f7eda8](https://github.com/Dannebicque/intranetV3/commit/9f7eda863c50f14ea7248a3c9a58a838dbeb8e42))


### Bug Fixes

* affichage date heure de l'absence. (patch temporaire...) ([9b831c4](https://github.com/Dannebicque/intranetV3/commit/9b831c4cffe07af052468938def3961987eacdc8))
* Envoi des mails aux étudiant alors que l'option est désactivée. ([e4b63a3](https://github.com/Dannebicque/intranetV3/commit/e4b63a385bd05d3c27e088d88231656f0c460c7b))
* Envoi des mails aux étudiant alors que l'option est désactivée. ([b00a7e3](https://github.com/Dannebicque/intranetV3/commit/b00a7e3537ea8cf0e4bfdb8c0e60abb218e2e4ed))
* Gestion et accès aux notes/évaluations ([2c5b8ba](https://github.com/Dannebicque/intranetV3/commit/2c5b8ba03475ad28d3106e91ff94cabbfa9fe925))

### [3.1.27](https://github.com/Dannebicque/intranetV3/compare/v3.1.26...v3.1.27) (2021-09-30)


### Bug Fixes

* Accès aux évaluations ([690a5b7](https://github.com/Dannebicque/intranetV3/commit/690a5b7269cd5f38d4dad9b3b70af84a9f5b979e))
* Accès aux évaluations, traduction et commentaire non obligatoire ([8879f4e](https://github.com/Dannebicque/intranetV3/commit/8879f4ed2184990f2c7def54077708ad68d9f1cd))

### [3.1.26](https://github.com/Dannebicque/intranetV3/compare/v3.1.25...v3.1.26) (2021-09-30)

### Bug Fixes

* Affichage de la date de naissance uniquement RP + ASS + CDD ([482928b](https://github.com/Dannebicque/intranetV3/commit/482928b14c4a3a474344b473abaa3d2587648fb3))

### [3.1.25](https://github.com/Dannebicque/intranetV3/compare/v3.1.24...v3.1.25) (2021-09-29)


### Features

* Gestion des centres financiers pour les bons de commandes dématérialisés ([cfdb3d5](https://github.com/Dannebicque/intranetV3/commit/cfdb3d541b3eed38e8a40f9a8fdf87e6b1dc931f))


### Bug Fixes

* Affichage des emplois du temps profs depuis l'admin ([5746497](https://github.com/Dannebicque/intranetV3/commit/5746497ed67cdf6c6c0bfa512863c398fe146846))
* Affichage des emplois du temps profs depuis l'admin ([a9beef7](https://github.com/Dannebicque/intranetV3/commit/a9beef7f939ac5732f306c5a18f9db6a75ef8a90))
* Ajout et suppression de documents ([b413d76](https://github.com/Dannebicque/intranetV3/commit/b413d7655f4d54ce5a1034ecb3750a90ec8715ff))
* Commentaire sur une évaluation optionnel ([c5a5e23](https://github.com/Dannebicque/intranetV3/commit/c5a5e2380caf928db0a0e9c0459120f9bf5f6211))
* Configuration des logs ([058d5f5](https://github.com/Dannebicque/intranetV3/commit/058d5f5adf6f20a2af8fb24e4f7efac7296ef5bc))

### [3.1.24](https://github.com/Dannebicque/intranetV3/compare/v3.1.23...v3.1.24) (2021-09-27)


### Features

* Ajout d'une colonne pour les UE et Parcours dans la liste des matières du PPN. ([34e563e](https://github.com/Dannebicque/intranetV3/commit/34e563e1709a94efe77b2334b441ffd20f66cf6a))


### Bug Fixes

* Date de mise à jour qui change à chaque affichage si entité avec un fichier. ([8fcd6f8](https://github.com/Dannebicque/intranetV3/commit/8fcd6f8c7b1bc578fe86d97f88b57cdf3a0accc1))

### [3.1.23](https://github.com/Dannebicque/intranetV3/compare/v3.1.22...v3.1.23) (2021-09-27)


### Features

* Dématérialisation de la demande des BC. Structure et Workflow ([bb2faff](https://github.com/Dannebicque/intranetV3/commit/bb2faff7fa17812bf9ac2adfe7d88d498673b02c))
* Récupération du groupe depuis les événements Celcat ([3443000](https://github.com/Dannebicque/intranetV3/commit/3443000ff1bde999461bb257bd62cd93a3ff7d45))
* Récupération du groupe depuis les événements Celcat ([8c6da02](https://github.com/Dannebicque/intranetV3/commit/8c6da028c044b2457ffc2a0a9cdaa1a31523351e))
* Récupération du groupe depuis les événements Celcat ([03719a8](https://github.com/Dannebicque/intranetV3/commit/03719a8fb1038a21083b0cea62f018eed4c632eb))
* Récupération du groupe depuis les événements Celcat ([a2f2c9a](https://github.com/Dannebicque/intranetV3/commit/a2f2c9ac43741314c934df726b171d4d80dbc94a))
* Récupération du groupe depuis les événements Celcat ([b9a2bf0](https://github.com/Dannebicque/intranetV3/commit/b9a2bf0cc80cc012b64e48ca599c78e72adeeec5))
* Récupération du groupe depuis les événements Celcat ([5c309eb](https://github.com/Dannebicque/intranetV3/commit/5c309eba1337131a4785a9a1ba35733e16dfc1ee))


### Bug Fixes

* Affichage du groupe dans l'EDT ([611ad25](https://github.com/Dannebicque/intranetV3/commit/611ad2572adfb23a9350e2aa00d512df2685cb90))
* Feuilles placement. Select ne marche pas après ajax. ([0678e34](https://github.com/Dannebicque/intranetV3/commit/0678e3412526f9de9144d1bc8522500c1889974d))
* Nombre de mesasge n'est plus dans DataUserSession. A modifieR. Suspendu ([36471e2](https://github.com/Dannebicque/intranetV3/commit/36471e2e1e0fe5d37c033b11b7bbb45e77b5f981))
* Saisie des absences depuis l'admin ([4f82e60](https://github.com/Dannebicque/intranetV3/commit/4f82e6048aafcfff2a9d40a3f648393550bd7358))
* TinyMce dans la messagerie ([a701531](https://github.com/Dannebicque/intranetV3/commit/a701531ab6a982c96c7d8a61c00f5e02558fa7f8))

### [3.1.22](https://github.com/Dannebicque/intranetV3/compare/v3.1.21...v3.1.22) (2021-09-26)


### Features

* Amélioration de l'affichage de l'EDT quelque soit la source ([c3af970](https://github.com/Dannebicque/intranetV3/commit/c3af9708ced43224b52ef27deab051e708fa8df3))
* Dématérialisation de la demande des BC. Structure et Workflow ([e572d0d](https://github.com/Dannebicque/intranetV3/commit/e572d0de8eebe15296b0b96ab81acc9ced6405c9))
* Saisie des absences depuis Celcat ou l'EDT de l'intranet ([1a0773b](https://github.com/Dannebicque/intranetV3/commit/1a0773bbb32bf8bd4095b4f821b43af271194de8))


### Bug Fixes

* Import avec les périodes de projets DUT ([27e6975](https://github.com/Dannebicque/intranetV3/commit/27e6975af4301a977a439d5b2776d6412bb7fdd4))
* tableType étudiant et appelSuivi, EvenementEdt.php, paging ([d953b84](https://github.com/Dannebicque/intranetV3/commit/d953b8474d8de333e4e16606219edba5c6868e96))

### [3.1.21](https://github.com/Dannebicque/intranetV3/compare/v3.1.20...v3.1.21) (2021-09-26)


### Features

* Amélioration du formulaire d'ajout d'un groupe ([976ac24](https://github.com/Dannebicque/intranetV3/commit/976ac248fd7e6be37f9a9f878f5b8398089591a4))
* Pré-remplissage des nom, prénom et date de naissance des personnels à partir du LDAP. Ajout du type d'utilisateur TEC. ([b6e274c](https://github.com/Dannebicque/intranetV3/commit/b6e274c578d0bb011091f92a38723a46e0ac01e9))
* Pré-remplissage des nom, prénom et date de naissance des personnels à partir du LDAP. Ajout du type d'utilisateur TEC. ([1e2e864](https://github.com/Dannebicque/intranetV3/commit/1e2e864c47f96fa611de009d15b506db0b2609e9))
* Synchro structure depuis UE ou Semestre. ([aa4d3ff](https://github.com/Dannebicque/intranetV3/commit/aa4d3ffce203afde4ee34ff8a06d0abf23469226))
* Synchro structure depuis UE ou Semestre. ([ce9d770](https://github.com/Dannebicque/intranetV3/commit/ce9d770bb52a08202afd251d63ce3c4f5420ed30))
* Synchro structure depuis UE ou Semestre. ([c00494e](https://github.com/Dannebicque/intranetV3/commit/c00494e8a023379549ad1bc28bb92590670bc1d9))
* TableType pour tous les étudiants ([798dd00](https://github.com/Dannebicque/intranetV3/commit/798dd00a697233af747eac338a4dc93ddab25098))


### Bug Fixes

* Bug et traductions sur la création de structure ([5c422c7](https://github.com/Dannebicque/intranetV3/commit/5c422c7519e9ba4d80e94fa2f1f7dd250c1ce755))
* Convertion Markdown si champ vide ([049718f](https://github.com/Dannebicque/intranetV3/commit/049718ff474e28b84e38b9a4c0099184849dc7f5))
* Correctifs page structure ([a1d6724](https://github.com/Dannebicque/intranetV3/commit/a1d6724d28a4021ce3fe827d2712536f6447dcc5))
* Filtre sur personnel = null pour la création d'un prévisionnel ([e498b19](https://github.com/Dannebicque/intranetV3/commit/e498b19464c726ce0dd2c955e02cf6746853fb84))
* mise en page des formulaires ([ac81a79](https://github.com/Dannebicque/intranetV3/commit/ac81a79f847e3a7f10088b1f1688d9ba8215e1b0))
* Saisie absence, sans absent. Bug de semestre. + Saisie depuis l'EDT ([d3de0f6](https://github.com/Dannebicque/intranetV3/commit/d3de0f689d262f418a4c3c483825aa86561b29b6))
* Suppression d'une matière ([46dd240](https://github.com/Dannebicque/intranetV3/commit/46dd24048aa45b9ad7a43a785b9593cfb3aa97a5))
* Suppression d'une matière ([1592c8d](https://github.com/Dannebicque/intranetV3/commit/1592c8d74608b55da38b25fbeb5f4195dc261dfa))
* Suppression d'une matière ([4227719](https://github.com/Dannebicque/intranetV3/commit/4227719e1abf51e720db62c6f23644f3e767bfdb))
* Suppression des documents ([7c64b90](https://github.com/Dannebicque/intranetV3/commit/7c64b90a04185fb0cf75835e556ccbff626f920d))
* Suppression des documents et des types de document, avec suppression des fichiers et des liaisons ([71dc323](https://github.com/Dannebicque/intranetV3/commit/71dc32370c0a373e16225205c08289844284e9c2))
* Suppression du prévisionnel avec le DTO. Optomisation des adapters ([a090102](https://github.com/Dannebicque/intranetV3/commit/a090102bdc4f552677e1ae86ce483ed31db8e903))
* Traduction du formulaire d'ajout de matière ([24846f0](https://github.com/Dannebicque/intranetV3/commit/24846f0048027a7d38980399c57efdd932d06d57))
* Typage float, et conversion ([2eb0689](https://github.com/Dannebicque/intranetV3/commit/2eb06894644e2392a04bf666a9b8260e66ddedd8))

### [3.1.20](https://github.com/Dannebicque/intranetV3/compare/v3.1.19...v3.1.20) (2021-09-23)


### Features

* Pré-remplissage des nom, prénom et date de naissance des personnels à partir du LDAP. Ajout du type d'utilisateur TEC. ([98a2326](https://github.com/Dannebicque/intranetV3/commit/98a2326cea3bf2ca7ba8ac2810755d3662dbac10))

### [3.1.19](https://github.com/Dannebicque/intranetV3/compare/v3.1.18...v3.1.19) (2021-09-23)


### Features

* Affichage du bouton action dans les profils étudiants pour les assistantes ([387296d](https://github.com/Dannebicque/intranetV3/commit/387296dde0249281fe5f80387c02c7b07321e90f))


### Bug Fixes

* Génération du slug pour les personnels ([5d4a61a](https://github.com/Dannebicque/intranetV3/commit/5d4a61a53ea2d3cdbe6ad27d222c1ab02c84b98e))
* Liste des justificatifs selon l'année universitaire de l'utilisateur ([fcd1463](https://github.com/Dannebicque/intranetV3/commit/fcd1463eb330ac2b338b5fc00c1d38595c50606b))

### [3.1.18](https://github.com/Dannebicque/intranetV3/compare/v3.1.17...v3.1.18) (2021-09-22)


### Bug Fixes

* Filtre par année universitaire ([ecc513b](https://github.com/Dannebicque/intranetV3/commit/ecc513be15ab4915cb56aee64d8434e2e1b5db4a))
* Génération du slug pour les personnels ([a5ca277](https://github.com/Dannebicque/intranetV3/commit/a5ca2771b520556f7ce139a119fe8149ca2b07a4))
* Unicode lors de la synchro celcat ([c94ee29](https://github.com/Dannebicque/intranetV3/commit/c94ee29e0ae6bed7934b04eece5dbb11118b9573))
* Unicode lors de la synchro celcat ([ed90e9b](https://github.com/Dannebicque/intranetV3/commit/ed90e9bbc6463b4108fbef423c4e29f52a44f379))
* Unicode lors de la synchro celcat ([08a39c6](https://github.com/Dannebicque/intranetV3/commit/08a39c6f635a4333717a48283c09936c0771d932))

### [3.1.17](https://github.com/Dannebicque/intranetV3/compare/v3.1.16...v3.1.17) (2021-09-20)


### Features

* Affichage de l'emploi du temps importé depuis Celcat ([0d646da](https://github.com/Dannebicque/intranetV3/commit/0d646dac4b7e91a66ea60d34e0f5a06498760957))
* Ajout de paramètres pour les personnels ([4fe6143](https://github.com/Dannebicque/intranetV3/commit/4fe6143788464ce8daf8667bdb5c5918f6a061cc))


### Bug Fixes

* Affichage des icônes sur les bornes ([a06bbaf](https://github.com/Dannebicque/intranetV3/commit/a06bbafa26ffd29e8bd74230049a8bfc93e22081))
* Affichage des périodes de stage pour les étudiants ([258508e](https://github.com/Dannebicque/intranetV3/commit/258508e1c6dc4121d3081e87f6bc74c2531d1200))
* ToolBar choix de l'année universitaire ([576f1e2](https://github.com/Dannebicque/intranetV3/commit/576f1e2849580da402cc0ed26c54b725f71e7db7))

### [3.1.16](https://github.com/Dannebicque/intranetV3/compare/v3.1.15...v3.1.16) (2021-09-17)


### Bug Fixes

* Affichage des périodes de stage pour les étudiants ([b862ab5](https://github.com/Dannebicque/intranetV3/commit/b862ab5cd1a1143c439c2e487d78b703e73fc596))
* Bug adresse Stage ([b680b67](https://github.com/Dannebicque/intranetV3/commit/b680b67ebc944b05a361ad3fe840cddcd222d36d))
* Bug adresse Stage ([5a7282c](https://github.com/Dannebicque/intranetV3/commit/5a7282ce42a000e1ef8d1db19b794ea6b4308b86))

### [3.1.15](https://github.com/Dannebicque/intranetV3/compare/v3.1.14...v3.1.15) (2021-09-15)


### Features

* Apercu du justificatif d'absence ([3a7d6bf](https://github.com/Dannebicque/intranetV3/commit/3a7d6bf6697800e8236bd1b2f04395c5a7cd789c))
* Edition des PN en live-edit ([7b9b2ae](https://github.com/Dannebicque/intranetV3/commit/7b9b2aed0277f7c2ad09c55d145e96fc667a04f7))


### Bug Fixes

* assets ([051cb04](https://github.com/Dannebicque/intranetV3/commit/051cb04191cefe98dd256135eb4ed5f6f6932a72))
* assets ([6284c12](https://github.com/Dannebicque/intranetV3/commit/6284c12b02c181a85b428ed5848643a7c4ef4b38))
* Liens dans les menus pour la gestion de la structure de diplôme ([91026d2](https://github.com/Dannebicque/intranetV3/commit/91026d2aca0e7a23af959a5ffcf93e3fba341dbf))
* Lisibilité structure ([2983a59](https://github.com/Dannebicque/intranetV3/commit/2983a59d87df3646a49212efe52864c319ccbf72))
* Liste des justificatifs ([778918d](https://github.com/Dannebicque/intranetV3/commit/778918d58ae3e2ee266735c99a09c4fa13648b60))
* Tableau de rattrapage ([a5dc268](https://github.com/Dannebicque/intranetV3/commit/a5dc2680ea6dddb42f53508d9f59c7bca6a4b74d))

### [3.1.14](https://github.com/Dannebicque/intranetV3/compare/v3.1.13...v3.1.14) (2021-09-13)


### Features

* Suppression département étudiant depuis trombi ([9e9f912](https://github.com/Dannebicque/intranetV3/commit/9e9f9120d24efbe873f2eda51bba3bd8e070612f))
* Suppression département étudiant depuis trombi ([a3e17da](https://github.com/Dannebicque/intranetV3/commit/a3e17daaab6bd38e9991f40b947d5ffe6717e77b))


### Bug Fixes

* Suppression département étudiant depuis trombi ([f3ad1ae](https://github.com/Dannebicque/intranetV3/commit/f3ad1ae33360dab70ef1da606b03cc0d1e01f1e8))

### [3.1.13](https://github.com/Dannebicque/intranetV3/compare/v3.1.12...v3.1.13) (2021-09-12)


### Features

* Refonte de la liste des étudiants d'un semestre avec le composant Table ([9e1a9ab](https://github.com/Dannebicque/intranetV3/commit/9e1a9ab80ec505d88f44a67fb042bbf274cb3a0a))
* SelectChangeWidget ([69e2a4d](https://github.com/Dannebicque/intranetV3/commit/69e2a4d8c7daf398f64fb775e11a5006b12c92bd))

### [3.1.12](https://github.com/Dannebicque/intranetV3/compare/v3.1.11...v3.1.12) (2021-09-12)


### Features

* Utilisation de tom-select pour la partie emploi du temps ([28d1117](https://github.com/Dannebicque/intranetV3/commit/28d1117fd0c184f0e697ca5ab3c9af80a0179afd))


### Bug Fixes

* Affichage du type de groupe ([17100ec](https://github.com/Dannebicque/intranetV3/commit/17100ec252d9e736c9cb9bd399846a09f49aa691))
* assets ([f88b901](https://github.com/Dannebicque/intranetV3/commit/f88b90141ebc6782b0155f7859b4ba634c5b5fac))
* Correctifs sur la partie période de stage, lisibilité du formulaire, traductions, et textes d'aide ([3a0c6c0](https://github.com/Dannebicque/intranetV3/commit/3a0c6c032dab3a3308623c18ecbb2c86d5ebb18c))
* DatePicker sur la saisie des absences ([8c50e6f](https://github.com/Dannebicque/intranetV3/commit/8c50e6f96937ac8bee70f3fba55021f94df41b71))
* Filtre par défaut sr l'Edt en administration ([1d7cd68](https://github.com/Dannebicque/intranetV3/commit/1d7cd68450df13ad7700910ba9f704a6a6e1d224))
* Suppression des console.log dans le js ([d494861](https://github.com/Dannebicque/intranetV3/commit/d494861a2a173f7cd461de3595adb4d52c8abefb))
* Tom-Select + utilisation sur des formulaires ([9d34b4d](https://github.com/Dannebicque/intranetV3/commit/9d34b4dc1f3097ce0e110e6493aff13b35cc8ce3))

### [3.1.11](https://github.com/Dannebicque/intranetV3/compare/v3.1.10...v3.1.11) (2021-09-11)


### Features

* Ajout d'une commande pour mette à jour les données étudiantes depuis Apogée ([1d94d6b](https://github.com/Dannebicque/intranetV3/commit/1d94d6be1551b974413e87a783ea25d5a0ac4dff))


### Bug Fixes

* Gestion des types de groupes qui disparait selon le semestre ([5edf7e4](https://github.com/Dannebicque/intranetV3/commit/5edf7e45d0b0c7e49f25c3e2f8cfa16cdf6a570b))

### [3.1.10](https://github.com/Dannebicque/intranetV3/compare/v3.1.9...v3.1.10) (2021-09-11)


### Features

* Ajout d'un type EntityColumnType.php sur le composant Table ([4a1e4f8](https://github.com/Dannebicque/intranetV3/commit/4a1e4f8d8f1aa7eaa76705d9d0b075ced0da817c))


### Bug Fixes

* Calcul du slug ([f1a2c9e](https://github.com/Dannebicque/intranetV3/commit/f1a2c9ea37ab03b7d1d7971ade8942ba98cc3c62))
* Import d'étudiants depuis Apogée avec la date au format dd/mm/aa ([d9ff74a](https://github.com/Dannebicque/intranetV3/commit/d9ff74a96e3827e911da97e6f9fd06d246ef56e3))
* Liens avec id du département ([71da933](https://github.com/Dannebicque/intranetV3/commit/71da9331d3481b414906db95d36a8ee3e1bdb7ce))
* Synchros apogée, gestion des versions, import DUT ou BUT ([b4a394d](https://github.com/Dannebicque/intranetV3/commit/b4a394dd441c0b1de1e09cba6ebf5db5a7976eff))
* Utilisation d'un table type dans Appel Fait ([8548e11](https://github.com/Dannebicque/intranetV3/commit/8548e11532aa04fe9d247ce2c460e899711a6c5d))

### [3.1.9](https://github.com/Dannebicque/intranetV3/compare/v3.1.8...v3.1.9) (2021-09-10)


### Features

* Import des Maquettes depuis Apogée ([8e5f7c7](https://github.com/Dannebicque/intranetV3/commit/8e5f7c7b7772a5ec4d198858fb3754dc6285064b))
* Import des Maquettes depuis Apogée ([c036be5](https://github.com/Dannebicque/intranetV3/commit/c036be5b697b4e738e294d534bf9f0d1ae9fb41b))
* Import des Maquettes depuis Apogée ([5139505](https://github.com/Dannebicque/intranetV3/commit/5139505ec03e05fb494925b0440248e9b4d0207d))
* Import des Maquettes depuis Apogée ([47e770b](https://github.com/Dannebicque/intranetV3/commit/47e770b104b01f27c6b35cec6351ea9ae6bc242d))
* Import des SAE ([42822c1](https://github.com/Dannebicque/intranetV3/commit/42822c1a54e3fc70a743da2698e58f367d67fa08))
* Import des SAE ([c2ec78d](https://github.com/Dannebicque/intranetV3/commit/c2ec78dea076f56d049ec9b0799509378cc2754c))


### Bug Fixes

* Bug de la date updated ([f4c85fc](https://github.com/Dannebicque/intranetV3/commit/f4c85fcde5ee9fb82dbdb656b98930683970611e))
* Bug Edt suite suppression dépendance ([12d033b](https://github.com/Dannebicque/intranetV3/commit/12d033b591b83e98adfc97b74fa2586bd7b197f5))
* Fix divers ([2c3f83d](https://github.com/Dannebicque/intranetV3/commit/2c3f83d05d8340837f8b6b90e794964fa9a279cc))
* Fix divers ([d5f7740](https://github.com/Dannebicque/intranetV3/commit/d5f77403f33dd4cb9c7ddf19a261c7632e369404))
* Fix divers ([9944818](https://github.com/Dannebicque/intranetV3/commit/99448181265e8604237c8baac6483e5703422ce4))
* Fix divers ([0d28d75](https://github.com/Dannebicque/intranetV3/commit/0d28d7589202dde8303baf7f09bfd43271b9ce48))
* Fix divers ([fc1f60c](https://github.com/Dannebicque/intranetV3/commit/fc1f60c56e9b836b774b56a5efd754bf40fbfff6))
* Fix divers ([10bf83c](https://github.com/Dannebicque/intranetV3/commit/10bf83cc4b2e8b352e51b86c30a707563f2799c2))
* Fix divers ([010122d](https://github.com/Dannebicque/intranetV3/commit/010122d2e5d8b843422c67be35f4ae698312006a))
* Fix divers ([643a656](https://github.com/Dannebicque/intranetV3/commit/643a656c36bb62327499b07a428a230ad3380552))
* Fix divers ([b9b6704](https://github.com/Dannebicque/intranetV3/commit/b9b6704759dc7bf43e8cc0f31753c7090a679d57))
* Fix divers ([da49968](https://github.com/Dannebicque/intranetV3/commit/da49968f22cf12eedf701660a87afd0e441d2ee2))
* Fix divers ([c132153](https://github.com/Dannebicque/intranetV3/commit/c132153b64d1c4b5bcbe170472917a1e24942ed3))
* Fix divers ([fbf928d](https://github.com/Dannebicque/intranetV3/commit/fbf928d1e0fc0af86040e998ccec01fd3d7fc743))
* Ldap ([43a39af](https://github.com/Dannebicque/intranetV3/commit/43a39afcf3b5d378ce5d17f25a5dd875e2cffb77))
* MyLdap correctif pour utiliser la librairie Symfony ([a059987](https://github.com/Dannebicque/intranetV3/commit/a0599871d3e5403d7c4653479f3f77b42e9bcb2f))
* Saisie d'un rattrapage ([4a919ec](https://github.com/Dannebicque/intranetV3/commit/4a919ece4886a0c1e78e1e502e96ae16b74e1998))

### [3.1.8](https://github.com/Dannebicque/intranetV3/compare/v3.1.7...v3.1.8) (2021-09-08)


### Features

* Optimisation des classes executées avant le contrôleur (receuil des données) ([8c6cd65](https://github.com/Dannebicque/intranetV3/commit/8c6cd65c11a736cf71848f92117bf4e9552d25b9))
* Retrait de datatableJs dans les assets ([1e52628](https://github.com/Dannebicque/intranetV3/commit/1e5262881cf000a6c03be373e2495b7226c93c65))


### Bug Fixes

* Améliorations et correctifs composant table ([292032f](https://github.com/Dannebicque/intranetV3/commit/292032f6661daa1887078ff4a9ed90b1b8b52fb5))
* Bug questionnaire ([5dc413a](https://github.com/Dannebicque/intranetV3/commit/5dc413a434216458b8f3fd455185348c12a00c9b))
* Bug questionnaire ([d5cf39c](https://github.com/Dannebicque/intranetV3/commit/d5cf39c5f9583dd25f44e4ece2271253af21af9b))
* Composant table et requetes liées aux pages ([8aa96d5](https://github.com/Dannebicque/intranetV3/commit/8aa96d5fca8bd018aeb2cf696fae946b5c671669))
* Convertion date/heure ([806f2bd](https://github.com/Dannebicque/intranetV3/commit/806f2bd98e4f54e0341ae0d713110a6ac4f4e119))
* date évaluation ([8df0292](https://github.com/Dannebicque/intranetV3/commit/8df0292866c9e5b6f59b9857ed5c936c47f38be8))
* Dates conventions ([4147ce4](https://github.com/Dannebicque/intranetV3/commit/4147ce49fde95ce2d30b4fa3873b86078dcb50fb))
* Fix divers ([92f7e4e](https://github.com/Dannebicque/intranetV3/commit/92f7e4e6939e69d5737f21c1b0da21eed2d045b8))
* problème ajout justificatif absence ([45b1833](https://github.com/Dannebicque/intranetV3/commit/45b1833531da7efc6a670c1136150b9e0f24c95e))
* Remise des heures EDT "hors COVID" ([0f826cb](https://github.com/Dannebicque/intranetV3/commit/0f826cb1fc72915b75ab00e6677dd6c57e0ec2f8))
* Typage Hrs ([7f9e267](https://github.com/Dannebicque/intranetV3/commit/7f9e267a453015dd854b31c040adc54b3f54343e))
* Utilisation du composant table ([2f9b18b](https://github.com/Dannebicque/intranetV3/commit/2f9b18bd6aa705866d8d9671c8e7dcbe427eb6ee))

### [3.1.7](https://github.com/Dannebicque/intranetV3/compare/v3.1.6...v3.1.7) (2021-09-02)


### Bug Fixes

* Bug typage Excel ([a7a0458](https://github.com/Dannebicque/intranetV3/commit/a7a0458cf1f2a730cb67b2c5bc4f28270253e3e1))
* Correctifs js, traductions, ... ([7ba0e74](https://github.com/Dannebicque/intranetV3/commit/7ba0e742bdc0a8e350f056adad7ba2de36b9a495))
* Documents Favoris ([c2b2912](https://github.com/Dannebicque/intranetV3/commit/c2b2912fee01c9a67680feddc087215cf872c1d0))
* Listing étudiants format qualité ([045e894](https://github.com/Dannebicque/intranetV3/commit/045e8944c752c8b8bfd32fc8721628a79d1aa004))
* Listing étudiants format qualité ([04553c5](https://github.com/Dannebicque/intranetV3/commit/04553c55c6e2fcc9671b3a48922bde69fe283538))
* Problème de DatePicker ([d25dd26](https://github.com/Dannebicque/intranetV3/commit/d25dd26e5a8a184e0fc97478711f6e5f5b526707))
* Suppression avec le composant Table ([9090bda](https://github.com/Dannebicque/intranetV3/commit/9090bda38d135ebe21295fd0e8f1b8ba34acac4a))

### [3.1.6](https://github.com/Dannebicque/intranetV3/compare/v3.1.5...v3.1.6) (2021-09-02)


### Bug Fixes

* Bornes EDT ([3707607](https://github.com/Dannebicque/intranetV3/commit/3707607a3e0053e0d46ea00980452a29ab16fe7f))
* Bornes EDT ([bcc9246](https://github.com/Dannebicque/intranetV3/commit/bcc9246a2f52c28f9592f662d8fe9c19e53333dc))
* Bornes EDT ([88ed61c](https://github.com/Dannebicque/intranetV3/commit/88ed61ca9d17e983a310057189533f99476934fe))
* Fix Celcat Groupes ([82459a4](https://github.com/Dannebicque/intranetV3/commit/82459a41f6ca868a4c7579bc7019de2d0932a3cc))
* Fix Celcat Groupes ([ef8a4ca](https://github.com/Dannebicque/intranetV3/commit/ef8a4ca98edab570e5d5b4c24169275e9f04447d))
* Fix Celcat Groupes ([12662aa](https://github.com/Dannebicque/intranetV3/commit/12662aaac64a64530996d46ae80c8f0d6970c59a))
* Fix Celcat Groupes ([619cad8](https://github.com/Dannebicque/intranetV3/commit/619cad881ce7aad1cf839d3d583769bed77990f6))
* Fix import Apogée étudiants étrangers ([c9eb8c7](https://github.com/Dannebicque/intranetV3/commit/c9eb8c797b89f242cc1b263a6c097c54b328279d))
* Suppression avec le composant Table ([6c70260](https://github.com/Dannebicque/intranetV3/commit/6c702600861fbf3fc05f93489ea2e5a6e63dd662))

### [3.1.5](https://github.com/Dannebicque/intranetV3/compare/v3.1.4...v3.1.5) (2021-08-31)


### Bug Fixes

* Ajout d'un permanent ([f0a4a69](https://github.com/Dannebicque/intranetV3/commit/f0a4a69d03ee0ab142063887cd044378da274658))
* Ancien Date Picker ([9aac574](https://github.com/Dannebicque/intranetV3/commit/9aac574561f18aa61788d70b61a1726ec837472d))
* Convert to float ([7997b96](https://github.com/Dannebicque/intranetV3/commit/7997b9673619e0a83984d3420f939ac1626f982d))
* Datatable (changement de département) ([333c70e](https://github.com/Dannebicque/intranetV3/commit/333c70e3b06ab5e1256fdccb85590eb886a05568))
* Datatable (changement de département) ([e7962d8](https://github.com/Dannebicque/intranetV3/commit/e7962d8938bf0c83ed7ed220d76a08e4fff64a6f))
* emploi du temps chronologique ([eeeb100](https://github.com/Dannebicque/intranetV3/commit/eeeb1008e2c3dc0916df3a5c207647a15d08a2e8))
* Emploi du temps chronologique ([da4f88c](https://github.com/Dannebicque/intranetV3/commit/da4f88c2a832407e629e948d25bb549e9487e531))
* Espaces dans le textarea d'import des étudiants ([4d716f8](https://github.com/Dannebicque/intranetV3/commit/4d716f83b2867d89f2104a48dafa75d92805684e))
* FindByDiplome manquante dans les manager du prévisionnel ([a0f1b0e](https://github.com/Dannebicque/intranetV3/commit/a0f1b0e7fd395cc66e9cb2461b0b7061b6bf6b80))
* FindByDiplome manquante dans les manager du prévisionnel ([269f370](https://github.com/Dannebicque/intranetV3/commit/269f37020946141f777d434766036512776f7aec))
* Import apogée si plusieurs étapes ([105177f](https://github.com/Dannebicque/intranetV3/commit/105177f6468cc7c4ffc4d03aa68e4c3ed6d03ebe))
* Import des prévisionnels ([d4141a0](https://github.com/Dannebicque/intranetV3/commit/d4141a07a7b4785d9d632d63958deb25cf3ee247))
* Synchro Apogée ([7117acc](https://github.com/Dannebicque/intranetV3/commit/7117acc5d0132db512102873b7ecbf6742d918a6))
* Synchro Apogée ([10cab10](https://github.com/Dannebicque/intranetV3/commit/10cab10f0eb1bbc6d761b5dcc0017eb1742a2192))
* Typo sur constante ([bc8cebf](https://github.com/Dannebicque/intranetV3/commit/bc8cebf35e343bc4fe4819b4c9afffa0d68ff75a))
* Update dates EDT ([b85db4b](https://github.com/Dannebicque/intranetV3/commit/b85db4b4052b30693a1e26a8de5d53c9675edc5a))

### [3.1.4](https://github.com/Dannebicque/intranetV3/compare/v3.1.3...v3.1.4) (2021-08-30)


### Bug Fixes

* Affichage des détails d'un événement pour les étudiants ([b0047f3](https://github.com/Dannebicque/intranetV3/commit/b0047f3b6e2bcf2c1e738ad8de1ed63899035227))
* Optimisation de ConvertToFloat. ([e570f68](https://github.com/Dannebicque/intranetV3/commit/e570f682c9cafcbf943e4c60291120a4c11ead15))
* Options pas dans le bon format ([902b78a](https://github.com/Dannebicque/intranetV3/commit/902b78aa1b72d63b6cdcf19fe96eac50fad4864f))

### [3.1.3](https://github.com/Dannebicque/intranetV3/compare/v3.1.2...v3.1.3) (2021-08-30)


### Bug Fixes

* Bug ancien picker ([47db044](https://github.com/Dannebicque/intranetV3/commit/47db04483c4b8ed583942493c434e28833ed8aed))
* FindByDiplome dans le prévisionnel ([c492e77](https://github.com/Dannebicque/intranetV3/commit/c492e778f67a6c2a90328285ba38a3678fc718a4))
* PDF format qualité ([ee69ccf](https://github.com/Dannebicque/intranetV3/commit/ee69ccf486fe7b21bfbac4f6fd5bd7bf19f1afc0))
* setMatiere residuel. ([48cd503](https://github.com/Dannebicque/intranetV3/commit/48cd50370d1c7427f150f74831762ff880abd06c))
* Table sur la page Borne ([0dc66e2](https://github.com/Dannebicque/intranetV3/commit/0dc66e259ffee1b4dbc8fac15286c673219d9214))
* Typo getter ([d284828](https://github.com/Dannebicque/intranetV3/commit/d284828b480d1a848ca8f85f450b34fafc2b4521))

### [3.1.2](https://github.com/Dannebicque/intranetV3/compare/v3.1.1...v3.1.2) (2021-08-29)


### Features

* Import Apogée depuis les départements ([d347db2](https://github.com/Dannebicque/intranetV3/commit/d347db2e2c9c79b8e01dbabdc94ed5a4c7a1607d))


### Bug Fixes

* Convertion en float ([b8353be](https://github.com/Dannebicque/intranetV3/commit/b8353be32977b61abc3a6008309686e94111ffab))
* Correctifs Table + Nouveau DatePicker ([3cf32d2](https://github.com/Dannebicque/intranetV3/commit/3cf32d2a102dda7eaf026071056298ace05401bf))
* import apogée et mise en page. ([8e4588d](https://github.com/Dannebicque/intranetV3/commit/8e4588d2cb883f597ae7a2f895bce6a8de9a70ce))
* Utilisation du composant Table ([7d3c13a](https://github.com/Dannebicque/intranetV3/commit/7d3c13a9b7231536a22351536d2147f9f834d178))

### [3.1.1](https://github.com/Dannebicque/intranetV3/compare/v3.1.0...v3.1.1) (2021-08-29)


### Features

* Import Apogée depuis les départements ([f2c6236](https://github.com/Dannebicque/intranetV3/commit/f2c623625fc87ab151af4d3bbae8c151519c9cd0))


### Bug Fixes

* Configuration pagination ([e94754e](https://github.com/Dannebicque/intranetV3/commit/e94754e72a9ae387525793ccda5cd54a8532bbaa))
* Mis à jour JS sans Umbrella, retrait des dépendances inutiles ([9f0c96a](https://github.com/Dannebicque/intranetV3/commit/9f0c96a08fd102f58f2684fadb02a45063b22937))
* Mise en place de Services.php dans DependencyInjection ([bb9545e](https://github.com/Dannebicque/intranetV3/commit/bb9545e052056aaf1bfd69958bfa88a4c5c1157d))
* Mise en place de Services.php dans DependencyInjection ([7688cad](https://github.com/Dannebicque/intranetV3/commit/7688cadd1afc138b1417dbb6cb332eea97130505))
* Requete Synchro Apogée améliorée ([c687812](https://github.com/Dannebicque/intranetV3/commit/c687812c469a0ba71d53aa7abec25ce42b1f7f67))
* Type fichier Version.php ([c9b7a70](https://github.com/Dannebicque/intranetV3/commit/c9b7a70c48569cc307596a2334769a39f058fe9a))

## [3.1.0](https://github.com/Dannebicque/intranetV3/compare/v3.0.11...v3.1.0) (2021-08-29)


### Bug Fixes

* Mis à jour JS sans Umbrella, retrait des dépendances inutiles ([33d1ebc](https://github.com/Dannebicque/intranetV3/commit/33d1ebca866fc72822ec5dda203012736fd84d82))
* Refonte composant Table, ajout de Widget ([01c8e3e](https://github.com/Dannebicque/intranetV3/commit/01c8e3e07d64c89f42b0e617c0d41613eb7b3597))
* Utilisation du nouveau composant Table ([818e8eb](https://github.com/Dannebicque/intranetV3/commit/818e8eb9757ca836d2e51f3c6b752b9aab6af6e4))

### [3.0.11](https://github.com/Dannebicque/intranetV3/compare/v3.0.10...v3.0.11) (2021-08-28)


### Features

* gestion des filtres sur le composant tableau. ([dc477c3](https://github.com/Dannebicque/intranetV3/commit/dc477c3f64e611897493a8e06afce8b2511c2ad7))
* Nouveau composant pour datatable, correctiofs V0.2 ([d6fe9ef](https://github.com/Dannebicque/intranetV3/commit/d6fe9ef884c819dd71ccfc328583e5c47abe5b72))
* Nouveau composant pour le questionnaire V0.1 ([47c6d3c](https://github.com/Dannebicque/intranetV3/commit/47c6d3ce3ff8b78fa98edd6d947ae6b4c41f08ab))
* Suppression de select2 (et les parties associées à Umbrella). Utilisation de tom-select et d'un composant HTML surchargé. ([a4f70ee](https://github.com/Dannebicque/intranetV3/commit/a4f70eec15c13b904edf60c8fe33a4c84636ab77))


### Bug Fixes

* Affichage d'un message lorsque la saisie est faite (sans absent) ([57a96f2](https://github.com/Dannebicque/intranetV3/commit/57a96f2027ef262202fd5a4e27686629a957b2cb))
* Affichage des matières dans le suivi d'appel ([9c45b3f](https://github.com/Dannebicque/intranetV3/commit/9c45b3f271dc3e97f712d41fdadf4eac582ccb8d))
* Affichage des matières dans le suivi d'appel ([f9d7521](https://github.com/Dannebicque/intranetV3/commit/f9d75218af3638d3fcdfa14b767a1e7e7a8a3e3a))
* Affichage du groupe dans l'ICS ([abd3c11](https://github.com/Dannebicque/intranetV3/commit/abd3c11c0f3e6a2746fdbcf90b4b809e4e81c4ba))
* Affichage en doublons + erreur lors de la récupération du groupe par défaut ([#68](https://github.com/Dannebicque/intranetV3/issues/68)) ([2a043ca](https://github.com/Dannebicque/intranetV3/commit/2a043cad0bbb735b0d41b0f4124c531be0661c4c))
* Bug typage méthode getPassword ([975adca](https://github.com/Dannebicque/intranetV3/commit/975adcab256581aea7a5cbd0a2b5ca3bf01a47a7))
* Code Element matière ([9deae5b](https://github.com/Dannebicque/intranetV3/commit/9deae5bb611c0922ddf3e9e6761053431e0e1c53))
* Code Element matière ([4f192de](https://github.com/Dannebicque/intranetV3/commit/4f192dedc6a903a47fe6d5bd5e7f844e75e3fdc8))
* Composant Table ([e2d5f01](https://github.com/Dannebicque/intranetV3/commit/e2d5f01feeda0908382cf0f88f7e53deef0fefc9))
* Correctifs mineurs JS ([0ccc91f](https://github.com/Dannebicque/intranetV3/commit/0ccc91f24c81c6c6d4337a001fac17075029d23c))
* Correction taille icone dashboard ([0556e49](https://github.com/Dannebicque/intranetV3/commit/0556e49f26342143b2f139e3ba1f942d52a20129))
* CS ([4736692](https://github.com/Dannebicque/intranetV3/commit/4736692d540b2d2b5af7de5cb1bce8f48d2a8ace))
* Date null ([b558c55](https://github.com/Dannebicque/intranetV3/commit/b558c55489b604a02ed6c117cd16c0b8e7c18e80))
* Divers correctifs et améliorations de code. ([a18ce13](https://github.com/Dannebicque/intranetV3/commit/a18ce130291a9d505ffc5e0cab9caf9679b7fa9f))
* DTO prévisionnel et typage ([37ea499](https://github.com/Dannebicque/intranetV3/commit/37ea49909b36a0241c7f9c9868d1f69cc4e18f0e))
* Event présent, même si ne venait pas de l'EDT ([07cc887](https://github.com/Dannebicque/intranetV3/commit/07cc887fb2153638da8edcae3882923de5ec2564))
* Export PDF avec les différents types de matières (a adapter avec EDTManager ensuite) ([37ff161](https://github.com/Dannebicque/intranetV3/commit/37ff1615e1d0c84088dbc038ff477100c5c277ef))
* Export Prévisionnel ([849d10e](https://github.com/Dannebicque/intranetV3/commit/849d10e41c809116120efaa21f58994283bae41e))
* Génération des PDF, erreur si plusieurs liens (?). ([313e2dc](https://github.com/Dannebicque/intranetV3/commit/313e2dc55a36f8c0a224c646b68bedd04d2c33d4))
* Import maquette depuis Apogée ([6eedefc](https://github.com/Dannebicque/intranetV3/commit/6eedefc8083446838815d34876a7a12275f8022a))
* Lisibilité du semestre et du diplôme associé (confusion DUT.BUT) ([b068845](https://github.com/Dannebicque/intranetV3/commit/b06884521c1ae45b4d978234debff9d69469bee4))
* PHPCSFixer ([186d778](https://github.com/Dannebicque/intranetV3/commit/186d7782b2139762d2e1fa0038749e0bfc4e6559))
* Si pas de semaine de formation trouvée. Bug sur l'export ICS des étudiants. ([544cbd8](https://github.com/Dannebicque/intranetV3/commit/544cbd81b79a9189245568934e3e7cd0bca3cb15))
* Suppression de Umbrella ([31a4d0d](https://github.com/Dannebicque/intranetV3/commit/31a4d0dba2aa35e9f6a6b319aba5fb781ecff139))
* Suppression de Umbrella et des dépendances JS ([72f71d2](https://github.com/Dannebicque/intranetV3/commit/72f71d2f570955e529db85589423c6c7ce9f91c0))
* Suppression des fichiers de déploiement automatisés ([6faf71b](https://github.com/Dannebicque/intranetV3/commit/6faf71b8a7f82db16f4f0e0342ffc60fa3050c66))
* Suppression du datepicker d'Umbrella. ([b625336](https://github.com/Dannebicque/intranetV3/commit/b625336a440d6ee8390702cdc84195893da75f93))
* Suppressions des icones MDI. ([ff747a0](https://github.com/Dannebicque/intranetV3/commit/ff747a036fff8360013fb00ac7e94f1052355353))
* Synchronisation Apogée (groupes et maquettes) ([f6082e2](https://github.com/Dannebicque/intranetV3/commit/f6082e211263492d64da8d3232bb03abb93538b2))
* Synchronisation Celcat ([4d9f87f](https://github.com/Dannebicque/intranetV3/commit/4d9f87f69902ad53965b12d1cca079af2003bcdf))
* Typage incorrect ([6851962](https://github.com/Dannebicque/intranetV3/commit/6851962a1141531b858b64d9f155b4da793ac3d9))
* Typage incorrect ([75706af](https://github.com/Dannebicque/intranetV3/commit/75706af2700b3d5c614376a467e716cd3d5ca4a3))

### [3.0.10](https://github.com/Dannebicque/intranetV3/compare/v3.0.9...v3.0.10) (2021-07-29)


### Features

* Affichage de la version du site ([6a74598](https://github.com/Dannebicque/intranetV3/commit/6a7459832396518f0cf90d035c0ac83d9805895b))
* Ajout activation/désactivation d'une année ([f02e72d](https://github.com/Dannebicque/intranetV3/commit/f02e72dd82ff87416cf8b962cfa8e3fc52369302))
* Ajout du "type" de spécialité au bac. Pour les statistiques ([c0f4a7e](https://github.com/Dannebicque/intranetV3/commit/c0f4a7ee0523a3f5a7b7bf02d62feca1f5e0b6be))
* bouton saisie absence faite ([4f25804](https://github.com/Dannebicque/intranetV3/commit/4f25804f40bb27cba8af28ae21b8a3b8e939f57a))
* Filtre par années actives ([2e50ddc](https://github.com/Dannebicque/intranetV3/commit/2e50ddc4ee19f401a42a61d7174b3b32fc04772a))
* Gestion de la version améliorée ([c4cf550](https://github.com/Dannebicque/intranetV3/commit/c4cf55046fe1e8e69ad2d95e8966021943daa772))
* Nouveau composant pour datatable, sans jquery, et sans dépendance à Umbrella. V0.1 ([dffb96a](https://github.com/Dannebicque/intranetV3/commit/dffb96a7f9f008a25a1fffb14bbbcf7d89a0bf67))
* Redirection vers l'aide officielle ([fa2e28f](https://github.com/Dannebicque/intranetV3/commit/fa2e28ff4baced9e6bf2c863ed4525449092445d))
* Suivi de l'appel fait. ([2697fbd](https://github.com/Dannebicque/intranetV3/commit/2697fbd58414bb4f142babc5ce379c0d8ac4db07))
* Utilisation de la librairie Carbon pour la gestion des dates ([18c7c33](https://github.com/Dannebicque/intranetV3/commit/18c7c33baba189be003ca1809f9bac31365eb15a))


### Bug Fixes

* Affichage du semestre actif dans la liste des questionnaires ([738a456](https://github.com/Dannebicque/intranetV3/commit/738a456c909ba1164bbb1d9a82c0a1ec409f6622))
* bouton saisie absence faite ([59684d0](https://github.com/Dannebicque/intranetV3/commit/59684d0b99a675dd849198c89e868f0eb62bc232))
* Export Ical avec Ressources, SAE et Matières ([b64ce21](https://github.com/Dannebicque/intranetV3/commit/b64ce21c0d8fcdd7551ee1b9858598671d1d82e3))
* Import d'une prévisionnel OMEGA avec SAE/Ressource ou Matieres ([c9f40cc](https://github.com/Dannebicque/intranetV3/commit/c9f40cc53af7e2d5d06f7e7d8629fac4f3c208a6))
* Kernel suite mise à jour sf ([6dabd1e](https://github.com/Dannebicque/intranetV3/commit/6dabd1e45616cd6d6ef9b451cc0763bb5a8b4397))
* Mise à jour version SF + Notifier ([653a2c1](https://github.com/Dannebicque/intranetV3/commit/653a2c1ff71923945bd638efe10d934d571507e9))
* Mise à jour version SF + Notifier ([0f634b8](https://github.com/Dannebicque/intranetV3/commit/0f634b8fc5738b99a19d0d3c99385ac18c4c18dd))
* recherche SAE/Ressource sur BUT, filtre par diplome ([9621298](https://github.com/Dannebicque/intranetV3/commit/9621298e979a0f7d0bc9197a142b539eab587e73))
* Suppression du lien vers Matiere pour utiliser la classe générique ([a6c5ad0](https://github.com/Dannebicque/intranetV3/commit/a6c5ad0b47527ddc163f9868610fe4d0f70b9b7d))
* typo ([371850f](https://github.com/Dannebicque/intranetV3/commit/371850ffed66be7af3106683fddb3df8a3a2da8b))
* Utilisation d'une exception lorsqu'une matière n'existe pas ([1069e76](https://github.com/Dannebicque/intranetV3/commit/1069e76c581b9c9a24b53551568e00e12e7c1577))

### [3.0.9](https://github.com/Dannebicque/intranetV3/compare/v3.0.6...v3.0.9) (2021-07-21)
-> Versionning non fonctionnel

### [3.0.8](https://github.com/Dannebicque/intranetV3/compare/v3.0.6...v3.0.8) (2021-07-21)

-> Versionning non fonctionnel

### [3.0.6](https://github.com/Dannebicque/intranetV3/compare/v3.0.8...v3.0.6) (2021-07-21)

-> Versionning non fonctionnel

### [3.0.2](https://github.com/Dannebicque/intranetV3/compare/3.0.1...3.0.2) - 2021-05-12

-> Versionning non fonctionnel

### [3.0.1](https://github.com/Dannebicque/intranetV3/compare/3.0.0...3.0.1) - 2021-05-12

-> Versionning non fonctionnel

## [3.0.0](https://github.com/Dannebicque/intranetV3/compare/v1.0.3...3.0.0) - 2021-03-01

Versions 3.0.0 à 3.0.9

### Commits

- Mise en place du référentiel de
  BUT [`e6f84f1`](https://github.com/Dannebicque/intranetV3/commit/e6f84f18cd0df750d36f21909d2f631949caa0d5)
- Edition des questionnaires en
  SA [`7ca831e`](https://github.com/Dannebicque/intranetV3/commit/7ca831e1811c8f58e83380d4e81e9c08c10cb8be)
- PHp-CS-Fixer [`5471f42`](https://github.com/Dannebicque/intranetV3/commit/5471f4234e934d4a9dd4fb623b30b00120b49950)
- Update Webpack [`509bfaa`](https://github.com/Dannebicque/intranetV3/commit/509bfaa1668d680ba84aa1e1815cfabe8d0f2c5a)
- Passage de DomPdf à WkHtmlToPdf (gain de
  perf) [`9bc3b45`](https://github.com/Dannebicque/intranetV3/commit/9bc3b457d887a4d9edf8c62086f3c7317f342243)
- Suppression de DomPdf pour passer à
  WkHtmlToPdf [`ac9e50f`](https://github.com/Dannebicque/intranetV3/commit/ac9e50fe963d5723d8f7d5283fe89e943bb0380a)
- Sentry [`9462053`](https://github.com/Dannebicque/intranetV3/commit/946205366e20f847754271cd542a9be85530fe41)
- Remove de
  Sentry [`a153ee6`](https://github.com/Dannebicque/intranetV3/commit/a153ee6ae79996035e21cc3ca573fcac08c38687)
- Remplacement de Quill par
  TinyMce [`984a568`](https://github.com/Dannebicque/intranetV3/commit/984a568d5c1b69d8e06f18b27d9cf8b324a2e1a0)
- PHp-CS-Fixer [`1972b54`](https://github.com/Dannebicque/intranetV3/commit/1972b54e4dfc910821a6f7a03501f617a69cdf6a)
- Attestations COVID [`dc22ddb`](https://github.com/Dannebicque/intranetV3/commit/dc22ddb1504bb1cd4635450c98bf42d0ed371b82)
- Référentiel de compétence [`9bb061f`](https://github.com/Dannebicque/intranetV3/commit/9bb061f1ee1c1d29c1a9560fbf4386489fc5f3fa)
- Remplacement de la police Material Icon [`8bc81ce`](https://github.com/Dannebicque/intranetV3/commit/8bc81ce05840a6cf73322f6318a8eeac52eca1d5)
- Exports enquête diplômés [`90280d8`](https://github.com/Dannebicque/intranetV3/commit/90280d8b6e0c3299fecbab3bc95c0c2cd102efd8)
- Amélioration sous-commission/grand jury [`e7d9019`](https://github.com/Dannebicque/intranetV3/commit/e7d9019d71b1433168d649942d6abf8e14453154)
- Enquete diplomes [`4415204`](https://github.com/Dannebicque/intranetV3/commit/44152044eb5e6d000a18b256f3080dab7a3d4e1d)
- Grand Jury et Export Apogée [`6148bf3`](https://github.com/Dannebicque/intranetV3/commit/6148bf322e2808156636a2b6a32c3e04aafbc1d3)
- Script Celcat [`3e71505`](https://github.com/Dannebicque/intranetV3/commit/3e7150525e622b61cc898a4e67c0bd33eda11d22)
- Utilisation d'une date et stockage en Bdd plutôt qu'un calcul à chaque affichage [`3edce41`](https://github.com/Dannebicque/intranetV3/commit/3edce41ee15439aba59816153f19ae79813fd7bc)
- Sous commission export Apogée [`5416c40`](https://github.com/Dannebicque/intranetV3/commit/5416c40fc3b7e6a37002d6ab192a0158a6ad4f93)
- Bug trombi sans groupe par défaut [`1875c9f`](https://github.com/Dannebicque/intranetV3/commit/1875c9f5a7981f36498a6dc7c5403ede1e72c3ac)
- Script Celcat [`b583ef8`](https://github.com/Dannebicque/intranetV3/commit/b583ef89554d5d97d4bd6bf871967ec6d1ab37f5)
- Navigation dans la structure d'un département [`bd8d7cf`](https://github.com/Dannebicque/intranetV3/commit/bd8d7cfc66abcb5edf264a09234535ffc1da86d0)
- Ouverture RDD [`e428de5`](https://github.com/Dannebicque/intranetV3/commit/e428de53427a4ee201166021d0abe82961a3d88e)
- Génération des relevés de notes + Zip + message [`ea834bf`](https://github.com/Dannebicque/intranetV3/commit/ea834bf051a3942a92d8a2389c7c1b3f8bb16762)
- Structures et fiches d'aide [`11527b3`](https://github.com/Dannebicque/intranetV3/commit/11527b3f0e8da2d1f18f0614d506d39cc44367e4)
- Modification COVID [`24f62f7`](https://github.com/Dannebicque/intranetV3/commit/24f62f7bc9254d9bd5b47e403ef491fcfdcff6c4)
- Mise en place de Messenger avec RabbitMq [`809a609`](https://github.com/Dannebicque/intranetV3/commit/809a609c186ed52df01e6ffa976a14f55f2eb95e)
- Sous Comm [`93a80c1`](https://github.com/Dannebicque/intranetV3/commit/93a80c10e9492031a7b9d9cee790e68fdfac76c1)
- Import de scolarités depuis V2 [`7a61315`](https://github.com/Dannebicque/intranetV3/commit/7a613157889b607927a7273da373d86e7a5193e1)
- Sous-Comm statistiques [`e628e9c`](https://github.com/Dannebicque/intranetV3/commit/e628e9ce19478adb5e01fdcdc852f5b1f4c3dba7)
- Graphique de répartition des notes en administration [`f8365d4`](https://github.com/Dannebicque/intranetV3/commit/f8365d4818a0459a2aee412c4f6ac18d35493fe0)
- Suppression des rattrapages et justificatifs en lot [`ea97038`](https://github.com/Dannebicque/intranetV3/commit/ea97038a536fae4d2dbb0875cff2770349de3b85)
- Correctifs sous-comm (semestres précédents et ue) et export [`00584e2`](https://github.com/Dannebicque/intranetV3/commit/00584e29b059745e7965ce6bf77c27e90c8aa433)
- Service réalisé [`28cce31`](https://github.com/Dannebicque/intranetV3/commit/28cce31a35fbc5e26a20418499f948e8233629f3)
- Optimisation message, avec listes adaptées [`15b8e93`](https://github.com/Dannebicque/intranetV3/commit/15b8e9386c9e62585c2ad1ceeb21b21a4434539c)
- Export [`a70e803`](https://github.com/Dannebicque/intranetV3/commit/a70e8037dcc3bb48eb837dc1abad70a454688e7a)
- Meilleurs utilisations des paramètres [`4e7bf81`](https://github.com/Dannebicque/intranetV3/commit/4e7bf81de85bd4f62f2653391955700384c0e8cd)
- export relevés [`b6ae78c`](https://github.com/Dannebicque/intranetV3/commit/b6ae78c538790150d71d4df1678cff87b4a4a5c1)
- SousCommissionSauvegarde.php [`b99eed7`](https://github.com/Dannebicque/intranetV3/commit/b99eed74b92714f3cec46bea077f5df8d5e80ea2)
- Export PDF/XLSX/CSV des évaluations depuis l'administration [`d1cf1cd`](https://github.com/Dannebicque/intranetV3/commit/d1cf1cd566d0354c20e0de30a7e99457dfdb3487)
- Enquete diplômé [`7ac08a8`](https://github.com/Dannebicque/intranetV3/commit/7ac08a8d87205b5567548f218bff9b6a85c18061)
- Init du mot de passe [`ac5c333`](https://github.com/Dannebicque/intranetV3/commit/ac5c333d69a8f31bffb51710ff1cf65c3e5787ec)
- Améliorations pages superadmin [`2e3f59a`](https://github.com/Dannebicque/intranetV3/commit/2e3f59a3cfbdeb9948f031e246ad2f59176457f8)
- Correctif sur Aide [`01fd39e`](https://github.com/Dannebicque/intranetV3/commit/01fd39e63d570141384e33c68c05d28536d28142)
- Script Celcat [`0360485`](https://github.com/Dannebicque/intranetV3/commit/0360485a895e42056996be35d60013b4dd3645cc)
- RDD horodatage [`08f4092`](https://github.com/Dannebicque/intranetV3/commit/08f4092393ff2663031737d8a496904cabf7a613)
- Gestion des éléments de scolarite (ajout/modification) [`7dea2cd`](https://github.com/Dannebicque/intranetV3/commit/7dea2cd94b4c74ce7ab293e042789bd06d832bd7)
- Correctifs mineurs [`3818e00`](https://github.com/Dannebicque/intranetV3/commit/3818e002ced0501f206d5d71bed07646b6baa7fe)
- Nettoyage des notes sous-comm [`fb89bd2`](https://github.com/Dannebicque/intranetV3/commit/fb89bd226bbc32c38932f58c397957ec459cd34a)
- Script Celcat [`a4e7404`](https://github.com/Dannebicque/intranetV3/commit/a4e74045dbd190e930fd7ca530b483a3fb78b7c0)
- Gestion démission étudiant [`a95a8b3`](https://github.com/Dannebicque/intranetV3/commit/a95a8b346f30d9daafa880371d8a0e9795fa831f)
- Bornes EDT [`ce61123`](https://github.com/Dannebicque/intranetV3/commit/ce611235d0751dbd426d9ccda0c81c07bad62e16)
- Correctif export Apogée [`9a6bb5b`](https://github.com/Dannebicque/intranetV3/commit/9a6bb5b4cd0abc74efdc4e43265ab56a73a21a99)
- Bug EDT [`354f75d`](https://github.com/Dannebicque/intranetV3/commit/354f75d45b2222a564446ecd6d6748bed5378bda)
- Nombre de semestre par diplôme dans les sous-comm [`e37557c`](https://github.com/Dannebicque/intranetV3/commit/e37557c23539bfdcfa7f86af4601d8157a373790)
- Correctif export Apogée [`32ca87b`](https://github.com/Dannebicque/intranetV3/commit/32ca87b3fbe097657a5c2bd6b0ba8c224693f4b5)
- Bug salle examen [`5515828`](https://github.com/Dannebicque/intranetV3/commit/5515828a83c984f5486d9f872a9abe77bf705fbb)
- Export EDT [`81d2b8f`](https://github.com/Dannebicque/intranetV3/commit/81d2b8fe9bbee6ade50d18d13d3a9056c52790dd)
- Bug Export [`ebd2d82`](https://github.com/Dannebicque/intranetV3/commit/ebd2d8285d0fb3ef8f828a72977be7ecbcbcfa55)
- Script Celcat [`4d210d7`](https://github.com/Dannebicque/intranetV3/commit/4d210d702e7f8e3be0ef489e8afb3044ef6fecce)
- Correctif export qualité [`8f980ad`](https://github.com/Dannebicque/intranetV3/commit/8f980ad71c72a050e210cc927882343071a72e7b)
- Import scolarité V2 [`b3e8182`](https://github.com/Dannebicque/intranetV3/commit/b3e818215e9593e705a8f24c9d7d85508bedd807)
- Bugs Exports [`a0683a6`](https://github.com/Dannebicque/intranetV3/commit/a0683a6f1a1482b3a6b384d8c43b8b706015d38b)
- Enquete affiche des semestres d'une année. [`9206ddb`](https://github.com/Dannebicque/intranetV3/commit/9206ddb2d44877c08cfd1ee62bd1accb7e8cb245)
- Problème impression fiche suivi [`563b836`](https://github.com/Dannebicque/intranetV3/commit/563b836cf8f42e541c9b34f1b6a779a9491cdff1)
- Export enquête RDD [`e1b0a96`](https://github.com/Dannebicque/intranetV3/commit/e1b0a96b3db9cddcaac6425cf1c43a453dfe1782)
- PHp-CS-Fixer [`32a9394`](https://github.com/Dannebicque/intranetV3/commit/32a93943813fede0fc24b2bece3f1cbb1e1a1261)
- Correctifs mineurs [`fe6372d`](https://github.com/Dannebicque/intranetV3/commit/fe6372dc4a977f3327da1780969e4219e928bd49)
- Correctif export qualité [`75378a6`](https://github.com/Dannebicque/intranetV3/commit/75378a6d87e191ce6503ccee4886728deaf5d653)
- Export ICAL [`df8d9b2`](https://github.com/Dannebicque/intranetV3/commit/df8d9b2027210bc9777599e4ea0925fa567bbb06)
- Bug fin de semestre [`8f94d7c`](https://github.com/Dannebicque/intranetV3/commit/8f94d7c6098c4c417a4d1626f0def2eef8891e7d)
- Update Webpack [`45fcd71`](https://github.com/Dannebicque/intranetV3/commit/45fcd712c7ba83e5fe5cdf9c1848dbf96d5aaccd)
- Problème accent nom ficher [`fb9846b`](https://github.com/Dannebicque/intranetV3/commit/fb9846bbbabeab108bd77d786a9c337101e792e7)
- Attestations COVID [`4fc4b8b`](https://github.com/Dannebicque/intranetV3/commit/4fc4b8ba74f35b910164a6c305912667ba0f8a31)
- Ajout de l'option APC selon le type de diplôme [`1b5a77f`](https://github.com/Dannebicque/intranetV3/commit/1b5a77ffcae7ae5f6fff0a95af12dddebfa8fc20)
- Export [`50b98ee`](https://github.com/Dannebicque/intranetV3/commit/50b98ee0f88a20731631dff03b40dcc2b4c6675e)
- Export Apogee + prise en compte absences [`8e51811`](https://github.com/Dannebicque/intranetV3/commit/8e518117c03417f4d1b596e72436388a4c575fb8)
- Enquêtes ajout de l'horodatage [`626edc2`](https://github.com/Dannebicque/intranetV3/commit/626edc2c22f999fed24f7958404f6fae99951887)
- Problème récupération des notes [`5ab98ed`](https://github.com/Dannebicque/intranetV3/commit/5ab98ede95c7266dd282484aad6a49a7a80f71fd)
- Export PDF Covid [`da50daf`](https://github.com/Dannebicque/intranetV3/commit/da50daf984dadbeedad2f1d314c1e39256e88fe7)
- Ouverture RDD [`8022283`](https://github.com/Dannebicque/intranetV3/commit/8022283996ec009dbf47a50d62636f8111fe9569)
- Exports enquête diplômés [`d9b353c`](https://github.com/Dannebicque/intranetV3/commit/d9b353ce620f5488f3827e6ddf0e126ed01c5f8f)
- Bug export sous-commission [`5d426ad`](https://github.com/Dannebicque/intranetV3/commit/5d426addb777c04f3a6e1899e935398d7cdec6cb)
- Sous Comm [`b80c8ee`](https://github.com/Dannebicque/intranetV3/commit/b80c8ee4f51fa86e781f3cd1ef14dcb6f8b09f61)
- Format des mails aux personnels [`ac10430`](https://github.com/Dannebicque/intranetV3/commit/ac10430fbac8d65d6cd5ac147f5eecc8a38ad880)
- export relevés [`81ae110`](https://github.com/Dannebicque/intranetV3/commit/81ae110451100054189dd3575ac2c36c5032192a)
- Sous Comm [`e4ed9e1`](https://github.com/Dannebicque/intranetV3/commit/e4ed9e1d25dd89ef345de107eaf11bfb58e5af2b)
- Exports enquête diplômés [`dafb590`](https://github.com/Dannebicque/intranetV3/commit/dafb590407eba77992c79188dcd5e84151996c1a)
- Attestations COVID [`dea7a73`](https://github.com/Dannebicque/intranetV3/commit/dea7a73f33ef7026d4458cc796e45a4261c1a9c9)
- Typo mail gestion des stages [`6c09d32`](https://github.com/Dannebicque/intranetV3/commit/6c09d32f36d97b2380a56019beabfc1aa435cf6a)
- Export Apogee [`1d6861b`](https://github.com/Dannebicque/intranetV3/commit/1d6861b450f24be9e03262b3aec9d3b3c557a6e8)
- Problème non fichier export [`56548e1`](https://github.com/Dannebicque/intranetV3/commit/56548e1043534c0e7ac53a3cd467f76df7683eef)
- readme [`b87f9e2`](https://github.com/Dannebicque/intranetV3/commit/b87f9e277ed2dab1c637f150a0306af2c7fdff97)
- Export Apogee [`963a84a`](https://github.com/Dannebicque/intranetV3/commit/963a84ab6cfcaf5426536bccc58fe384f0f0305f)
- Problème accent nom ficher [`4e55820`](https://github.com/Dannebicque/intranetV3/commit/4e55820d178bd25531e76563d6086b728943ad21)
- Export Apogee [`7e35006`](https://github.com/Dannebicque/intranetV3/commit/7e350068faf75d209a6b6f3c44b16e6f004fe063)
- Génération de mot de passe [`2930260`](https://github.com/Dannebicque/intranetV3/commit/29302608a6d723acfe16b516d6fa13096f1dd572)
- Bug etudiant démissionnaire [`dbf63f0`](https://github.com/Dannebicque/intranetV3/commit/dbf63f0a1b5d2deac6e66ad13a5b5088b785697a)
- Bug export Ical [`470e853`](https://github.com/Dannebicque/intranetV3/commit/470e853772e67dbdd764b2beb7050e59068db934)
- Export [`c2b6b65`](https://github.com/Dannebicque/intranetV3/commit/c2b6b6564170dfed4403f9796a793d96832c9f8f)
- Script Celcat [`9e0d7ba`](https://github.com/Dannebicque/intranetV3/commit/9e0d7ba4c64162e8a31da9d6f89dd5d3e4625316)
- Script Celcat [`514f283`](https://github.com/Dannebicque/intranetV3/commit/514f283b754efeccb7d88cbf70566c0bf7ed1483)
- Bug export trombi PDF [`5dfc1c1`](https://github.com/Dannebicque/intranetV3/commit/5dfc1c1513c2462a82cf0cc7dbb46ce039fba5f1)
- Docker local pour RabbitMq et MailCatcher [`2ec9850`](https://github.com/Dannebicque/intranetV3/commit/2ec9850c61fb887d6ba6a4c98a39c16b364bc6c8)
- Sous Comm [`b30629f`](https://github.com/Dannebicque/intranetV3/commit/b30629f82d0e60d4c54818ebbf027802d330ed8e)
- Sous Comm [`f901a59`](https://github.com/Dannebicque/intranetV3/commit/f901a596305bdaf0cd3ebb29f5c4f97ee3b79c64)
- Sous Comm [`d24fc51`](https://github.com/Dannebicque/intranetV3/commit/d24fc51e537069ce1023986f4a316c8c74a8fb09)
- Gestion des téléphones commencant par 33 [`729593f`](https://github.com/Dannebicque/intranetV3/commit/729593f8c6837dbd2b6fb9cceaca03a44585af06)
- Ajout de datatable [`f540c60`](https://github.com/Dannebicque/intranetV3/commit/f540c60f262cf184b327225c32ec2eae5f4b577c)
- Date relevés avec l'export console [`7f54b00`](https://github.com/Dannebicque/intranetV3/commit/7f54b00a010cb02c99d7d17e93f3b3ea99604bda)
- export relevés [`7b67354`](https://github.com/Dannebicque/intranetV3/commit/7b6735488ed34f411675ff2489a92fab23f8768d)
- Amélioration sous-commission/grand jury [`800fb21`](https://github.com/Dannebicque/intranetV3/commit/800fb2106cd22a6c416075dd25437bf7cc04c7c8)
- Export RDD/Enquete [`83a1baa`](https://github.com/Dannebicque/intranetV3/commit/83a1baa0048d51437e77fec4032e2add32145141)
- Sous Comm [`2b6443a`](https://github.com/Dannebicque/intranetV3/commit/2b6443ac8225aef0ac257f346c938c40a6030447)
- Exports enquête diplômés [`0b10703`](https://github.com/Dannebicque/intranetV3/commit/0b107030c0ac9bb3ab17a13e6a7f29cb267cd697)
- Bug typo convention de stage [`7b131fa`](https://github.com/Dannebicque/intranetV3/commit/7b131fa38bc28af7f86503d314be3da4d7c15858)
- bug date EDT [`d10346a`](https://github.com/Dannebicque/intranetV3/commit/d10346ae04759aaa761493bb0323935af00f1d5b)
- Import APogée [`2b0a8e6`](https://github.com/Dannebicque/intranetV3/commit/2b0a8e6b95478140c75e9b0d2f15da67defa6f27)
- Script Celcat [`46b52c8`](https://github.com/Dannebicque/intranetV3/commit/46b52c8e0d92ac7548d01c82b4dc911addbf3e3e)
- Couleur export notes. [`7350c23`](https://github.com/Dannebicque/intranetV3/commit/7350c2399405201867f8e2ab1a84ac88dcfe5935)
- Correctifs sous-comm (semestres précédents et ue) et export [`f18c76b`](https://github.com/Dannebicque/intranetV3/commit/f18c76b5055b70b453a8be6eaf717b7a5bc9a0f3)
- Date [`bc52ae6`](https://github.com/Dannebicque/intranetV3/commit/bc52ae62ad3363bbc82c1d20ed9de7b2ebfad830)
- Sous comm [`fd931d9`](https://github.com/Dannebicque/intranetV3/commit/fd931d96fc720a39d618260b72fb63bd2ec1ab70)
- Enquêtes diplômés. Listing. [`71f64aa`](https://github.com/Dannebicque/intranetV3/commit/71f64aa6218efda98c0f950dbc4e28db0d290490)
- Echo qui traine [`575e979`](https://github.com/Dannebicque/intranetV3/commit/575e979885757ca7da53cfd764aaeed7166f7350)
- Bug décalage heure d'été Ical [`fe6eca6`](https://github.com/Dannebicque/intranetV3/commit/fe6eca64378d83d92b5b59f294d7ff86516e8d89)
- Bug export Trombi [`715a10f`](https://github.com/Dannebicque/intranetV3/commit/715a10fa3bade042676dc9b8963a0987dbc3cb91)
- Bug export Enquete [`9eb64b6`](https://github.com/Dannebicque/intranetV3/commit/9eb64b65d9dedeac7d0df4809de6e36cf92ebbd7)
- Sentry [`dff234d`](https://github.com/Dannebicque/intranetV3/commit/dff234d2c5a60939cbb725b529434b6ef326d301)
- Export [`c9e4264`](https://github.com/Dannebicque/intranetV3/commit/c9e4264da8edf34a04c3a40ce60d88089a6ab718)
- Script Celcat [`3cd2fc4`](https://github.com/Dannebicque/intranetV3/commit/3cd2fc481b4771884a05c1e34becb41c7f416e6a)
- Script Celcat [`f5b8a8c`](https://github.com/Dannebicque/intranetV3/commit/f5b8a8c971fa8d6d06f33b726698f05c2a4bcf90)
- Problème extension pdf [`1cd4c8e`](https://github.com/Dannebicque/intranetV3/commit/1cd4c8ef7679235eff166f2b947e622241c2212b)
- Sous-Comm statistiques [`4360849`](https://github.com/Dannebicque/intranetV3/commit/4360849d03aef88db8c0f3f385fb3f162534d950)
- Sous-Comm statistiques [`86a52a8`](https://github.com/Dannebicque/intranetV3/commit/86a52a89ffefe30961eb005e152ae84aafcf27b0)
- Typo nom du zip [`412f588`](https://github.com/Dannebicque/intranetV3/commit/412f58852deb34c3005a1d6795de362cbe9394af)
- export relevés [`f5a83a6`](https://github.com/Dannebicque/intranetV3/commit/f5a83a6ae141bd00017be47dab3fa5c36f626f04)
- Update COVID [`abefe62`](https://github.com/Dannebicque/intranetV3/commit/abefe62cfabf18a26b03aebd1b105b253c4add79)
- Gestion des téléphones commencant par 33 [`17b9784`](https://github.com/Dannebicque/intranetV3/commit/17b9784e78718727535641bcc000e81475e44eaa)
- Type variable [`e89ce4b`](https://github.com/Dannebicque/intranetV3/commit/e89ce4b802399c155760ae4ea4de4eb59534fb59)
- Export [`40b4c38`](https://github.com/Dannebicque/intranetV3/commit/40b4c3896ea571e0ed865a61dadf8018b9151f32)
- Script Celcat [`c71c60b`](https://github.com/Dannebicque/intranetV3/commit/c71c60bbe537c5cb773c393c146854b4dca5f95b)
- Export Apogee [`6bc4752`](https://github.com/Dannebicque/intranetV3/commit/6bc475257397abce42a1864a4538496086746c0e)
- Bug Export [`1e56266`](https://github.com/Dannebicque/intranetV3/commit/1e56266d8f25d95825643ac610f2b4d4277fa801)
- Bug import EDT [`99cb9ce`](https://github.com/Dannebicque/intranetV3/commit/99cb9ce49120a4bbaa8e9572ada0b29493e7474f)
- Bug qualité [`6f63fb4`](https://github.com/Dannebicque/intranetV3/commit/6f63fb4df5a131069bcd42c9224c44058d3863d4)
- Export Apogee [`89630be`](https://github.com/Dannebicque/intranetV3/commit/89630be606d63f3dd42892665080c97b15b325a1)
- Bug synchro iCal [`6b0b66c`](https://github.com/Dannebicque/intranetV3/commit/6b0b66c6bc0c69e5c0dc3fb7c1eb4b3ac0a0ea97)
- import étudiant Apogée [`a46f31a`](https://github.com/Dannebicque/intranetV3/commit/a46f31a48a282684451ce6651d7b4a993db47827)
- Script Celcat [`b40191e`](https://github.com/Dannebicque/intranetV3/commit/b40191e93fb1fa9f6ef27c12b817584de06b1f89)
- Script Celcat [`8d05d73`](https://github.com/Dannebicque/intranetV3/commit/8d05d733be17aeb9fdc88a1a289140934d97a387)
- Export PDF profs [`f980b63`](https://github.com/Dannebicque/intranetV3/commit/f980b639e4d9cd94f83ed953baa0abb3523288ed)
- Export problème de merge dans l'excel [`4f0f37c`](https://github.com/Dannebicque/intranetV3/commit/4f0f37cc87bfd1c6804941921c8235408dcd5a8c)
- Couleur export notes. [`308fd24`](https://github.com/Dannebicque/intranetV3/commit/308fd24d36d086fe097d296422f1151c34678208)
- Export EDT [`ef3667b`](https://github.com/Dannebicque/intranetV3/commit/ef3667ba4613a955fe5adc1653d03b1e86e16a81)
- Bug Export [`591165d`](https://github.com/Dannebicque/intranetV3/commit/591165d066ea8d70e9081a6fbd9b33558357f9a7)
- Export RDD/Enquete [`176946a`](https://github.com/Dannebicque/intranetV3/commit/176946a09baf70b4e2bb7c11ba07a4ddaf60362a)
- Sous Comm [`21d1b58`](https://github.com/Dannebicque/intranetV3/commit/21d1b5895db934faa8ea8df47f71ce9caa13e854)
- Exports enquête diplômés [`e1b3eca`](https://github.com/Dannebicque/intranetV3/commit/e1b3eca36dc578534e458b0c3ebdce436f264dcc)
- Bug sous-commission si option [`9c59c29`](https://github.com/Dannebicque/intranetV3/commit/9c59c29784eb82350a1f6b1224086074371f5733)
- Enquete diplomes [`54929d6`](https://github.com/Dannebicque/intranetV3/commit/54929d60f9176dfcfa5ddbe9df038a6d1508e1fd)
- Bug typage note. [`2e7f7f7`](https://github.com/Dannebicque/intranetV3/commit/2e7f7f7f074d2866e5c0d4dff863445c40e1c93a)
- Security [`cac9ea3`](https://github.com/Dannebicque/intranetV3/commit/cac9ea3c72392b89b7335fb6d9347db1944530a6)
- Export [`739821f`](https://github.com/Dannebicque/intranetV3/commit/739821f679c6ec63c2288f36ce5d39cd17faacb9)
- Typo profil [`ae65752`](https://github.com/Dannebicque/intranetV3/commit/ae6575273da80acdaf66161028f06865c5b10cee)
- Lisibilité période de stage [`dd97562`](https://github.com/Dannebicque/intranetV3/commit/dd975622ec3754d41911d486ad15240ec7a98b5e)
- Enquete diplomes [`d8ae2c2`](https://github.com/Dannebicque/intranetV3/commit/d8ae2c2f9ef95b873552c08997a5a73b70ab02ad)
- Problème lien erroné [`58be7df`](https://github.com/Dannebicque/intranetV3/commit/58be7dffc70f47885adc02def171ad71ddebee70)
- Sentry [`2b54219`](https://github.com/Dannebicque/intranetV3/commit/2b5421976a6c8cec5009a11e4e2896560eba79d5)
- Typo traduction [`68a2142`](https://github.com/Dannebicque/intranetV3/commit/68a2142ecfa058203d24c0818e6938ea232d5da8)
- Date relevés avec l'export console [`a4b5ac7`](https://github.com/Dannebicque/intranetV3/commit/a4b5ac7d9406eadde4078306f9ddb52622abf774)
- Mise en avant des matières suspendues [`0901c1b`](https://github.com/Dannebicque/intranetV3/commit/0901c1be95af152df98b6ac75b0da530eeb83b44)
- Enquete diplomes [`d0d0059`](https://github.com/Dannebicque/intranetV3/commit/d0d005975f9b883b2497d14d251b2141cc0f427b)
- Accès partie fin de semestre aux responsables des notes [`ce16caa`](https://github.com/Dannebicque/intranetV3/commit/ce16caac92d5930397ac4613f5e4bb1d008f2d72)

## [v1.0.3](https://github.com/Dannebicque/intranetV3/compare/1.0.2...v1.0.3) - 2020-12-23

### Commits

- Tag [`8692e63`](https://github.com/Dannebicque/intranetV3/commit/8692e63090ccc1f4af6333ee7ba925adbd65c3bb)

## [1.0.2](https://github.com/Dannebicque/intranetV3/compare/1.0.1...1.0.2) - 2020-12-22

### Merged

- Bump ini from 1.3.5 to 1.3.7 [`#49`](https://github.com/Dannebicque/intranetV3/pull/49)
- Integrating Code Climate [`#36`](https://github.com/Dannebicque/intranetV3/pull/36)
- Go to webpack [`#29`](https://github.com/Dannebicque/intranetV3/pull/29)

### Commits

- Correction Enquête avec pb de session [`47ba587`](https://github.com/Dannebicque/intranetV3/commit/47ba58743636a665a73447b2edbf943893e11994)
- Mise en place du graphique radar sur le profil [`8b3e3db`](https://github.com/Dannebicque/intranetV3/commit/8b3e3db8e34fb5b558bf91c4b4e397a6a44b0f47)
- Insallation de Symfony UX/Chartjs + 5.2 [`89d4dd3`](https://github.com/Dannebicque/intranetV3/commit/89d4dd398393b75c6f89abcea92ce50703df1b48)
- Accès LDAP pour admin et administratifs [`fe3a007`](https://github.com/Dannebicque/intranetV3/commit/fe3a0078ab395dfb83f04941f7986362b97c1cd4)
- Mise en place d'une aide en interne [`ab2d92f`](https://github.com/Dannebicque/intranetV3/commit/ab2d92f1eb187026098d2b9737e17c547aa2189d)
- Structure du référentiel APC [`a00436d`](https://github.com/Dannebicque/intranetV3/commit/a00436d02f0a08fa722ade802403e068f1e462b0)
- Correctif groupe par défaut dans le trombinoscope [`a76cab6`](https://github.com/Dannebicque/intranetV3/commit/a76cab64d0a6dcce696c1b87d3bb4bc203f70fb8)
- Bug EDT créneau manquant [`f0c496b`](https://github.com/Dannebicque/intranetV3/commit/f0c496b86ce39549fe8ce4c5dea10aec20071911)
- Remove PhpTranslation. Update composer [`f3ad035`](https://github.com/Dannebicque/intranetV3/commit/f3ad03541375fb7478bc41c036e186ec538f4637)
- Bug questionnaire qualité [`80485b4`](https://github.com/Dannebicque/intranetV3/commit/80485b44717fb17ca7091672210e1b4a5da3e359)
- Mise en place d'une aide en interne [`36d63c6`](https://github.com/Dannebicque/intranetV3/commit/36d63c67ce9b9fb1f351e60ddc419dd175a2a017)
- Suppression de todo [`8e5832a`](https://github.com/Dannebicque/intranetV3/commit/8e5832a63d82dcdc8ae200f9d1cc1734280e6e8b)
- Mise en place de la partie RDD et enquète diplômés. [`0c30090`](https://github.com/Dannebicque/intranetV3/commit/0c300904aa8535d3aa01b5410ebdaa23193cb83a)
- Application attestation covid [`2d54a9d`](https://github.com/Dannebicque/intranetV3/commit/2d54a9dc71a749fad875dbce9df93d4762b92850)
- Calcul sous-Commission travail. Simplification de la structure de BDD [`42e74ed`](https://github.com/Dannebicque/intranetV3/commit/42e74ed92b4ec4cc47ac9629cc73a76f842a5191)
- Nouvelle convention de stage [`04f2dc1`](https://github.com/Dannebicque/intranetV3/commit/04f2dc13e22abde8f847e80bc44c76f759a63c00)
- Gestion des avenants de stage [`c543b4b`](https://github.com/Dannebicque/intranetV3/commit/c543b4b4d76656881e3fc86ef2dda83ba7505e34)
- Convocation étudiant COVID [`d480062`](https://github.com/Dannebicque/intranetV3/commit/d480062986f5666505d9de0a518cac22f99e2839)
- Bug calcul moyenne. Import de parcours. [`108ce0d`](https://github.com/Dannebicque/intranetV3/commit/108ce0d00d045a403e311f9ddf26c20375947e1f)
- Enquete + RDD [`638a6ef`](https://github.com/Dannebicque/intranetV3/commit/638a6efc1becd1d9d242f13161933ed87344a2c8)
- Amélioration Code Quality [`854e729`](https://github.com/Dannebicque/intranetV3/commit/854e7299453f062fab354b11a8f5032ce3899493)
- Ajout d'un graphique de répartition sur la synthèse des notes. [`7ca3d96`](https://github.com/Dannebicque/intranetV3/commit/7ca3d966b32bf2535ef1a98eb17ee925a6c1f068)
- Correctifs sur les personnels + notes [`45dba75`](https://github.com/Dannebicque/intranetV3/commit/45dba75cee585520204db70c9efea9b3f16fa09a)
- Application covid etudiant [`8af21a8`](https://github.com/Dannebicque/intranetV3/commit/8af21a851456c1463e2498d9577fa8e38c6dd56b)
- Correctifs qualité de code [`aa9f303`](https://github.com/Dannebicque/intranetV3/commit/aa9f30367c833c90dd8eb75ed6986d5faf699286)
- Sous commission publier/dépublier [`9ccf159`](https://github.com/Dannebicque/intranetV3/commit/9ccf15931aa1d934ddd1c35065027eb623449222)
- Détails d'un stage en mode étudiant [`85c1162`](https://github.com/Dannebicque/intranetV3/commit/85c1162822898157750a355bf55860e395ff7d9b)
- Export Stage [`46e9378`](https://github.com/Dannebicque/intranetV3/commit/46e93786e06ea61a379bc77189073e3751db5c10)
- Enquete + RDD [`b11171c`](https://github.com/Dannebicque/intranetV3/commit/b11171c3c77843831adebb76e1f7438e1b9dc7ff)
- Qualité de code [`47001f4`](https://github.com/Dannebicque/intranetV3/commit/47001f4d3464c00db48998b6fb445d65a7a8b7db)
- Export infos RDD [`8101e5d`](https://github.com/Dannebicque/intranetV3/commit/8101e5dc3d70bd85f277bbbae3b249abcf27e061)
- Modification scolarité [`3480d11`](https://github.com/Dannebicque/intranetV3/commit/3480d11ac906572e4baf93d68b68cb0d6571c1d8)
- Relevé note définitif [`a059234`](https://github.com/Dannebicque/intranetV3/commit/a05923439e1baff09f5b8ec4868cfefecf9c98f2)
- Accès qualité sur l'intranet. [`5e24847`](https://github.com/Dannebicque/intranetV3/commit/5e2484787676d171366a399fe67eb9948f371850)
- Relevé de note définitif [`78be4c3`](https://github.com/Dannebicque/intranetV3/commit/78be4c39659b1190e970bd3362a1194c930d1a17)
- Bugs Emprunt Matériel [`9fc7f9f`](https://github.com/Dannebicque/intranetV3/commit/9fc7f9f121f9b042833dfa8bc1a2b60050f1c421)
- Rdd + enquete [`9afdd7d`](https://github.com/Dannebicque/intranetV3/commit/9afdd7ddb0275afb523ae4a3dc7741b5fcbd2da7)
- Export des demandes d'attestation [`e9f25a6`](https://github.com/Dannebicque/intranetV3/commit/e9f25a665c54179648485078cfa8a65a0bc9ae1b)
- Export RDD [`7fe9f86`](https://github.com/Dannebicque/intranetV3/commit/7fe9f862cd8436f82dc0c59a1ce5bd9fa2329a1c)
- Export toutes les notes [`35d25b0`](https://github.com/Dannebicque/intranetV3/commit/35d25b020d42b51bfe99f774c3debe9391baf02d)
- Partie questionnaires Qualités pour les étudiants [`d6c33a1`](https://github.com/Dannebicque/intranetV3/commit/d6c33a101490755df8bef7ad53c17da8b916ddfc)
- Notif + email lors de la publication d'une sous commission [`3ad6f4f`](https://github.com/Dannebicque/intranetV3/commit/3ad6f4faf3137a8ab2b49205d6823b3e23b9ac73)
- Suppression du code commenté [`107d207`](https://github.com/Dannebicque/intranetV3/commit/107d20712fe9f4445532f8fe0082a663e6fc0d38)
- Application covid etudiant [`c3e6b8b`](https://github.com/Dannebicque/intranetV3/commit/c3e6b8bb7004d53f11435f45c8ad5143288028ec)
- Bugs Exports listing [`3d8c9b2`](https://github.com/Dannebicque/intranetV3/commit/3d8c9b232984ef472955a42ed74286083a482bc2)
- Export Sous comm. [`9128fe6`](https://github.com/Dannebicque/intranetV3/commit/9128fe6d774bdd4e2edac21d72f889258d22e411)
- Attestation déplacement [`5d66bb0`](https://github.com/Dannebicque/intranetV3/commit/5d66bb00a1607b2f4a6e1311e7b4af20b1603a7b)
- Application attestation covid [`b8ea171`](https://github.com/Dannebicque/intranetV3/commit/b8ea1716df69b963a07ad57e26d85f6a694a82a2)
- Génération du PDF + ajout en PJ du mail [`d00e4f8`](https://github.com/Dannebicque/intranetV3/commit/d00e4f8efb4dd0d62ca806111fa8ecdc5e4f5262)
- Enquete (mise à jour suite restructuration BDD) [`b982aef`](https://github.com/Dannebicque/intranetV3/commit/b982aef81213b36cf7eb1bc44687734e71dc8847)
- Export RDD [`ceb0629`](https://github.com/Dannebicque/intranetV3/commit/ceb06294b53542246a15fd9b31dead1ca605ac25)
- Traduction + readme [`42dbf70`](https://github.com/Dannebicque/intranetV3/commit/42dbf7004f66509f6415dc7f5d8b1feee65ed781)
- Page de maintenance [`0fb99ca`](https://github.com/Dannebicque/intranetV3/commit/0fb99caf3009e807631aae3a3acfbf823f500554)
- Fin de semestre et pré-remplissage [`8064866`](https://github.com/Dannebicque/intranetV3/commit/8064866e85ef42046d34fe0d6400476f6a38a91e)
- Fin de semestre et pré-remplissage [`a167e8d`](https://github.com/Dannebicque/intranetV3/commit/a167e8df6189e1b3ad91137c7407445ce210b280)
- Ajout de la PJ obligatoire + typos dans mails [`6b5a582`](https://github.com/Dannebicque/intranetV3/commit/6b5a5829b75dd059baa25bbd3d5c209c07545009)
- Bug lien notification [`71550d6`](https://github.com/Dannebicque/intranetV3/commit/71550d6aa9afe4cd38ae245d2e580c9c55fed7e0)
- Ajout d'étudiant en admin [`a410f03`](https://github.com/Dannebicque/intranetV3/commit/a410f03e9a2c5e6976524356c8577d7175563dd4)
- Export infos RDD [`37e1d6e`](https://github.com/Dannebicque/intranetV3/commit/37e1d6e8e9634d55ac9c5c0e6352bf24b2b28e22)
- Convocation étudiant COVID [`30035db`](https://github.com/Dannebicque/intranetV3/commit/30035db21937507c168be3d5fa9f4964b905ec61)
- Bug destinataire en copie. [`ac985e4`](https://github.com/Dannebicque/intranetV3/commit/ac985e48c72523f7e2869e950ccf32b38cc39c4d)
- Ajout de la suppression d'un emprunt [`ff47f58`](https://github.com/Dannebicque/intranetV3/commit/ff47f58d547e9641ce19271a611e54ce66e38d3e)
- Questionnaire ajout masquage [`ec5358d`](https://github.com/Dannebicque/intranetV3/commit/ec5358da18c657c5c75ca37eda702ba7dffe7ace)
- Suppression des demandes [`25a9872`](https://github.com/Dannebicque/intranetV3/commit/25a9872285d39725cbf47c208086c761ea4f0317)
- Page de personnel en admin [`2fff262`](https://github.com/Dannebicque/intranetV3/commit/2fff262202a9a4f221b4c0f7c20a03ca85775215)
- Traitement du cas exaequo [`76f2ec8`](https://github.com/Dannebicque/intranetV3/commit/76f2ec8555eb288435defaacddf69999bcd6d374)
- Set theme jekyll-theme-slate [`1740751`](https://github.com/Dannebicque/intranetV3/commit/17407513787fa2f3723bdeb659aacb5153ef138c)
- Buf suppression d'un document [`14f2c9e`](https://github.com/Dannebicque/intranetV3/commit/14f2c9ed043a244544eb19017b295d74f5591bd9)
- Entité Questionnaire [`d0dedb0`](https://github.com/Dannebicque/intranetV3/commit/d0dedb0dbb41af5943f87a1ef56fb74aa54d24cb)
- Ajout des groupes + tris sur rattrapage [`fd9a555`](https://github.com/Dannebicque/intranetV3/commit/fd9a555af4413730ea2f91aec37dad5b74fd1409)
- Suppression traduction modèle import note [`573e6af`](https://github.com/Dannebicque/intranetV3/commit/573e6af3a2817e6f4170eec9351b890bb68d5938)
- Attestation déplacement [`c0c10b0`](https://github.com/Dannebicque/intranetV3/commit/c0c10b0c2b78e78c10946fed716e6d78974abbcd)
- Tri date [`f3be978`](https://github.com/Dannebicque/intranetV3/commit/f3be978f18262c6fb80b09ef1d42395be46106cd)
- Imports parcours scolarités depuis intranet V2 [`b24c977`](https://github.com/Dannebicque/intranetV3/commit/b24c977eed672095370234640de0c4c496eaf5f6)
- Correctif ordre des paramètres obligatoires/facultatifs EDT [`83fa2f6`](https://github.com/Dannebicque/intranetV3/commit/83fa2f6baf87f4c963ff863934406cc74700e76a)
- Typo sur la convention et siret optionnel [`b7efd9a`](https://github.com/Dannebicque/intranetV3/commit/b7efd9a4d7b6bfaa95a020c2ad3901d222cf4027)
- Correctifs fiches suivis [`5976766`](https://github.com/Dannebicque/intranetV3/commit/5976766e993b09db72e88ee411e7b7a79d18ef99)
- Suppression de echo/dump [`bd04553`](https://github.com/Dannebicque/intranetV3/commit/bd045537409f0ee897119420529cbb8c4d058852)
- Application attestation covid [`7f26455`](https://github.com/Dannebicque/intranetV3/commit/7f26455a7308aaee8cec9e68628a626dcf41b860)
- Application attestation covid [`e5c0590`](https://github.com/Dannebicque/intranetV3/commit/e5c05900e91a314a289666d4d51bd42ebee26775)
- Correctif duplicate stage Avenant [`0d3b0ed`](https://github.com/Dannebicque/intranetV3/commit/0d3b0ed4e6bb29c3be1711941d411a92aa155e0a)
- Lien site univ si vide [`00e30cf`](https://github.com/Dannebicque/intranetV3/commit/00e30cf1fdc6d1cea4f64f67cbb4dd15f2ee4c9c)
- Page de maintenance [`e0734f6`](https://github.com/Dannebicque/intranetV3/commit/e0734f659ebaa80b13e323418c07d727255d5fcf)
- Correctif formulaire rattrapage avec champs obligatoires [`e9dd557`](https://github.com/Dannebicque/intranetV3/commit/e9dd557a47469937a0a12d74c6be7e4c260b7c29)
- Application attestation covid [`5340d0b`](https://github.com/Dannebicque/intranetV3/commit/5340d0ba0945589b6ae8e963ad908065b8e8f39f)
- Typo application COVID [`c66abe0`](https://github.com/Dannebicque/intranetV3/commit/c66abe04df7f321d46daaad53aa77b3b96507696)
- Erreur 666 =&gt; 403 [`c1117e6`](https://github.com/Dannebicque/intranetV3/commit/c1117e6841bd919fffdb76d2b1e3e7e3ec6e05c4)
- Bug import des notes [`74bb96c`](https://github.com/Dannebicque/intranetV3/commit/74bb96c8fd7758003a75fc8a3308792225c39211)
- Service par matière depuis Celcat [`10461d7`](https://github.com/Dannebicque/intranetV3/commit/10461d7950e0eab9464da6ef60d8c3ce3ab29597)
- Page de maintenance [`fda54ad`](https://github.com/Dannebicque/intranetV3/commit/fda54ad4d89884243151c1e9028d0976ac1a3c99)
- Bug fichier Excel [`3877b78`](https://github.com/Dannebicque/intranetV3/commit/3877b7820e705d3866ce813c36994bdd717349a6)
- Erreur calcul moyenne UE ou matière [`018e3e8`](https://github.com/Dannebicque/intranetV3/commit/018e3e858c274fda54106d4e845475cd8fb277a0)
- Application attestation covid [`102a49f`](https://github.com/Dannebicque/intranetV3/commit/102a49f4024b41290c78b88c45c9cc2e4c21c696)
- Ajout des champs entreprise pour les publipostages [`3f024c1`](https://github.com/Dannebicque/intranetV3/commit/3f024c1a5149822b91908740101e49c8dfb8a429)
- Bug convention de projet [`b148dca`](https://github.com/Dannebicque/intranetV3/commit/b148dca830868994c64003c3c6ab6ab7ec136be5)
- Typo application COVID [`9b3cd92`](https://github.com/Dannebicque/intranetV3/commit/9b3cd92a8a20a1ffd8418ff89b2b5824bd246d5e)
- Visibilité des évaluations depuis la partie prof. [`d9770b8`](https://github.com/Dannebicque/intranetV3/commit/d9770b8df4e074512fc7c046e8199e0b05907de3)
- Type + lisibilité édition Evaluation [`8c770ff`](https://github.com/Dannebicque/intranetV3/commit/8c770ff9450a01cd1659a1ddb14d18f411d496ce)
- Ajout des champs entreprise pour les publipostages [`52c5a54`](https://github.com/Dannebicque/intranetV3/commit/52c5a543691f61aaece27dae6ea391ac1d83c039)
- Bug affichage stage [`04069cc`](https://github.com/Dannebicque/intranetV3/commit/04069cc0f03d21f909504badc20fee9f9c202990)
- Application attestation covid [`a9c58e7`](https://github.com/Dannebicque/intranetV3/commit/a9c58e7955ffacfd001f17f4b1590a047c8ae6f1)
- Application attestation covid [`517397d`](https://github.com/Dannebicque/intranetV3/commit/517397d835c425da78ad710e37a388bbcf82bf9e)
- Correctifs sur les personnels + notes [`d469a5b`](https://github.com/Dannebicque/intranetV3/commit/d469a5b6d1f77b599039911d180c2852cd30ff6c)
- Typo dans la convention de stage [`66dd650`](https://github.com/Dannebicque/intranetV3/commit/66dd650df46c7e6a0f2e4d427d3fe3ca00e9c158)
- Bug date personnel [`2eb642d`](https://github.com/Dannebicque/intranetV3/commit/2eb642dd1584b51e6847b36cc225a1e4f7b3f94b)
- Ajout des champs entreprise pour les publipostages [`5d9d93a`](https://github.com/Dannebicque/intranetV3/commit/5d9d93a4e42506d134ca1d6b7cf07b7a278dd05e)
- Bug RP alternance [`77d0868`](https://github.com/Dannebicque/intranetV3/commit/77d08685f914190053e03c019e7063dab781b46f)
- Tri des matières par ordre de code [`6af7b09`](https://github.com/Dannebicque/intranetV3/commit/6af7b0964e883e8c4de3c193f4abf46f06898803)
- Bug lien notification [`4a0f2ba`](https://github.com/Dannebicque/intranetV3/commit/4a0f2ba323a0022aeff04d5fcf8f089d5db0d5e8)
- Suppression des demandes [`0edce2b`](https://github.com/Dannebicque/intranetV3/commit/0edce2b629d050900ccbe5fce923151b9604000a)
- Suppression des demandes [`1453c11`](https://github.com/Dannebicque/intranetV3/commit/1453c11c47dc8ca6d9b6150bee9eeb12261677e9)
- Application covid etudiant [`522c1dc`](https://github.com/Dannebicque/intranetV3/commit/522c1dc020d12ab5048bad0bb3d241b378fb8ff1)
- Génération du PDF + ajout en PJ du mail [`b24ba96`](https://github.com/Dannebicque/intranetV3/commit/b24ba96aeab42f2857342611d7267b4d0884e6d8)
- Application attestation covid [`ba6df50`](https://github.com/Dannebicque/intranetV3/commit/ba6df50b18446798743a27cf0dfa6fda3aacf6e3)
- Génération du PDF + ajout en PJ du mail [`2c3a60d`](https://github.com/Dannebicque/intranetV3/commit/2c3a60dee410f309f8b5cf4558f023c36ff9c6d3)
- Bug affichage notes étudiants [`1060dc6`](https://github.com/Dannebicque/intranetV3/commit/1060dc6cda302a7a9d5ea123d3262d97e8fd816c)
- Correctifs sur les personnels + notes [`07c450c`](https://github.com/Dannebicque/intranetV3/commit/07c450c985452f0c5dd2bf3b673d835a48fc0f86)
- Bug planning [`b81dfed`](https://github.com/Dannebicque/intranetV3/commit/b81dfedaad1790f31426100b5180c4b3bc082789)
- Bug RP alternance [`c52c488`](https://github.com/Dannebicque/intranetV3/commit/c52c4882574fcd87ecfab77d5472f7a278c0972a)
- Format heure créneaux COVID [`89bc41e`](https://github.com/Dannebicque/intranetV3/commit/89bc41edfa0dcd0ff675897297cdcbcd217fd39a)
- Typo listing prof. [`14408ab`](https://github.com/Dannebicque/intranetV3/commit/14408aba04c355c35fc890dfb2e97e8b3585b0fa)
- Bug étrange année universitaire alternance. [`24ca139`](https://github.com/Dannebicque/intranetV3/commit/24ca139f52463d80c0ed63a61e1d74d2a38bfb98)
- Page de maintenance [`9324c11`](https://github.com/Dannebicque/intranetV3/commit/9324c119021aa1b78888834b030a5ef2dd77b221)
- Augmentation du time_limit [`22a5e54`](https://github.com/Dannebicque/intranetV3/commit/22a5e542f022d916425987b980dbb2322d262232)
- Ajout d'un motif [`e9b034a`](https://github.com/Dannebicque/intranetV3/commit/e9b034ad4f1efcdb13d051ad5ecf92f0d80fb9ad)
- Application attestation covid [`def1a29`](https://github.com/Dannebicque/intranetV3/commit/def1a292293bd9de312715316aaad9ec03789633)
- Typo sur la convention et siret optionnel [`8afaca4`](https://github.com/Dannebicque/intranetV3/commit/8afaca44a91d8d9603603d88f07160f67ffcc6be)
- Bug affichage notes étudiants [`83e9224`](https://github.com/Dannebicque/intranetV3/commit/83e9224abaf61120d69499befccfb4470a3bb89d)
- Visualisation de la partie notes pour GEA (responsable groupe TD) [`efb1d78`](https://github.com/Dannebicque/intranetV3/commit/efb1d7860ad78f436c65b363f49b58f983472154)
- Typo nb jours convention [`8116d6e`](https://github.com/Dannebicque/intranetV3/commit/8116d6e3d6ae2f03f565043c18dc6828e0226f64)
- Application covid etudiant [`d9f9bc6`](https://github.com/Dannebicque/intranetV3/commit/d9f9bc62a7d980e5409756678b558464d1599b4f)
- Typo application COVID [`482520c`](https://github.com/Dannebicque/intranetV3/commit/482520c2de877d3f153dc081d1099ec0cb701d41)
- Typo nb jours convention [`ba572fd`](https://github.com/Dannebicque/intranetV3/commit/ba572fdc84b39c5287c9ab0bdb447583bf9baf0a)
- Page de maintenance [`1daf5cc`](https://github.com/Dannebicque/intranetV3/commit/1daf5cc3b17912e1d8bf4015ea328f12266dd6e2)
- Typo sur la convention et siret optionnel [`9b8a69b`](https://github.com/Dannebicque/intranetV3/commit/9b8a69b4f21155cdfa245a271a5451c039866809)
- Update index.md [`3e1090a`](https://github.com/Dannebicque/intranetV3/commit/3e1090a0bd210f2dce88c4927146098b922a7504)
- QuickView [`6dd0a1c`](https://github.com/Dannebicque/intranetV3/commit/6dd0a1c7eea217ea567a1ae515401d2ad72fa2c1)
- Problème de floatant. Amélioration génération mail à partir d'un template [`d50f480`](https://github.com/Dannebicque/intranetV3/commit/d50f480905b90618f1d52cf2c8d0b79569e98060)
- Assets [`2f2369c`](https://github.com/Dannebicque/intranetV3/commit/2f2369c1fc9cd6ba58bcf7b40de3b209c0dd362e)
- Mise à jours assets en prof [`ee5e6d3`](https://github.com/Dannebicque/intranetV3/commit/ee5e6d3de42bbf4da9670889d80a3de0ae2a8613)
- Amélioration de l'edit en ligne avec textarea et CSS [`1adce3b`](https://github.com/Dannebicque/intranetV3/commit/1adce3b1f7ffc20c548ef9fa0ee30c3cef27336f)
- Assets prod [`6bdc4dd`](https://github.com/Dannebicque/intranetV3/commit/6bdc4dd7e54a2534c60dc696190ce7288a8bd6da)
- Scroll du menu [`50dd705`](https://github.com/Dannebicque/intranetV3/commit/50dd705d545f8e7df356da840d22ea575d0a37cc)
- Stages [`a867971`](https://github.com/Dannebicque/intranetV3/commit/a867971c2247dae40b51e432deba9b1108aaa1c7)
- Barre contextuelle + Version Agenda avec prévisionnel Chronologique par matière [`493f099`](https://github.com/Dannebicque/intranetV3/commit/493f099d5645a78f0771a3dbf91a79f18cbe92c6)
- Traductions avec Yaml et début de mise en cohérence [`64de4da`](https://github.com/Dannebicque/intranetV3/commit/64de4da8e046867de0c5587408d128d5ce5c27e0)
- Fichier de démo SQL [`69d729e`](https://github.com/Dannebicque/intranetV3/commit/69d729ed091a9974add99affa7b74c19e00fcd0f)
- Js [`0e3dc36`](https://github.com/Dannebicque/intranetV3/commit/0e3dc36d5d1dda3b9db47c597ba068898788a3b4)
- APC [`b6fe5f0`](https://github.com/Dannebicque/intranetV3/commit/b6fe5f0ecc52333c3911d6c00f0563d3a648ac7e)
- Nouvelle convention de stage [`9420915`](https://github.com/Dannebicque/intranetV3/commit/94209155a138c1580384ef9c0fcffc0554c8469d)
- Testing [`784f045`](https://github.com/Dannebicque/intranetV3/commit/784f045bc61ae6c15ab224c95df60768e6c14305)
- Bugsnag [`40ba7a8`](https://github.com/Dannebicque/intranetV3/commit/40ba7a86afbf7d1a97ecf48725a56835cb5924a9)
- Messagerie draft [`82d6864`](https://github.com/Dannebicque/intranetV3/commit/82d6864cbc40b2e53de2a3dd9ec2fc88f634f18d)
- Projet tutoré [`1b885e5`](https://github.com/Dannebicque/intranetV3/commit/1b885e5f5d3bcb9c4d642739fe8a03235f8d4252)
- Amélioration de l'edit en ligne avec textarea et CSS [`1916256`](https://github.com/Dannebicque/intranetV3/commit/19162565751b1d501df81cb8bb7a02965d5f6ae1)
- Evaluation + formulaire [`19b57b7`](https://github.com/Dannebicque/intranetV3/commit/19b57b737f79ecd0bf6007e674b36c4d70fda81b)
- QV partie Agenda [`1f03945`](https://github.com/Dannebicque/intranetV3/commit/1f03945d426dca72a21ac4f89800de356c19468b)
- GEstion des couleurs avec scss [`5f4349c`](https://github.com/Dannebicque/intranetV3/commit/5f4349c3d3b38d9a1c5cf78bd53fd7c7683784ca)
- Regenerate Uuid sur un clone [`bcc9cfa`](https://github.com/Dannebicque/intranetV3/commit/bcc9cfa7d8aa3ce87354a89a23dc3cecd99ea760)
- Edt PDF [`d3e8523`](https://github.com/Dannebicque/intranetV3/commit/d3e85239c84b582afad0adda7f32ea9dfde9a1a5)
-  Structure [`97e8104`](https://github.com/Dannebicque/intranetV3/commit/97e8104a9beb4be671b7cdacfcd857bc2ee222c1)
- Partie Stage [`5565d99`](https://github.com/Dannebicque/intranetV3/commit/5565d996b35457cf391c7ea78b2b58e995bf0379)
- Suppression des absences [`96f96bf`](https://github.com/Dannebicque/intranetV3/commit/96f96bff5c2e0b658aa039185bceefc0489b3ba1)
- Bug salle examen [`1aa686e`](https://github.com/Dannebicque/intranetV3/commit/1aa686e368aa6cf063c030ee0ee392680e2727e6)
- Fixtures et tests [`c7dce3c`](https://github.com/Dannebicque/intranetV3/commit/c7dce3c34e43d21694e60d4734333d7659ed2f04)
- Partie alternance [`e6da160`](https://github.com/Dannebicque/intranetV3/commit/e6da16005011ff91f9e875fbee31516333787a60)
- Partie stage et date + amélioration de mon ajax edit on line [`4137c6f`](https://github.com/Dannebicque/intranetV3/commit/4137c6ff08127842a807bd3ca3e108de58369c3a)
- Mise à jour assets [`f19c740`](https://github.com/Dannebicque/intranetV3/commit/f19c740a8b8460fb4fadf3cec8caaa3ca580c5ee)
- Assets [`6dd5fbd`](https://github.com/Dannebicque/intranetV3/commit/6dd5fbd1cadaa5fc91cb0949f7aa5297463189ad)
- Evaluation partie admin [`6dcb018`](https://github.com/Dannebicque/intranetV3/commit/6dcb018c375da24a1aba532bec32abdbb9eee5ab)
- Convention Corrigée [`838569d`](https://github.com/Dannebicque/intranetV3/commit/838569d7d98b70dfc332474361ed09e78023e3d6)
- Bug Constante : ETAT_STAGE_CONVENTION_IMPRIME [`e8b9cdb`](https://github.com/Dannebicque/intranetV3/commit/e8b9cdb2b9672cde8b2796d3df825bd54490ec96)
- Alternance suivi [`3344cc1`](https://github.com/Dannebicque/intranetV3/commit/3344cc104bd46edf0220a92de69d684c278006dc)
- Qualité [`cd3625f`](https://github.com/Dannebicque/intranetV3/commit/cd3625f46ab3bab17f49825d80477f6416a06209)
- Testing [`54fd424`](https://github.com/Dannebicque/intranetV3/commit/54fd424ab7fddecba894d9d8f76e56cc23ef4826)
- QV partie Agenda [`2409a0d`](https://github.com/Dannebicque/intranetV3/commit/2409a0dcaf27330b5620028aabe58eabc4b318f2)
- Maintenance page [`c421ccb`](https://github.com/Dannebicque/intranetV3/commit/c421ccb45128e02e16d4fed1032291bb37ee593d)
- Alternance [`0d1368e`](https://github.com/Dannebicque/intranetV3/commit/0d1368e043878fbc5961c7b48483257f5bbced7e)
- Divers correctifs [`229203e`](https://github.com/Dannebicque/intranetV3/commit/229203e2d9426a467a9435f99434640ead2b5f95)
- Update php.yml [`2a40c3b`](https://github.com/Dannebicque/intranetV3/commit/2a40c3bb4c4f543239b7cb7fa884687ca76c1af9)
- Problème agenda et JS [`03b4732`](https://github.com/Dannebicque/intranetV3/commit/03b4732568914fa3ed5cdbc8787c4072da5c5a82)
- fix readme and setup codeclimate [`657224b`](https://github.com/Dannebicque/intranetV3/commit/657224b633642d9eb86de0c8196ce5b6bd9aaf51)
- Bug salle examen [`a35b60d`](https://github.com/Dannebicque/intranetV3/commit/a35b60dfc1f7b82ce7537f92f98a14d447863e50)
- Courriers par défauts mails [`a916722`](https://github.com/Dannebicque/intranetV3/commit/a916722bf3984f222275f3b7ebb6b40eb22044ca)
- Logos courriers [`8c8958e`](https://github.com/Dannebicque/intranetV3/commit/8c8958ebb8670e741a5a3580df676fb9dda0ca16)
- Correctifs sur la doc [`b360904`](https://github.com/Dannebicque/intranetV3/commit/b360904bd4060708b28f99aef4aa4e9536905ada)
- Signature electronique [`bb6acaf`](https://github.com/Dannebicque/intranetV3/commit/bb6acaf9f9d6e62d366d8f9f74d3e34c2c837efe)
- traking interne [`149b502`](https://github.com/Dannebicque/intranetV3/commit/149b50238d172e3f6d4f4d477805ed08376f105c)
- Create php.yml [`270b77b`](https://github.com/Dannebicque/intranetV3/commit/270b77b62d51d24c046573f4d2db3c02b672c5a2)
- Mails vides stage [`17e045e`](https://github.com/Dannebicque/intranetV3/commit/17e045ebd6a904e47b01a84849828f9252b9da9f)
- Messagerie envois plusieurs groupes [`41efe4d`](https://github.com/Dannebicque/intranetV3/commit/41efe4d41571202662d752ae9905cbf505a2849a)
- Courrier [`59c750b`](https://github.com/Dannebicque/intranetV3/commit/59c750b872edc14d43b2cbd31aabc1e4fb87c94b)
- Mails et PJ [`8b071c6`](https://github.com/Dannebicque/intranetV3/commit/8b071c6a5e286f57741caa9dc160d8214e48fd1f)
- Assets bornes [`923241c`](https://github.com/Dannebicque/intranetV3/commit/923241c1bf2eedf65c210decfb7d0a66d287f49a)
- Maintenance page [`9cb378e`](https://github.com/Dannebicque/intranetV3/commit/9cb378ec947c7b9bb2d6cc8a902dffeecdec4b50)
- Stage mails sans "to" ? [`48150c9`](https://github.com/Dannebicque/intranetV3/commit/48150c93cbdbbf91b68e630bdfb7ce077799f18c)
- TravisCi [`a7ea70b`](https://github.com/Dannebicque/intranetV3/commit/a7ea70b925ba15ee7f95c9d28c88102bba949573)
- Problème de saisie des notes [`ff7b57c`](https://github.com/Dannebicque/intranetV3/commit/ff7b57c1bb404b7ea9faa34084b7d392d53bdb9c)
-  Problème id/uuid [`41aed70`](https://github.com/Dannebicque/intranetV3/commit/41aed70fe92a227c837d16b88ece6606626d6498)
- Force cache twig database [`5f11a34`](https://github.com/Dannebicque/intranetV3/commit/5f11a34c90d360a173053264251d77d6ce41f189)
- Logos courriers [`6e39a5f`](https://github.com/Dannebicque/intranetV3/commit/6e39a5f71e63e17472017d5a911d18bdfea07876)
- Barre contextuelle + Version Agenda avec prévisionnel Chronologique par matière [`bacd6ac`](https://github.com/Dannebicque/intranetV3/commit/bacd6ac5aa761d66600ebb4dd50a82fd6be4f0a9)
- Bugs mails [`471ed6b`](https://github.com/Dannebicque/intranetV3/commit/471ed6bbcaa94faac14183b88b3c0117fe65ac4e)
- Modal sur comparaison EDT [`558bfab`](https://github.com/Dannebicque/intranetV3/commit/558bfabe0581cbbfe6f8d619cf728c3b733ac926)
- Mails incomplet et refus [`b92e151`](https://github.com/Dannebicque/intranetV3/commit/b92e15125df9a19155ff56667c1590ceaa814318)
- Install.md [`a715143`](https://github.com/Dannebicque/intranetV3/commit/a715143f6129fdfcf2e7c6948f06c28116d0060f)
- Couleurs EDT Admin [`c88f2c9`](https://github.com/Dannebicque/intranetV3/commit/c88f2c9a75b00d0f9a5119e4cbe0012e509317ce)
- TravisCi [`1c0b3b6`](https://github.com/Dannebicque/intranetV3/commit/1c0b3b6de05dcb43222c0c9e26578eb4a9dbd9c1)
- Formulaire stage [`1c8f2c7`](https://github.com/Dannebicque/intranetV3/commit/1c8f2c7e3649918ac89a32927d43901f56257806)
- Bug modification code Celcat [`b9427dd`](https://github.com/Dannebicque/intranetV3/commit/b9427dde80e06bf5a505176cc679cb9da71d3611)
- Alternance [`0d6389b`](https://github.com/Dannebicque/intranetV3/commit/0d6389b9d7345fffdddd6ebed8493c17f246191b)
- Partie stage, amélioration des requêtes [`f150d81`](https://github.com/Dannebicque/intranetV3/commit/f150d81320aad73eb901a9de8d2ee44ec13c5638)
- TravisCi [`fe197da`](https://github.com/Dannebicque/intranetV3/commit/fe197daa10828282915e68efe74ecd9d1556ae2f)
- Qualité [`485d8bc`](https://github.com/Dannebicque/intranetV3/commit/485d8bc2275890e023b384116c0c4e5b2ff2c1b8)
- Convention de projet, typos [`7731705`](https://github.com/Dannebicque/intranetV3/commit/773170536f40ea9a945877f5ce5eb88e4f3a4e32)
- Placement [`1cf86f0`](https://github.com/Dannebicque/intranetV3/commit/1cf86f0fdfabf16b12186fe7dfc2c54f746e5150)
- Force cache twig database [`330015c`](https://github.com/Dannebicque/intranetV3/commit/330015cb2866e81da16461cfb1ad96419c8cd38c)
- Saisie des notes en admin [`d903f0f`](https://github.com/Dannebicque/intranetV3/commit/d903f0f19f34cc5c88ddb774e4d89aac8fe8c8bf)
- Alternance [`52a95bf`](https://github.com/Dannebicque/intranetV3/commit/52a95bffdfb4149e73e9c904dfe4f9676db6e1db)
- TravisCi [`6aa085e`](https://github.com/Dannebicque/intranetV3/commit/6aa085e7e5f7d857a9da8033e143bda524389249)
- Alternance suivi [`be70494`](https://github.com/Dannebicque/intranetV3/commit/be70494d5add13d8d545b28b1145899d1d85971d)
- Bug import des notes [`0027937`](https://github.com/Dannebicque/intranetV3/commit/00279373faee7f89a84eadb80f1fb0c64ec73f3d)
- Force cache twig database [`d6909fc`](https://github.com/Dannebicque/intranetV3/commit/d6909fc0b7feeb68667930a34b01619eb77ac020)
- Courrier [`c3a316d`](https://github.com/Dannebicque/intranetV3/commit/c3a316de7f81030ed105e66328d832cb610f8162)
- Courrier [`27b6fb0`](https://github.com/Dannebicque/intranetV3/commit/27b6fb0b03f9defc84ad70bbbfcd28cbd68418c9)
- PJ dans les messages [`2b187b4`](https://github.com/Dannebicque/intranetV3/commit/2b187b47e6543f80738a00d87a14ab4d0c11a868)
- APC [`fa11e40`](https://github.com/Dannebicque/intranetV3/commit/fa11e403a09cc2d9330358df100bedc3c297b78a)
- Courrier [`e32bfce`](https://github.com/Dannebicque/intranetV3/commit/e32bfce34246de07760a688f643a1ddcb96b1ffd)
- Force cache twig database [`f2bcbf8`](https://github.com/Dannebicque/intranetV3/commit/f2bcbf8550f2c868d950c2aaa76fec54736eafef)
- TravisCi [`25cab69`](https://github.com/Dannebicque/intranetV3/commit/25cab69ae258d93e40708bd4ad29b39242f62081)
- TravisCi [`9ef0dbb`](https://github.com/Dannebicque/intranetV3/commit/9ef0dbb10b206303a7ff3a0c39fd5605efa2e367)
- Champ optionnel dans formulaire de stage [`2e435c5`](https://github.com/Dannebicque/intranetV3/commit/2e435c587cb1a6a31ceb44e2f25ac22cf4e3f9bc)
- Update readme.md [`663f7cb`](https://github.com/Dannebicque/intranetV3/commit/663f7cb798a13cf4248596be7c5c479bb911c8ba)
- Courrier [`1a029b1`](https://github.com/Dannebicque/intranetV3/commit/1a029b11fbbaaf254de937bdfd856ab6f0efc9f8)
- Partie Stage [`2cd616f`](https://github.com/Dannebicque/intranetV3/commit/2cd616f4b445593a7556167087c186d698b0515f)
- Force cache twig database [`28eb39e`](https://github.com/Dannebicque/intranetV3/commit/28eb39eff5233a81fe28d907fdc38b97ce412bab)
- TravisCi [`7034c45`](https://github.com/Dannebicque/intranetV3/commit/7034c454ab2d5e5e8571e94a6916cf9cd694f196)
- Courrier [`dda4011`](https://github.com/Dannebicque/intranetV3/commit/dda4011e37056e78705cf44518d1fc5d9249c0a2)
- Bugsnag [`32968b0`](https://github.com/Dannebicque/intranetV3/commit/32968b035b1186b57b2a17ec1980618bb7319d49)
- Bug affichage évaluation [`f43ebd0`](https://github.com/Dannebicque/intranetV3/commit/f43ebd043b818324b251859ef9deec0f85b568cc)
- Partie Stage [`8c9dc1a`](https://github.com/Dannebicque/intranetV3/commit/8c9dc1a92ac2d9e2e7ba4a7df31b2bca23b5dc62)
- Alternance [`71b4e9e`](https://github.com/Dannebicque/intranetV3/commit/71b4e9ecc6b5cbbc8c85c859c0346b2c07a99e59)
- Typo mails projets [`3c07860`](https://github.com/Dannebicque/intranetV3/commit/3c078605d97eef948feef542dab8f01e985ca5d0)
-  typos [`ed22474`](https://github.com/Dannebicque/intranetV3/commit/ed22474bff5ce9c2470951c40012054f20b4ba5f)
- Courrier [`4755d44`](https://github.com/Dannebicque/intranetV3/commit/4755d443c8ee651ff090f09c9aea264b53a804a9)
- APC [`361b62d`](https://github.com/Dannebicque/intranetV3/commit/361b62d1992d784bb2fa34810bc73acb81f1b872)
- Regenerate Uuid sur un clone [`a5223db`](https://github.com/Dannebicque/intranetV3/commit/a5223dbe2797767db9ee26d672195251f2293555)
- Edt [`437387e`](https://github.com/Dannebicque/intranetV3/commit/437387e24f48f9d8c35b4e7d1fd613d85a5d1d97)
- Ordre des périodes [`29de258`](https://github.com/Dannebicque/intranetV3/commit/29de2587bd46667aa4c956c7b05d3396ed00017c)
- Tri des étudiants [`580c70f`](https://github.com/Dannebicque/intranetV3/commit/580c70fd3ba34ef85f12d6fc7994de62074f1f03)
- Bug saisie évaluation [`0c2dcd7`](https://github.com/Dannebicque/intranetV3/commit/0c2dcd79d5eef1259a101efbb5157bfd663f33d3)
- Stage mails sans "to" ? [`1ae18d2`](https://github.com/Dannebicque/intranetV3/commit/1ae18d2676eba8247d33c8cd0f0bd20de60859d9)
- SIgnature [`add4ffa`](https://github.com/Dannebicque/intranetV3/commit/add4ffacb6a282d84a4639818d438dfd7c74aeb5)
- Nb jour stage [`858743f`](https://github.com/Dannebicque/intranetV3/commit/858743fc69d9fb940e142384d0cb5053a2e739ab)
- Pj avec droits [`20dfd91`](https://github.com/Dannebicque/intranetV3/commit/20dfd914a469c34dc6db51e66399febdd8c6c976)
- TravisCi [`45fee7a`](https://github.com/Dannebicque/intranetV3/commit/45fee7ade23159dc4a930904ba507500c3560475)
- Courrier [`6bf025e`](https://github.com/Dannebicque/intranetV3/commit/6bf025e38e3289e9838d4a59e489c70da0c8264b)
- Courrier [`e160acd`](https://github.com/Dannebicque/intranetV3/commit/e160acdd480d01a5f3aee479e473a816317eb217)
- Logos courriers [`9190850`](https://github.com/Dannebicque/intranetV3/commit/9190850af4c291aefe83d557a3f8b686c23da5d1)
- Logos courriers [`c0c9367`](https://github.com/Dannebicque/intranetV3/commit/c0c93673c9d43c43830439ecf225f80cd5fb6da2)
- Formulaire stage [`46d5aa4`](https://github.com/Dannebicque/intranetV3/commit/46d5aa42085e854ebfa6cc4d3c1ccf6bb9ba86b4)
- TravisCi [`b6ee9f6`](https://github.com/Dannebicque/intranetV3/commit/b6ee9f6ecd868d483a2683cfa161e90cb15f8b80)
- TravisCi [`27d586f`](https://github.com/Dannebicque/intranetV3/commit/27d586fa22b816acd34bf531a6ca5ba45286978d)
- Amélioration de l'edit en ligne avec textarea et CSS [`e2c0336`](https://github.com/Dannebicque/intranetV3/commit/e2c0336df3fedce14dd2652d1bf4581efb9bfb8d)
- QV partie Agenda [`0444a87`](https://github.com/Dannebicque/intranetV3/commit/0444a87a6a73de4cd1feee9b23e155ec8226cd11)
- Correctifs sur la doc [`f833ca6`](https://github.com/Dannebicque/intranetV3/commit/f833ca681c6f65ec21bcf6352d446f38fad82b7b)
- merge [`0f4d922`](https://github.com/Dannebicque/intranetV3/commit/0f4d922713e3f6ea2bfc32a3dcf5144f3de56303)
- Correction bug apogee + gestion par année. [`cc10fb6`](https://github.com/Dannebicque/intranetV3/commit/cc10fb696169fd86eb3b5e1ae221ae4a1f0eb023)
- assets [`8f36293`](https://github.com/Dannebicque/intranetV3/commit/8f36293ca201e01283c937b7387d7ace2e62b944)
- buld js/css [`9bca986`](https://github.com/Dannebicque/intranetV3/commit/9bca98655a149e80df5a92ae1d17b0804f42533c)
- Passage à Webpack + Mise à jour icones (suppression ti + i8) [`0237f28`](https://github.com/Dannebicque/intranetV3/commit/0237f280ab8b57ea5a9e7273239f3ccca11e8fc9)
- Gestion des courriers de stage [`798d0fa`](https://github.com/Dannebicque/intranetV3/commit/798d0fa4c67e1028cc2b3ca544bf346e4948d153)
- QuillJs listes + Ajout civilité dans les champs de publipostage [`fc6d30a`](https://github.com/Dannebicque/intranetV3/commit/fc6d30a6959d03d9daa6960efd8fbd1d3dfd724d)
- Réservation de matériel commun [`1676e23`](https://github.com/Dannebicque/intranetV3/commit/1676e2351f7d1327c46f558d9314a23600a431fc)
- Gestion étudiants en scolarité. [`f70cb86`](https://github.com/Dannebicque/intranetV3/commit/f70cb863322b249dafbdd3333ee4b6d295c50d1e)
- Stage etape impression [`6a03181`](https://github.com/Dannebicque/intranetV3/commit/6a031817b4f4f78a59922d43ec35244a4fe2fda2)
- Correctifs [`46d0c1a`](https://github.com/Dannebicque/intranetV3/commit/46d0c1a2f2ae3ef292a07d18eac71f31aa3a7052)
- Correctif changement semaine EDT [`87da2ec`](https://github.com/Dannebicque/intranetV3/commit/87da2ec915b970e7059ea2e203a89b05e0faa929)
- Correctif changement semaine EDT [`132a997`](https://github.com/Dannebicque/intranetV3/commit/132a99726af65cebc42f35e8ac9f476e698d1f2d)
- Super Admin, bug [`b20f9bf`](https://github.com/Dannebicque/intranetV3/commit/b20f9bf2995d73becbb20a00ecbd46a86128eccb)
- Gestion Absences et AbsencesJustificatifs avec un format DateHeure et la librairie Carbon [`83f791f`](https://github.com/Dannebicque/intranetV3/commit/83f791f7bf4d5b81b8ba2170cf00dbc101bdc5ac)
- Passage à Webpack + datatables [`3b7ae12`](https://github.com/Dannebicque/intranetV3/commit/3b7ae125691c9f5a18c41451833aa073f1cf7483)
- Gestion des documents et documents favoris. [`747780b`](https://github.com/Dannebicque/intranetV3/commit/747780b90fe1caf2beabd2c621875ba2443898fc)
- Partie profil étudiant (scolarité + notes) [`10770ec`](https://github.com/Dannebicque/intranetV3/commit/10770eca9f51cc5ad39db3209ed5d3ac57ff0608)
- Stage etape impression [`67f3822`](https://github.com/Dannebicque/intranetV3/commit/67f3822a0f0ee456c2f33d570c26d4b6c9c96cf2)
- Messagerie personnels/vacataires [`859b425`](https://github.com/Dannebicque/intranetV3/commit/859b4258c2af66a0275d4111fa734c2c4624631f)
- Bugs [`e91ce11`](https://github.com/Dannebicque/intranetV3/commit/e91ce11085fbbbc345f8f4042bdfacaf7ef4ae44)
- correcitfs mineurs [`2ac94b5`](https://github.com/Dannebicque/intranetV3/commit/2ac94b5bbb93bb9e3f88fa2fb921c92258f2523b)
- Correctifs qualité + update [`83b9052`](https://github.com/Dannebicque/intranetV3/commit/83b90521fd1566d48c16af32c172b0226ef7c0ed)
- Refonte partie groupe et type de groupe [`c2d316d`](https://github.com/Dannebicque/intranetV3/commit/c2d316d6aa9ed210254d5cf36aa11e216ae15da1)
- Refonte partie calcul de sous-commission + export [`aebbd2b`](https://github.com/Dannebicque/intranetV3/commit/aebbd2b01acfc0705d87042445ee99b329595aec)
- Optimisation des dates avec Carbon [`3982218`](https://github.com/Dannebicque/intranetV3/commit/3982218c24868a97bd4d2d104e816a919008e78f)
- Retour à Ramsey/UUid plutôt que Symfony/Uid [`0752315`](https://github.com/Dannebicque/intranetV3/commit/0752315a6364ec58c8b89f84780f0cfb2e586274)
- Gestion de projet etudiant [`5391ea9`](https://github.com/Dannebicque/intranetV3/commit/5391ea9f68a20f74c874ea3dc81c1f3032219020)
- Contrôle des controllers (qualité, ...) [`db6c0d8`](https://github.com/Dannebicque/intranetV3/commit/db6c0d858ef99fa05e73bd16cd17c95fc641ac58)
- Messenger pour les mails (à confirmer en prod) [`520e762`](https://github.com/Dannebicque/intranetV3/commit/520e762c5d4696a9f162974b94e6c957d174e796)
- Passage à Webpack [`52e819a`](https://github.com/Dannebicque/intranetV3/commit/52e819a0d58f95f2b33238abc560c6da6010a40f)
- Suppression de MyEtudiant et refactoring en plusieurs petites classes indépendantes [`3b351bc`](https://github.com/Dannebicque/intranetV3/commit/3b351bcee2e768123d0afd5aadb219a30e655596)
- Partie Projet [`ad31004`](https://github.com/Dannebicque/intranetV3/commit/ad31004fde3dab7fcce78b0c7b62c28cfbf5e052)
- Vérification partie absence, justificatifs et justifier [`1cb484d`](https://github.com/Dannebicque/intranetV3/commit/1cb484d5f2d62647fa8cc798b90bfb2600d91f87)
- Correctifs de qualité de code [`612317a`](https://github.com/Dannebicque/intranetV3/commit/612317a5815c37aa631aa9e130cce7908534b741)
- Partie stage. Correctifs [`d9a66ae`](https://github.com/Dannebicque/intranetV3/commit/d9a66ae61dd203955796bf1d26645cf7a2d05fd0)
- Pièces jointes [`9c6877c`](https://github.com/Dannebicque/intranetV3/commit/9c6877cfef83b45e3acb86e008779287d56f0cde)
- Partie Stage et refonte Mailer [`52567ab`](https://github.com/Dannebicque/intranetV3/commit/52567aba90ec0e76d1fc8253e7881cddfef87742)
- Projet Etudiant [`832be8d`](https://github.com/Dannebicque/intranetV3/commit/832be8dff804513c20b1b47f8c625831ccf35860)
- Pages comparatifs EDT/Previ [`02b636b`](https://github.com/Dannebicque/intranetV3/commit/02b636b3f7f6091756f82f8568e70b2e339f743c)
- Nombreux correctifs de bugs, d'affichage et de JS [`45054cd`](https://github.com/Dannebicque/intranetV3/commit/45054cdf69ea8137dc88c25f92608b3101de0640)
- Correctifs [`e819b30`](https://github.com/Dannebicque/intranetV3/commit/e819b3011833d4dca111231e28f3b8c07ac1f234)
- Nombreux correctifs de bugs, d'affichage et de JS [`8cb6a34`](https://github.com/Dannebicque/intranetV3/commit/8cb6a34b7327e5cf50af0bd609f4084b3416caca)
- Projet Etudiant [`0326b8f`](https://github.com/Dannebicque/intranetV3/commit/0326b8f32bab6f232d76a9e826fc7a2b36d2432a)
- Partie statistique sur le département [`df5d3c1`](https://github.com/Dannebicque/intranetV3/commit/df5d3c17764976bfc7dec936172a4006a67e96d0)
- Super Admin, bug [`267b28b`](https://github.com/Dannebicque/intranetV3/commit/267b28b2a26dceec101e21d05868518cbd65f4e4)
- Nombreux correctifs de bugs, d'affichage et de JS [`9333596`](https://github.com/Dannebicque/intranetV3/commit/933359612d55fe0e9e98e283a092b4900414a6c5)
- Export PDF Edt + Ical [`277459e`](https://github.com/Dannebicque/intranetV3/commit/277459e371bc34457cc8f3b415bf12c98a1a8602)
- Affichage et synchro EDT avec Celcat [`4603788`](https://github.com/Dannebicque/intranetV3/commit/4603788d0d0be9c15d74f12d46d29658b52c7ecc)
- Correctif changement semaine EDT [`7359bf8`](https://github.com/Dannebicque/intranetV3/commit/7359bf8519f172ddc39f6a8e6fe8e3880ac83bfb)
- Passage à Webpack [`30d7b96`](https://github.com/Dannebicque/intranetV3/commit/30d7b96bc01444cdda1e591062ed3106b178c250)
- Gestion des courriers de stage [`c92e6d7`](https://github.com/Dannebicque/intranetV3/commit/c92e6d738466d2af170d74e07ebbbecba94ddba1)
- Messagerie [`9bbc9e0`](https://github.com/Dannebicque/intranetV3/commit/9bbc9e0778133cfa0f1e0ab9f1c3e58ce4a306b8)
- Affichage EDT étudiants [`c9dcbb3`](https://github.com/Dannebicque/intranetV3/commit/c9dcbb34c9aa812126badf9453734f680061ff07)
- Optimisation code, correctifs mineurs, restructuration [`6a57253`](https://github.com/Dannebicque/intranetV3/commit/6a572530d2d499faa7ca5d1696367ffb5c9af773)
- Modification d'un étudiant [`2e2a489`](https://github.com/Dannebicque/intranetV3/commit/2e2a489258fe9736c5c28a93f074af8c6874e403)
- import apogee [`5d5aa26`](https://github.com/Dannebicque/intranetV3/commit/5d5aa2692ffb0d50f53fdab8b487136d3857a243)
- Partie projet etudiant [`5eb5220`](https://github.com/Dannebicque/intranetV3/commit/5eb5220d74e59f82471ff6b43d249e0051e2ca66)
- Messagerie [`e5822b4`](https://github.com/Dannebicque/intranetV3/commit/e5822b4b8548db5a9efe2bc0e8560f77e9962878)
- Passage à Webpack [`e214796`](https://github.com/Dannebicque/intranetV3/commit/e214796c9bda034b9308cb6049564e6150a25b1b)
- Correctifs [`f30ddd5`](https://github.com/Dannebicque/intranetV3/commit/f30ddd58c29ce361bc4422647175fd4c3f9c76fb)
- Bugs [`73891e3`](https://github.com/Dannebicque/intranetV3/commit/73891e30c44d9a681c53bdce9953841636d335cf)
- Gestion étudiants [`7ef3287`](https://github.com/Dannebicque/intranetV3/commit/7ef328714456bc6f33ab6469f103e84586290233)
- Bugs EDT + Affichage [`b769f8d`](https://github.com/Dannebicque/intranetV3/commit/b769f8d7445e1f9fc7ca62345d390ff79464ea12)
- projet tutoré [`4c4cd00`](https://github.com/Dannebicque/intranetV3/commit/4c4cd0017e17a202f66a742a41ea5713063f1ce4)
- LDAP enseignant [`f79e319`](https://github.com/Dannebicque/intranetV3/commit/f79e3198d99c242b2f98b918f82a5f826441844f)
- Gestion étudiants en scolarité. [`5f5f2df`](https://github.com/Dannebicque/intranetV3/commit/5f5f2dfdfec9c75684ca6b3fb4e702b68d841161)
- Convertisseur code_insee/ville [`598113f`](https://github.com/Dannebicque/intranetV3/commit/598113fa21d783c2308099607f058c46e1f65690)
- Bug du menu hamburger [`7e0b5d1`](https://github.com/Dannebicque/intranetV3/commit/7e0b5d18574889451cbde6f2a87682c3ff277246)
- Bug justification Etudiant [`7465dc3`](https://github.com/Dannebicque/intranetV3/commit/7465dc3ffa1ad42adcd3bb07d7fe9e4408af2f1d)
- Gestion des courriers de stage [`9a49f00`](https://github.com/Dannebicque/intranetV3/commit/9a49f00b9551da21913e085a2729591fa6e3bd6a)
- Etape imprimée [`d18e96d`](https://github.com/Dannebicque/intranetV3/commit/d18e96d72c5dcaa08b1dae36499ec50a0c0bf511)
- recherche avec filtre sur le département [`7e683d5`](https://github.com/Dannebicque/intranetV3/commit/7e683d5581dc4c36c8844a8fd2a901bb0adeaa8b)
- Affichage matiere et enseignant sur documents [`44e6c88`](https://github.com/Dannebicque/intranetV3/commit/44e6c880e7e46dcbd17647a26d427b4344f37dfd)
- Typos convention de stage [`a167c97`](https://github.com/Dannebicque/intranetV3/commit/a167c97f6a004d91511c9262b493e362b4bd037f)
- Amélioration gestion étudiants [`3903bc9`](https://github.com/Dannebicque/intranetV3/commit/3903bc963e806dbc05bfb7b8a0d00c66498b6443)
- Export des absences / matière [`3ad55dc`](https://github.com/Dannebicque/intranetV3/commit/3ad55dc85efe5b0ed2bc4bae66c9b11f6f909cf4)
- Partie notification, avec ajout UUID et marquage comme lu des notifications [`75f4a43`](https://github.com/Dannebicque/intranetV3/commit/75f4a43cd3054fc0ef031558d46c8129691643b5)
- Nombreux correctifs de bugs, d'affichage et de JS [`34a400a`](https://github.com/Dannebicque/intranetV3/commit/34a400a81ce2855b7db06c7aa8a8e3d5bd3f17e6)
- Suppression du libelle d'entreprise [`14df64f`](https://github.com/Dannebicque/intranetV3/commit/14df64f4c1b8ed2f93bae5a4b2486bebec564061)
- test apogee [`3f7b923`](https://github.com/Dannebicque/intranetV3/commit/3f7b92370f9200e2c627187386f6d5d9d18e5b00)
- Bugs divers [`102d368`](https://github.com/Dannebicque/intranetV3/commit/102d36890a672700d56960012dce1f1ef6085b00)
- Problème icône en CSS avec FA [`6b14515`](https://github.com/Dannebicque/intranetV3/commit/6b1451569d8f6c344aa37de0a390aa75f5d83e4b)
- Gestion des likes sur les articles [`aa91ed4`](https://github.com/Dannebicque/intranetV3/commit/aa91ed4f792c2ee97eafbbcd7694279e94423e53)
- import apogee [`b509e16`](https://github.com/Dannebicque/intranetV3/commit/b509e16e78a11f3d958ce0b17437f22e6ddf8105)
- export pdf [`6d881ef`](https://github.com/Dannebicque/intranetV3/commit/6d881ef74fcfe6b81f9bae7f889674655965be81)
- Suppresssion du libelle d'entreprise qui est en fait la raison sociale [`2dff419`](https://github.com/Dannebicque/intranetV3/commit/2dff4198ea91f633aa6eb877a61cd0e848a3b748)
- Justification des absences (bug profil) + mailing et notif [`75bb65d`](https://github.com/Dannebicque/intranetV3/commit/75bb65db12e11cb937398b2eab346f3a82380c7a)
- Optimisation des dates avec Carbon [`5c794d9`](https://github.com/Dannebicque/intranetV3/commit/5c794d99beada3dda49641686ee051f66ec5b31c)
- Justification des absences (bug profil) + mailing et notif [`669abf6`](https://github.com/Dannebicque/intranetV3/commit/669abf625d44d0c36bc8ed67cdc6dbfedba8cb11)
- Refonte partie groupe et type de groupe [`b6b3fc1`](https://github.com/Dannebicque/intranetV3/commit/b6b3fc1371edfea10b1bfdf3dd4f54288e7bbc66)
- Bornes [`f890bfa`](https://github.com/Dannebicque/intranetV3/commit/f890bfa03e93828ac90a269c2539368c2896ee68)
- Saisie des absences [`548164c`](https://github.com/Dannebicque/intranetV3/commit/548164c30ce572574090e57531833442276affd8)
- Saisie des absences [`1a59e74`](https://github.com/Dannebicque/intranetV3/commit/1a59e74d62647babc0d5ff3cf19fb5c93e11c620)
- bugs [`bbe654d`](https://github.com/Dannebicque/intranetV3/commit/bbe654d57ea44b3824dd93f3da2b1469eef03418)
- Convention avec adresse [`60c0bb0`](https://github.com/Dannebicque/intranetV3/commit/60c0bb08f6b193135a7ccc117a41d9fd0db383cf)
- Refonte partie groupe et type de groupe [`421e115`](https://github.com/Dannebicque/intranetV3/commit/421e11545a4fcb3c7cdcf70528ae18282ede070a)
- LDAP enseignant [`f8b1ef5`](https://github.com/Dannebicque/intranetV3/commit/f8b1ef5f9a753adeadf69d5c7a742d096b58b36a)
- correctifs [`7399bfc`](https://github.com/Dannebicque/intranetV3/commit/7399bfc21fa4b1fa834aef1702eb2ab21ae6fb4b)
- Bug messagerie [`db700e2`](https://github.com/Dannebicque/intranetV3/commit/db700e28acce57eca7f66bd45d2f1b11a3e5fd7e)
- Stage etape impression [`8778ad5`](https://github.com/Dannebicque/intranetV3/commit/8778ad5efd6b1d1e8c947e542196ca0046ad4c89)
- Bug profil [`2d6afa0`](https://github.com/Dannebicque/intranetV3/commit/2d6afa0888a9488ce92fae806b8edd66d6d6a6d2)
- Bug des groupes [`14b72ed`](https://github.com/Dannebicque/intranetV3/commit/14b72ed6c8ec0cce6dbecb2822cc386b4e5b2a1b)
- Bug Edt [`4b98667`](https://github.com/Dannebicque/intranetV3/commit/4b986678970d12150dfce071eae8d1f9bd3878d8)
- Bug PDF [`7541f87`](https://github.com/Dannebicque/intranetV3/commit/7541f87e023676e92f29f4b59eab1e461c961e95)
- Affichage EDT + Affichage semestre actif [`7114db0`](https://github.com/Dannebicque/intranetV3/commit/7114db0852f8f94a1b955498c748e95caa8f30f3)
- Changement département + logo [`461802a`](https://github.com/Dannebicque/intranetV3/commit/461802a2cd540b9203a96beb790d3775feb2c256)
- Bouton démission sur le profil étudiant [`1ce30ec`](https://github.com/Dannebicque/intranetV3/commit/1ce30eccd4600a49b6df78445c9c63e5b8bd0dea)
- QuillJs listes + Ajout civilité dans les champs de publipostage [`15df403`](https://github.com/Dannebicque/intranetV3/commit/15df403ade144fba2d689247b2eb101b6de361f9)
- GEstion des salles [`20e9e0e`](https://github.com/Dannebicque/intranetV3/commit/20e9e0e6c362e2cdd42210b3ed27427f2ae88126)
- Export groupes [`f013151`](https://github.com/Dannebicque/intranetV3/commit/f013151b168b1ea3b0059b9764f00767ad9ee3f0)
- Comparaison Prévi/Edt [`1a7a83f`](https://github.com/Dannebicque/intranetV3/commit/1a7a83f6f2547213655e25855dd61f9daebd3e5e)
- test apogee [`398876a`](https://github.com/Dannebicque/intranetV3/commit/398876ad84e4ecf943f691fa78c168f4a07c780c)
- Adaptation des heures EDT [`3d57176`](https://github.com/Dannebicque/intranetV3/commit/3d57176fa5925ce5bd3e67393dc936a19ec85963)
- gestion des groupes [`60df3f3`](https://github.com/Dannebicque/intranetV3/commit/60df3f373c435307010c7a8cb4cb2aedf04dc363)
- Bug ajout personnel SADM [`443e401`](https://github.com/Dannebicque/intranetV3/commit/443e401dcae62990f89b08d694d72680b12ae786)
- updload photo [`75a3bfc`](https://github.com/Dannebicque/intranetV3/commit/75a3bfc839b700fd41762685e72b88312b89a3c1)
- Mails stages responsable + copie assistantes [`72d45f8`](https://github.com/Dannebicque/intranetV3/commit/72d45f85e2fa7ae97eb25ca90de2042907246d51)
- Bug import previsionnel vide [`6fd3984`](https://github.com/Dannebicque/intranetV3/commit/6fd398458c1b8ba48f2a6394172a16d3ecc13636)
- Choix département [`55cf842`](https://github.com/Dannebicque/intranetV3/commit/55cf842c47d9886b01f5f862d6e311ab5c71931e)
- Passage à Webpack + recherche + validation [`ed11c26`](https://github.com/Dannebicque/intranetV3/commit/ed11c26ffac91c39b4850da3f89b8c634a6af1b8)
- Bigs mailer suite modification de structure [`7ad57d0`](https://github.com/Dannebicque/intranetV3/commit/7ad57d0164a506456381817886d24ecbbaafe664)
- Stage [`53f5a52`](https://github.com/Dannebicque/intranetV3/commit/53f5a52c290d1d11036c5cbbeb6b1b0feb5c3ede)
- Correction bug apogee + gestion par année. [`a93f725`](https://github.com/Dannebicque/intranetV3/commit/a93f7256b569dd6ba6f541e7ce161dc5f0a69f0c)
- Stage etape impression [`35f51ee`](https://github.com/Dannebicque/intranetV3/commit/35f51eead3271cc582f6ca08956586af330237d8)
- Partie responsable des absences [`0356bdb`](https://github.com/Dannebicque/intranetV3/commit/0356bdb58f0702f12ae28f1f25c3f0a6d481c2aa)
- Bugs [`8b497b3`](https://github.com/Dannebicque/intranetV3/commit/8b497b3aa95f463da94f5415c8cf3abd656ef92c)
- Adaptation des heures EDT [`26a6036`](https://github.com/Dannebicque/intranetV3/commit/26a6036cfed838e8e7aedfef9c3565f451110970)
- Bugs vacataires [`a84290a`](https://github.com/Dannebicque/intranetV3/commit/a84290a1642a6991ebe3a9226ba79abad4111ab9)
- Passage à Webpack [`6f0e27b`](https://github.com/Dannebicque/intranetV3/commit/6f0e27b36489791f0a23b948ecece060144004c1)
- Visibilité profil [`1991f50`](https://github.com/Dannebicque/intranetV3/commit/1991f50c99d6724e3b36d76766629f8a8657b1a3)
- Bug messagerie [`ec84ef9`](https://github.com/Dannebicque/intranetV3/commit/ec84ef95df81e9fc170b260367db6b0a42b8244e)
- Bug choix département [`9ec2f66`](https://github.com/Dannebicque/intranetV3/commit/9ec2f663c63e832adc4f6e76e0aad13518aa6d4a)
- Ical + typo [`a9e578a`](https://github.com/Dannebicque/intranetV3/commit/a9e578a7e1fbcdf46a94354ea26c1544e922884a)
- Correctifs qualité + update [`3dfb2d8`](https://github.com/Dannebicque/intranetV3/commit/3dfb2d8552db5073981370bf27a77ab630437027)
- Profil [`a8ffb86`](https://github.com/Dannebicque/intranetV3/commit/a8ffb864897fffe3c2a7e4581c4e0ec498cfa8bf)
- test apogee [`5143aa6`](https://github.com/Dannebicque/intranetV3/commit/5143aa6401854b764e15d5fe3f4eccd3e80df568)
- Ajout du semestre dans le résultat de recherche [`e7e89d9`](https://github.com/Dannebicque/intranetV3/commit/e7e89d98c619046a5db8542517f3b3f33290124c)
- logs du 21/09 [`50f0df2`](https://github.com/Dannebicque/intranetV3/commit/50f0df287e4f7021e0f1c6cf1b632ed08ca4ec60)
- Partie Stage [`6011044`](https://github.com/Dannebicque/intranetV3/commit/6011044174d991ca943577da6819c02dcde52fc3)
- Partie Stage et refonte Mailer [`74658be`](https://github.com/Dannebicque/intranetV3/commit/74658be09ef12e6e8fbdf650c281b65887f42102)
- Gestion des personnes autorisés pour modification d'une évaluation [`72a2711`](https://github.com/Dannebicque/intranetV3/commit/72a2711c360c6b3ef2495a41ef051d6161716ca7)
- Export CSV des listings [`0b914d6`](https://github.com/Dannebicque/intranetV3/commit/0b914d6a15214a8969f48db7538b4f51b8b5ca53)
- Partie Stage [`d3d9e06`](https://github.com/Dannebicque/intranetV3/commit/d3d9e069ac4cbfb3f5917018a3f051012fded1a5)
- Prévisionnel export OMEGA avec accents/majuscule [`3b3a8ee`](https://github.com/Dannebicque/intranetV3/commit/3b3a8ee4a019701badca2f41bc6a3a22f6d219ba)
- Alternance [`209b05a`](https://github.com/Dannebicque/intranetV3/commit/209b05adfa8d87eddc5d26a89c90ce7bf9abf454)
- Synchro Ical [`e8b305c`](https://github.com/Dannebicque/intranetV3/commit/e8b305ca9d8d6ac80cec477b59972ccac14f40e4)
- bug date/articles [`fd970be`](https://github.com/Dannebicque/intranetV3/commit/fd970bec2b1a1350bfa59d5e1a470b3f4f5a628c)
- Stage etape impression [`17aa5e3`](https://github.com/Dannebicque/intranetV3/commit/17aa5e312024933750b7246e306078fb8a6bc2bd)
- Templates des mails [`5caae01`](https://github.com/Dannebicque/intranetV3/commit/5caae01ad7437bf79246e62dfea0b64501b81996)
- Bigs mailer suite modification de structure [`6e1922c`](https://github.com/Dannebicque/intranetV3/commit/6e1922ca6d08222fffd7e773be87adf37ac13ad0)
- Export Absences / Matiere [`5e98893`](https://github.com/Dannebicque/intranetV3/commit/5e98893244fa21799d8f33398e05e38e03cd0ce6)
- Logo Convention de stage [`bc20f51`](https://github.com/Dannebicque/intranetV3/commit/bc20f51fbaae4336ec3b9cc563704a047e9ef479)
- Tri des documents par libelle plutôt que date. [`62b4f7e`](https://github.com/Dannebicque/intranetV3/commit/62b4f7e66da258ff32674f9eb91597dabf16d634)
- Import EDT Intranet/XLS [`6241225`](https://github.com/Dannebicque/intranetV3/commit/624122561c8cfcbd4f8959623d2f5d5c603ff5a5)
- Ical [`ebddc4d`](https://github.com/Dannebicque/intranetV3/commit/ebddc4de2b2ec579742e26fc6c9f9c0248ecccaa)
- Messagerie [`e5a380a`](https://github.com/Dannebicque/intranetV3/commit/e5a380a30ea169026ceb9ebe3ee9f17023efd2ff)
- Matière PPN [`4fcd888`](https://github.com/Dannebicque/intranetV3/commit/4fcd88874463fe19a88845191a3af8d4dc9d899b)
- Bug des groupes [`e13ab32`](https://github.com/Dannebicque/intranetV3/commit/e13ab327774cf37e8273413c4c0101fb404e5a46)
- Affichage des départements dans la page choix-departement [`7527035`](https://github.com/Dannebicque/intranetV3/commit/752703546c5d34eea943808f0dcbf2acdd0d68c3)
- Correctif justification des absences [`ee03d96`](https://github.com/Dannebicque/intranetV3/commit/ee03d96d8a9476ff92e48541544bd81cb31521ca)
- Saisie date justificatif d'absence [`c2b31d8`](https://github.com/Dannebicque/intranetV3/commit/c2b31d81fb20cd394dc6b2c12ce2e108ed289c7b)
- updload photo [`a37c790`](https://github.com/Dannebicque/intranetV3/commit/a37c790e34e4f5adbd8a1e059fd9a4a26d25f5cc)
- Correctif affichage des absences [`bffbcaa`](https://github.com/Dannebicque/intranetV3/commit/bffbcaaacbdf1f8ee3b5cea337d024d009536d46)
- Synchro Ical [`6dc617b`](https://github.com/Dannebicque/intranetV3/commit/6dc617bc3c9e302ff9e7680cfffa8dc2ff0637c3)
- Bug des groupes [`32e865c`](https://github.com/Dannebicque/intranetV3/commit/32e865cd580217bf34d10c26308d79ba20b21395)
- Choix département [`c5d3b2e`](https://github.com/Dannebicque/intranetV3/commit/c5d3b2ec2cee0f76f34cf111ab5dff2ce9b7d92b)
- Bug Edt [`07628ba`](https://github.com/Dannebicque/intranetV3/commit/07628ba91fec78959aac649cf862510d61c4d369)
- LDAP enseignant [`1af7c5c`](https://github.com/Dannebicque/intranetV3/commit/1af7c5c11d4287b51d2b24e3a4ecc7585448679c)
- Bug des groupes [`acc477a`](https://github.com/Dannebicque/intranetV3/commit/acc477a918b834f132bb68eb0cb80ead652f1e5f)
- JS =&gt; Modal + Tooltip [`70510c5`](https://github.com/Dannebicque/intranetV3/commit/70510c5f3224a192e335b2f7e8a432262a000c93)
- Correctifs mise en page ergonomie partie stage. [`8a3f7a2`](https://github.com/Dannebicque/intranetV3/commit/8a3f7a2b386141babef211d2f9df38c2bd9cb6a0)
- Problèmes Accents dans les majuscules [`e71d609`](https://github.com/Dannebicque/intranetV3/commit/e71d6095c7a73f90a1cf9c8ab85ffc733924c99e)
- images absences [`a79d503`](https://github.com/Dannebicque/intranetV3/commit/a79d5033f64d59b6719cb67a11711854024931ee)
- Bugs [`c6210bf`](https://github.com/Dannebicque/intranetV3/commit/c6210bf49144117b86dc611c16985c0077386056)
- Gestion des courriers de stage [`ee79313`](https://github.com/Dannebicque/intranetV3/commit/ee79313652d823184da41f2e8557192123032587)
- Tri des documents par libelle plutôt que date. [`0b7cf27`](https://github.com/Dannebicque/intranetV3/commit/0b7cf2701db7704e1524fd6698af9a7d6bf1b740)
- Tri des documents par libelle plutôt que date. [`14ea4bd`](https://github.com/Dannebicque/intranetV3/commit/14ea4bd757f7b1b9a34c17bb335e48d56efaf0ff)
- Absences justificatifs [`8499131`](https://github.com/Dannebicque/intranetV3/commit/84991313d83cdbe27826fff7a3ab519a50eb986f)
- Modification enquete qualité [`b9f0888`](https://github.com/Dannebicque/intranetV3/commit/b9f0888576f294d1bb81200a0b8b59e1038155a6)
- Document [`70f6ec8`](https://github.com/Dannebicque/intranetV3/commit/70f6ec8ff56cfa679c5bebfc823fc263a614fc10)
- Changement de mot de passe. Interdit pour les étudiants [`386204a`](https://github.com/Dannebicque/intranetV3/commit/386204aa4d29118d7bc872b67ad5a1034dcceb76)
- Messagerie [`5ef8c71`](https://github.com/Dannebicque/intranetV3/commit/5ef8c71bb9061705c07d6155db347bfc2261322d)
- updload photo [`9f04b69`](https://github.com/Dannebicque/intranetV3/commit/9f04b696a2b14b0e41b5bf1059ba49b2de7e572f)
- assets [`6b31657`](https://github.com/Dannebicque/intranetV3/commit/6b3165728d172ced96c34de31bdcb0b4922c8717)
- assets [`d090afd`](https://github.com/Dannebicque/intranetV3/commit/d090afd66faffa44b39f06dbb44290a1444a4b13)
- Ical [`d7c6777`](https://github.com/Dannebicque/intranetV3/commit/d7c6777cdab7db75c02fa567f17b63243c13836c)
- Typos convention de stage [`5ff8cdb`](https://github.com/Dannebicque/intranetV3/commit/5ff8cdbe5b1ea7bad5d8968ed62cd3dfbd8be37a)
- Profil accessible aux assistantes [`93d55ce`](https://github.com/Dannebicque/intranetV3/commit/93d55ce612bd2bc3e97aa3f54f4d6d0a6d5d920e)
- Edt [`13acd71`](https://github.com/Dannebicque/intranetV3/commit/13acd717d699afcdf8f33934374a7237c5ca09b0)
- Problème synchro grioupes de langue [`559b19e`](https://github.com/Dannebicque/intranetV3/commit/559b19e19ad1a9007a9918a00e78c78280a2abc4)
- Modification PPN et configuration [`7a9bf61`](https://github.com/Dannebicque/intranetV3/commit/7a9bf612eec68b4b64719cb220aa108d1a8dd725)
- Bug des groupes [`aad404a`](https://github.com/Dannebicque/intranetV3/commit/aad404a086d067b9f72ae5e81b8462a098fb614f)
- Bug Edt promo [`54db2f6`](https://github.com/Dannebicque/intranetV3/commit/54db2f614367e9fd3edc2eb45ee56beb81bb6269)
- gestion des groupes [`96bdd37`](https://github.com/Dannebicque/intranetV3/commit/96bdd37f2f4bcfc5738524822bec8ac238612edc)
- Absences justificatifs, fichier optionnel [`ea294c6`](https://github.com/Dannebicque/intranetV3/commit/ea294c64c5a83060a27a1d1ceb1cb4f1b8c2b46c)
- pdf [`5c214f5`](https://github.com/Dannebicque/intranetV3/commit/5c214f5ab8b4c3ca349152bc0f1ce6856ef91ca9)
- pdf [`5f9dc27`](https://github.com/Dannebicque/intranetV3/commit/5f9dc27a36b2b350fe74d45276c61e37b359a54a)
- Bug des groupes [`ee8aaa2`](https://github.com/Dannebicque/intranetV3/commit/ee8aaa261a7252c56f55e6e47b24af4f510b8787)
- Bugs Ical Etudiant [`5cc7767`](https://github.com/Dannebicque/intranetV3/commit/5cc7767b888909c35aece0b95694dd93e0b8a541)
- Partie statistique sur le département [`3b798ed`](https://github.com/Dannebicque/intranetV3/commit/3b798ed13143dd9a0e122b6262ce30bde46a2425)
- Correctif courriers de stage [`f8a8cea`](https://github.com/Dannebicque/intranetV3/commit/f8a8cea2f3eb4044ba63779e025f780d600043a0)
- Partie Stage [`f97965a`](https://github.com/Dannebicque/intranetV3/commit/f97965a2f5e971077290fdce8555bfacb44869a0)
- Enquête qualité [`f2b6223`](https://github.com/Dannebicque/intranetV3/commit/f2b6223ace9636cfb2a691d9b09c3b0902b79858)
- Apogée [`1cd1426`](https://github.com/Dannebicque/intranetV3/commit/1cd1426b4f7656e2a6c667f3e36145b2c6ae11ca)
- Gestion des documents [`5c80bbb`](https://github.com/Dannebicque/intranetV3/commit/5c80bbbc77d9e18d7f9003c1807fc0d8954ceccc)
- Export Absences / Matiere [`f163398`](https://github.com/Dannebicque/intranetV3/commit/f163398e9cc14b6f5ce351e044c2e22d461800f2)
- Correctifs [`9ee2dc4`](https://github.com/Dannebicque/intranetV3/commit/9ee2dc44890c1d990a1854043afeb6c7af90d530)
- Ical [`2a7b727`](https://github.com/Dannebicque/intranetV3/commit/2a7b72775d17428c165dc2cd97930e97cfe89f47)
- Choix département [`6928cb4`](https://github.com/Dannebicque/intranetV3/commit/6928cb4dc1b6829616bf26d0b8a414dba2bcf4ae)
- Correctif courriers de stage [`53bef81`](https://github.com/Dannebicque/intranetV3/commit/53bef81b4a3d54c5886d2bbf7a9929bb8a733a36)
- logs du 20/09 [`a6f75db`](https://github.com/Dannebicque/intranetV3/commit/a6f75dbcacba779594cc6e4785c9adb029e765c1)
- Affichage du semestre sur le profil [`841bd9d`](https://github.com/Dannebicque/intranetV3/commit/841bd9d6f9e1aa2a2e0220a18987bbcc4a0f0580)
- Bug nom fichier input [`fd97b24`](https://github.com/Dannebicque/intranetV3/commit/fd97b24c72ea5e51191ee680a0ee41c0dba2d400)
- pdf [`e2da869`](https://github.com/Dannebicque/intranetV3/commit/e2da869b9c9b2ac8977222db37d733d290c14c68)
- Correctif stage [`276ca42`](https://github.com/Dannebicque/intranetV3/commit/276ca4257722b5b7df40fedf76a5680660d96d27)
- Modification enquete qualité [`bdab9e4`](https://github.com/Dannebicque/intranetV3/commit/bdab9e415d3cde42fef36bd912016dadc88e93ea)
- Réservation [`34f7459`](https://github.com/Dannebicque/intranetV3/commit/34f7459cedc3928bea659e8b125400658020ae34)
- Apogée [`1b2fb98`](https://github.com/Dannebicque/intranetV3/commit/1b2fb9892e0c2616a79f8c9f2d389d688e01b050)
- Bornes [`ac99d1e`](https://github.com/Dannebicque/intranetV3/commit/ac99d1eaf7b3b5afbcfcbb48b451ceb0d2ca65b7)
- Gestion des PDF EDT [`c1158be`](https://github.com/Dannebicque/intranetV3/commit/c1158bea81d0f7bec773f64ad3263cb5b072e00b)
- pdf [`a7bc63a`](https://github.com/Dannebicque/intranetV3/commit/a7bc63a4b796fb968b681b3789668cc45da6b092)
- assets [`3df751d`](https://github.com/Dannebicque/intranetV3/commit/3df751da7f1635a29f567373970b97a50233cdfc)
- Périodes de stage [`54c72d0`](https://github.com/Dannebicque/intranetV3/commit/54c72d029d421a07e57f00912cffcef14d2a3020)
- Périodes de stage [`1814492`](https://github.com/Dannebicque/intranetV3/commit/1814492cf30cc12a52223eb53ea78d9822df692a)
- Partie Stage [`09288b8`](https://github.com/Dannebicque/intranetV3/commit/09288b87faa5c43ed00a85d1f72f4cf924c58a1c)
- Bug des groupes [`8a03a31`](https://github.com/Dannebicque/intranetV3/commit/8a03a3146af0e1fa08231fe1c9cb8bf1b4248a44)
- Bug des groupes [`5858822`](https://github.com/Dannebicque/intranetV3/commit/5858822ef9b190eecc8b7d3806a50a8e69337c4a)
- Stage etape impression [`964aaab`](https://github.com/Dannebicque/intranetV3/commit/964aaabf72629dcf2d38d63adc0e68a8ee704270)
- Stage etape impression [`11d0372`](https://github.com/Dannebicque/intranetV3/commit/11d03721bef5ee6198dc3a4158a6e4fa78fe9ded)
- Templates des mails [`95c39ee`](https://github.com/Dannebicque/intranetV3/commit/95c39eeaf7f45c584282cc047e59a35194dbf215)
- Choix département [`f00f8fb`](https://github.com/Dannebicque/intranetV3/commit/f00f8fb5154f35651821d501f6aa0f7b2ea25f4b)
- Correctifs mise en page ergonomie partie stage. [`3eaa348`](https://github.com/Dannebicque/intranetV3/commit/3eaa3484d71d79f6204e3b20d832dbd0f5804b45)
- Tri des documents par libelle plutôt que date. [`a15052c`](https://github.com/Dannebicque/intranetV3/commit/a15052c67ebbbe81fad7f22bd55a880bc43ae8ab)
- Tri des documents par libelle plutôt que date. [`e0902f0`](https://github.com/Dannebicque/intranetV3/commit/e0902f0dccd9a2530383f31960a415cad92ea65e)
- Bigs mailer suite modification de structure [`8d36118`](https://github.com/Dannebicque/intranetV3/commit/8d36118cffb4328d50d6836596acbe5acfcfe993)
- Bug justificatif des absences. [`d0bfdb6`](https://github.com/Dannebicque/intranetV3/commit/d0bfdb643f8fc2f069c60462eb4d6f7eef59bf4b)
- Ical nom matière [`f69b26d`](https://github.com/Dannebicque/intranetV3/commit/f69b26d1dfcba66c21b95ac0d63cda7e192e0134)
- Prévisionnel [`411ae16`](https://github.com/Dannebicque/intranetV3/commit/411ae16b850f662a48fd56c9ffdef179fce6504f)
- Apogée pour un étudiant [`1836d19`](https://github.com/Dannebicque/intranetV3/commit/1836d1944a3e4892ce7cd4607f98e6be72b19e4b)
- Bug personnel/statut [`cfc4b08`](https://github.com/Dannebicque/intranetV3/commit/cfc4b08900602bd935f67b3b2307bda74fc7c81a)
- Bug justificatifs [`6fbf35c`](https://github.com/Dannebicque/intranetV3/commit/6fbf35c9aceebe709ae1e88a40b8280635c3db27)
- LDAP enseignant [`d622117`](https://github.com/Dannebicque/intranetV3/commit/d6221174f1691179551c767dfe17dee15f857073)
- LDAP enseignant [`5c21e57`](https://github.com/Dannebicque/intranetV3/commit/5c21e5721b1367b2eddf86ee2e5e024fc2089de5)
- Choix département par défaut [`e7e1638`](https://github.com/Dannebicque/intranetV3/commit/e7e1638fbe2a1a4462a8105a61719f3d59a1e0f0)
- pdf [`9a570dc`](https://github.com/Dannebicque/intranetV3/commit/9a570dc689777a2399e9b171c2942cadac152346)
- Bug des groupes [`482cd2f`](https://github.com/Dannebicque/intranetV3/commit/482cd2f9ecf77d85b0c82528bae24c9765bbe082)
- Bug des groupes [`37c188c`](https://github.com/Dannebicque/intranetV3/commit/37c188c3fac737311e53b78f7b5f38b47df679a3)
- Bugs [`1bffc52`](https://github.com/Dannebicque/intranetV3/commit/1bffc52ccf3ebb1aac3d5065f81de26c31bc55a3)
- Optimisation bg [`ce5de3a`](https://github.com/Dannebicque/intranetV3/commit/ce5de3a6c921823c2231720f93d1ec96ef229e6d)
- Ical + typo [`b22bba4`](https://github.com/Dannebicque/intranetV3/commit/b22bba4781ed6dcf41315d7e308cb24c6314ed63)
- Bug Apogee [`150a2d7`](https://github.com/Dannebicque/intranetV3/commit/150a2d7e0e6201893c09dd57affac57fef32ded8)
- Bug Apogee [`bf83ce7`](https://github.com/Dannebicque/intranetV3/commit/bf83ce756c414d7856c1c0c51a42f75927c246e6)
- Bug année universitaire absente sur rattrapage [`7f9af94`](https://github.com/Dannebicque/intranetV3/commit/7f9af94504ea1f1cd0df184debbda4668906c015)
- Correctif Salles [`f470c90`](https://github.com/Dannebicque/intranetV3/commit/f470c909ca6abbf1fc856e437f3e83c75fbeed68)
- Bigs mailer suite modification de structure [`42e35eb`](https://github.com/Dannebicque/intranetV3/commit/42e35ebc20e8bad3a5fd480e895f156468e08665)
- Message, typo [`8912322`](https://github.com/Dannebicque/intranetV3/commit/8912322054094709899551e33d2ca594d2785bc9)
- export PDF [`b715b1f`](https://github.com/Dannebicque/intranetV3/commit/b715b1fd1b358a00ae954d036344da67dcc046fe)
- Messagerie [`d11a7ac`](https://github.com/Dannebicque/intranetV3/commit/d11a7ac051ea227d03151704cd67195917975c67)
- Bug stage [`aae2b59`](https://github.com/Dannebicque/intranetV3/commit/aae2b597bf84ac6ba2c4b737918d4c0f1aec4945)
- Partie Stage [`979a46d`](https://github.com/Dannebicque/intranetV3/commit/979a46d6e1b987e96e40ce87e43d93cebd1e26fd)
- TravisCI [`633cec9`](https://github.com/Dannebicque/intranetV3/commit/633cec980eb4a605362e4066a22a0e4e3262203f)
- Bug enquête qualité [`29c13cb`](https://github.com/Dannebicque/intranetV3/commit/29c13cbfccb62ad3659b800900abb1e58815b249)
- don't inspect assets [`35d30c2`](https://github.com/Dannebicque/intranetV3/commit/35d30c2160e58078843c6fc90ae21de156188069)
- TravisCi [`f8dba67`](https://github.com/Dannebicque/intranetV3/commit/f8dba67b50ae1fff2b12f8566ebc2ab8364f58df)
- TravisCi [`00fbeba`](https://github.com/Dannebicque/intranetV3/commit/00fbeba10060f6cc2349a4961d2d63b0a3afb38a)
- Alternance [`e2ea7df`](https://github.com/Dannebicque/intranetV3/commit/e2ea7dfb73a2492ee6d591e5d3a1570ea83857ad)
-  Email [`a9a3aa8`](https://github.com/Dannebicque/intranetV3/commit/a9a3aa8cb585bfd71791e6d69ce37bd0465784c8)
- Icones apple [`3510104`](https://github.com/Dannebicque/intranetV3/commit/3510104c6c6264f403d8554df0a0852c8a4b83b9)
- Optimisation bg [`9ac9631`](https://github.com/Dannebicque/intranetV3/commit/9ac9631b2d592c5f09523b6203dcea1c5d6430a8)

## [1.0.1](https://github.com/Dannebicque/intranetV3/compare/1.0.0...1.0.1) - 2020-07-05

### Merged

- Organisation en onglet des pages "modules" [`#27`](https://github.com/Dannebicque/intranetV3/pull/27)
- Organisation en onglet des pages "modules" [`#26`](https://github.com/Dannebicque/intranetV3/pull/26)
- Develop [`#25`](https://github.com/Dannebicque/intranetV3/pull/25)
- Develop [`#24`](https://github.com/Dannebicque/intranetV3/pull/24)
- Develop [`#23`](https://github.com/Dannebicque/intranetV3/pull/23)
- Bump websocket-extensions from 0.1.3 to 0.1.4 [`#22`](https://github.com/Dannebicque/intranetV3/pull/22)

### Commits

- Mise à jour des statistiques des questionnaires, si plusieurs questionnaires [`0dec612`](https://github.com/Dannebicque/intranetV3/commit/0dec612a91403f135da8434efdafcbe02b8d5768)
- Passage de SF4.4 à 5.1 [`7e4ac52`](https://github.com/Dannebicque/intranetV3/commit/7e4ac52d12e3132b889ec76277b396ba91297dea)
- Correctif enquête [`09433d5`](https://github.com/Dannebicque/intranetV3/commit/09433d590bb6e0bbe6d893bb70e9b1a3e662ef53)
- Update CopyRight (PhpStorm Config) [`b01c974`](https://github.com/Dannebicque/intranetV3/commit/b01c97433ecbd14f14a974db357ae9374238182c)
- Passage à Uuid par symfony [`29a2668`](https://github.com/Dannebicque/intranetV3/commit/29a2668ac02aa47d7ef040318885f99a6f427b39)
- Renommage MesClasses en Classes [`b9d51fa`](https://github.com/Dannebicque/intranetV3/commit/b9d51fa700d03cfa6b1bd1d64dd2b39e1b3d6244)
- Update SF [`5f8952a`](https://github.com/Dannebicque/intranetV3/commit/5f8952aca791073dc243e8f55fa38e585f4e6ef7)
- Update 5.1.1 [`7ff218d`](https://github.com/Dannebicque/intranetV3/commit/7ff218d8282f0f47de8c81930f9adbf2b1489c51)
- Gestion département et UID [`bb459c0`](https://github.com/Dannebicque/intranetV3/commit/bb459c03f6469d245afdf2e65fa6e249c0955e70)
- Gestion des conditions sur les questions QCM [`77696b5`](https://github.com/Dannebicque/intranetV3/commit/77696b502f43d70c750d797a9096dcb2ae359d74)
- Renommage MesClasses en Classes [`b595d06`](https://github.com/Dannebicque/intranetV3/commit/b595d06f55a6f987eadc70b7031261da881716d9)
- Uuid type [`23508bc`](https://github.com/Dannebicque/intranetV3/commit/23508bc97a61b0e610d3fa666da0057ca926c521)
- Questions conditionnelles [`13babae`](https://github.com/Dannebicque/intranetV3/commit/13babae5c67bd6914cf771c242b7100cf3feef59)
- COrrectif pages sans login + CI [`ca8685b`](https://github.com/Dannebicque/intranetV3/commit/ca8685befb55a09355a8695955783ffcd85ceb9b)
- Gestion des conditions sur les questions QCM [`6f86b98`](https://github.com/Dannebicque/intranetV3/commit/6f86b983d98463f2ccc7b6000d8a191e2144d4a5)
- Optimisations Enquete [`5b0647f`](https://github.com/Dannebicque/intranetV3/commit/5b0647fb87d47eccfdb987b5ab351a351b76dee7)
- Gestion des conditions sur les questions QCM [`d180d88`](https://github.com/Dannebicque/intranetV3/commit/d180d888deb44cffcdb65de55d31b046b7efdfbf)
- Gestion des conditions sur les questions QCM [`bf86570`](https://github.com/Dannebicque/intranetV3/commit/bf865709372259b2b6c0a01b704a7fccbc1375c7)
- Questions conditionnelles [`24c615e`](https://github.com/Dannebicque/intranetV3/commit/24c615e3b1c7a4c18a3c903fafea1c6f362f1ad5)
- Optimisations Enquete [`33be806`](https://github.com/Dannebicque/intranetV3/commit/33be80610995c39a7994fd1c2981f17488a54518)
- Gestion des conditions sur les questions QCM [`3901be0`](https://github.com/Dannebicque/intranetV3/commit/3901be026d5150fd727bdb6459764d1d68ba21ef)
- Passage de SF4.4 à 5.1 [`e4c117e`](https://github.com/Dannebicque/intranetV3/commit/e4c117e78518b479317e6d004c565a9d197e2b2e)
- Update Jquery [`aad6733`](https://github.com/Dannebicque/intranetV3/commit/aad673367836c28103ef1ca082adb305758d7b6f)
- Gestion des conditions sur les questions QCM [`92aa08b`](https://github.com/Dannebicque/intranetV3/commit/92aa08b9a86ce94ef374cf6f002e93110457350c)
- Questions conditionnelles [`0649397`](https://github.com/Dannebicque/intranetV3/commit/0649397836f78310353363b50288c01ef54691dc)
- INTRANET-73 [Stage] Montant gratification en paramètre configurable par l'administrateur [`c78dcbc`](https://github.com/Dannebicque/intranetV3/commit/c78dcbc764e48dfc915aba6692d93410c27aaf7e)
- Gestion des conditions sur les questions QCM [`8e0edc5`](https://github.com/Dannebicque/intranetV3/commit/8e0edc5f8a1d5f10db4723f2ffe9f5129e089798)
- Gestion des conditions sur les questions QCM [`b2240ba`](https://github.com/Dannebicque/intranetV3/commit/b2240ba02917a1c6e06f6fa4f14937300876ff3c)
- Gestion département et UID [`266a2f8`](https://github.com/Dannebicque/intranetV3/commit/266a2f8b313988d38efbc3168213b2d32a066d53)
- Gestion des conditions sur les questions QCM [`d8285c8`](https://github.com/Dannebicque/intranetV3/commit/d8285c83c823a261ddf49137bb4670409c1ad38b)
- Typos [`dd3595d`](https://github.com/Dannebicque/intranetV3/commit/dd3595dfa6687a9b14b84004398fbc2ee45badf4)
- Optimisations Enquete [`6611469`](https://github.com/Dannebicque/intranetV3/commit/6611469b7a80e4c3ccd1b8345bcf3101e80bde18)
- Optimisations Enquete [`b74213d`](https://github.com/Dannebicque/intranetV3/commit/b74213de7459f99e49dc1278f29bfb8bcaf09668)
- Gestion des conditions sur les questions QCM [`ff9bbb1`](https://github.com/Dannebicque/intranetV3/commit/ff9bbb16d7c819f2b4aea53d552496ec7ebd3bbb)
- Gestion des conditions sur les questions QCM [`6ab8391`](https://github.com/Dannebicque/intranetV3/commit/6ab8391401b0045922f60d34a1d842cca00524cf)
- Gestion des conditions sur les questions QCM [`240fe0f`](https://github.com/Dannebicque/intranetV3/commit/240fe0f2c91084b6212f4a6923a25ebf51aaeb27)
- Questions conditionnelles [`0ddbb7e`](https://github.com/Dannebicque/intranetV3/commit/0ddbb7e7acfdb8359d872b294dde9ec93dff6121)
- Manque du fichier bootstrap.php [`a8553ca`](https://github.com/Dannebicque/intranetV3/commit/a8553caaa77ee81ad73b5e76947d6eb4b8015e76)
- Gestion des conditions sur les questions QCM [`77dff0b`](https://github.com/Dannebicque/intranetV3/commit/77dff0beac1c9f12bc9cc926e285636880a09f7a)
- Correction bug, function inside loop [`5132134`](https://github.com/Dannebicque/intranetV3/commit/51321345d220609c13f9c85163683b4a11ab67ce)
- COrrectif pages sans login + CI [`94abfe4`](https://github.com/Dannebicque/intranetV3/commit/94abfe42368950095e88f8c6fa60b32023a891f2)
- Update readme.md [`1d46b6d`](https://github.com/Dannebicque/intranetV3/commit/1d46b6d50c998d969d96d4062b28ce262f0c9aa1)
- Update readme.md [`591cc54`](https://github.com/Dannebicque/intranetV3/commit/591cc54694a31c4ecd5beace7ab7294cc500d021)
- Gestion des conditions sur les questions QCM [`3f59d9a`](https://github.com/Dannebicque/intranetV3/commit/3f59d9a52336d97c5c180b98de40b50c64417dc8)
- Questions conditionnelles [`37fd6db`](https://github.com/Dannebicque/intranetV3/commit/37fd6db6123a7e18b0865b9c5e0673eaac419e35)
- COrrectif pages sans login + CI [`eddb948`](https://github.com/Dannebicque/intranetV3/commit/eddb948c9647fa1e262f2e551e70a47f93198816)
- Gestion des conditions sur les questions QCM [`6cc623c`](https://github.com/Dannebicque/intranetV3/commit/6cc623c9f5562aa17910aa9bd807171a2177233e)
- Gestion des conditions sur les questions QCM [`452bf63`](https://github.com/Dannebicque/intranetV3/commit/452bf631659451038cef1a5ddf2dd353d0e8c4af)

## 1.0.0 - 2020-05-22

### Merged

- Update support into guard CAS [`#19`](https://github.com/Dannebicque/intranetV3/pull/19)
- [SCOLARITE] - Partie Questionniaire [`#18`](https://github.com/Dannebicque/intranetV3/pull/18)
- Bump lodash.mergewith from 4.6.1 to 4.6.2 [`#14`](https://github.com/Dannebicque/intranetV3/pull/14)
- Bump mixin-deep from 1.3.1 to 1.3.2 [`#13`](https://github.com/Dannebicque/intranetV3/pull/13)
- Bump lodash from 4.17.11 to 4.17.15 in /assets/the_admin [`#12`](https://github.com/Dannebicque/intranetV3/pull/12)
- Bump tar from 2.2.1 to 2.2.2 in /assets/the_admin [`#11`](https://github.com/Dannebicque/intranetV3/pull/11)
- Bump lodash.mergewith from 4.6.1 to 4.6.2 in /assets/the_admin [`#10`](https://github.com/Dannebicque/intranetV3/pull/10)
- Bump js-yaml from 3.13.0 to 3.13.1 in /assets/the_admin [`#9`](https://github.com/Dannebicque/intranetV3/pull/9)
- Bump mixin-deep from 1.3.1 to 1.3.2 in /assets/the_admin [`#8`](https://github.com/Dannebicque/intranetV3/pull/8)
- Bump fstream from 1.0.11 to 1.0.12 in /assets/the_admin [`#7`](https://github.com/Dannebicque/intranetV3/pull/7)
- Add shebang to deploy script [`#4`](https://github.com/Dannebicque/intranetV3/pull/4)

### Commits

- Mise en place docker et bonne structure (MPIOT) [`40947dd`](https://github.com/Dannebicque/intranetV3/commit/40947dd461dfdbe2e7e815109a1c9c30c9390eab)
- Remove previous assets [`43c5646`](https://github.com/Dannebicque/intranetV3/commit/43c56461daf670b34ee2bbbf532841a1f8c9e0fc)
- suppression du répertoire assets [`148db8a`](https://github.com/Dannebicque/intranetV3/commit/148db8aa93f1a8aea3eaa4a53754c280b0dde6da)
- Assets [`273dbd4`](https://github.com/Dannebicque/intranetV3/commit/273dbd4373890bd3d971fe822208d2d4b09006c3)
- Use Webpack to manage assets [`846aa6f`](https://github.com/Dannebicque/intranetV3/commit/846aa6fce7b931277f6cf9cbe411c3b6c921b299)
- Security avec Guard + Année Universitaire [`a713010`](https://github.com/Dannebicque/intranetV3/commit/a7130102de52af2174a8cc0dd6c5dcd04bd3914d)
- Messagerie / Notifications [`2052193`](https://github.com/Dannebicque/intranetV3/commit/20521937096a05f53f5c7b06de4aaa9598582b90)
- rebase en 4.2 [`cba4f1f`](https://github.com/Dannebicque/intranetV3/commit/cba4f1fed12f3681e0e833d6806aa06c2137e3e9)
- rebase en 4.2 [`e6ca362`](https://github.com/Dannebicque/intranetV3/commit/e6ca36298fcc7c90ca4d1539624060922ad6b39e)
- Quizz/Qualite en SuperAdmin [`02e22aa`](https://github.com/Dannebicque/intranetV3/commit/02e22aaf348db4fcdbdc9aea897fab229e76e1aa)
- Partie disponibilité pour les personnels [`17aeebd`](https://github.com/Dannebicque/intranetV3/commit/17aeebdd1c25e1edc961fcc9d05dd86471f0def1)
- Partie message. Envoi partiel. Liste des messages et états des messages [`d018ab6`](https://github.com/Dannebicque/intranetV3/commit/d018ab69c6aaec87827cddce0177a67f220b730e)
- Apogee [`1b8009e`](https://github.com/Dannebicque/intranetV3/commit/1b8009e50dec425c95346bf34b30d10f171cbb40)
- Import étudiants en super Admin [`d8b7cc5`](https://github.com/Dannebicque/intranetV3/commit/d8b7cc5e6b80c53ef3da15596fd5b8d91017256b)
- Correctifs mineurs [`67165e8`](https://github.com/Dannebicque/intranetV3/commit/67165e860fc5f97291e3807cf06ffd6e59555aa4)
- Groupes et traductions [`c56b169`](https://github.com/Dannebicque/intranetV3/commit/c56b1692a06acc78b2efb9bfab98081ca7933003)
- Gestion des groupes (type de groupe OK en JS) [`f8cffe8`](https://github.com/Dannebicque/intranetV3/commit/f8cffe8c077cde2e030c98e7561b5c86b332f031)
- Delete package-lock.json [`484c3b5`](https://github.com/Dannebicque/intranetV3/commit/484c3b5a696b003bb5f34a1d6431a629357353f1)
- Apogee [`9431ff9`](https://github.com/Dannebicque/intranetV3/commit/9431ff94baf5fb68950a38e7f21220d2144d7164)
- Qualité de code [`4833154`](https://github.com/Dannebicque/intranetV3/commit/4833154649a863914fae0fafa9cdec9e6559d286)
- [SUPERADMIN] - Partie Super Administration [`3063740`](https://github.com/Dannebicque/intranetV3/commit/306374032b3942859ddd527d49455dbfc881b1a4)
- Nettoyage de code [`b6edae1`](https://github.com/Dannebicque/intranetV3/commit/b6edae14644c5462b16564be51293d05409af1cd)
- Twig Extra Bundle et Intl [`3f19cc7`](https://github.com/Dannebicque/intranetV3/commit/3f19cc70b77c2d7147ae10905d48483fb6f932f9)
- Partie offre de stage en étudiant. [`8c113eb`](https://github.com/Dannebicque/intranetV3/commit/8c113eb29ef433652e2e45cf8b9bd0446c4658fd)
- Loader [`655db3a`](https://github.com/Dannebicque/intranetV3/commit/655db3a721cfebbca5e269d0ea9ea32096324b78)
- Nombre d'étudiants par diplôme [`ead0906`](https://github.com/Dannebicque/intranetV3/commit/ead0906fea9ef6c6084051965bd26f65cd41e5e1)
- Correctifs qualité. [`055d5e3`](https://github.com/Dannebicque/intranetV3/commit/055d5e3763884988ea2a36afa7851e818d78f4b6)
- [SCOLARITE] - Partie scolarite Administratif [`0527f3c`](https://github.com/Dannebicque/intranetV3/commit/0527f3c985da06e78209d07572d9e7f5ea981543)
- Passage à Twig 3.0 [`a9a1337`](https://github.com/Dannebicque/intranetV3/commit/a9a1337eec2cac40ed35fc3da10f05c5508775c1)
- Correctifs [`5231cdf`](https://github.com/Dannebicque/intranetV3/commit/5231cdf577c97afdf80b5ae4caa404673453429a)
- Ajout de Php-Translation [`79aa1d3`](https://github.com/Dannebicque/intranetV3/commit/79aa1d3bfc5eb792e7075e87e97dda04c87d2d02)
- Correctifs de Todo. [`cbd14e0`](https://github.com/Dannebicque/intranetV3/commit/cbd14e0b201a9d358d3ac424209df16e17cc294c)
- Stucture pour sous-commission [`5720d40`](https://github.com/Dannebicque/intranetV3/commit/5720d403852db6a50e90dc3735ea9873cd7b81e5)
- INTRANET-55 [Emprunts] :gestion matériel et emprints [`447a23d`](https://github.com/Dannebicque/intranetV3/commit/447a23db278770ab6aebe6ae5b6142cd2932b8ce)
- INTRANET-12 Finalisation globale de la partie stage [`deac9f0`](https://github.com/Dannebicque/intranetV3/commit/deac9f05d7f106db6d89e53526e49f1798367239)
- Code Quality [`89c37ac`](https://github.com/Dannebicque/intranetV3/commit/89c37ac67a45d4172a143ce975dd127914e0069c)
- Transformation de la notion de formation en département [`80c2e93`](https://github.com/Dannebicque/intranetV3/commit/80c2e9374fc9880cfc37f943aeba07005cc6e2e4)
- [SCOLARITE] - Partie scolarite Administratif [`dfeab0f`](https://github.com/Dannebicque/intranetV3/commit/dfeab0f9e8e9fc64c7e9d481557ada99c99402f5)
- Bug affichage EDT depuis Celcat [`a376ec7`](https://github.com/Dannebicque/intranetV3/commit/a376ec777bf4a1481d2c591c851d1bdfab02f675)
- Partie gestion des disponibilités avec création des créneaux et blocages des créneaux types [`6dd06ae`](https://github.com/Dannebicque/intranetV3/commit/6dd06ae3417b01fc2bb9c7b3c48c0ab75e02168e)
- Mise à jour + ajout du CAS [`3769c6d`](https://github.com/Dannebicque/intranetV3/commit/3769c6ddfc5336033d4cc313d4edbc1db2608630)
- INTRANET-2 Intégrer double authentification CAS + Symfony [`1181dbb`](https://github.com/Dannebicque/intranetV3/commit/1181dbbcf64f019e622a0f68683ab6cb8793a873)
- Divers correctifs : [`2dd2283`](https://github.com/Dannebicque/intranetV3/commit/2dd2283c7a19bf88cbadce6625eead3caf1ecd3a)
- Emploi du temps profs et étudiants. Emploi du temps promo en admin. Todo: filtres [`448a0fa`](https://github.com/Dannebicque/intranetV3/commit/448a0fa6738cd761743f515fed2e99ffe210b5af)
- Passage en 4.3 stable + correctifs mineurs + prise en compte du PPn selon le semestre [`be01a64`](https://github.com/Dannebicque/intranetV3/commit/be01a646ec40c3b1032f9d3cd1fc80d517986507)
- correctifs sur la partie étudiant (profil, edt, applications) [`7b0971d`](https://github.com/Dannebicque/intranetV3/commit/7b0971ded531217718d222fac351998b0fd0675f)
- Refonte du trombinoscope et footer document PDF [`f5ee901`](https://github.com/Dannebicque/intranetV3/commit/f5ee901efdd3d98e27aeedb984891e1395d2c4c6)
- Partie EDT : Compare et Réalise (non testé). Harmonisation des signatures dans repository. [`5cd0765`](https://github.com/Dannebicque/intranetV3/commit/5cd07657dda3319454bbd49f3f04bc45b07672aa)
- correctifs + prévisionnels (modals) + suppressions de todos [`c2c2ab1`](https://github.com/Dannebicque/intranetV3/commit/c2c2ab134904a291b52fb5a6639b4dfe14dcbbee)
- [SCOLARITE] - Partie scolarite Administratif [`f095e2e`](https://github.com/Dannebicque/intranetV3/commit/f095e2ecaaf17310423ee33b754e740cf39c5213)
- Partie RGPD + menu dans l'administration selon les roles [`1c51275`](https://github.com/Dannebicque/intranetV3/commit/1c51275bae2aa7f6c53b01b8acb80076d7cc556f)
- correctifs suite à l'analyse de code [`6f2bc38`](https://github.com/Dannebicque/intranetV3/commit/6f2bc381e8bc07bdbf39918882256aecc6410fdc)
- Partie suivi alternance pour un personnel [`a4f7a42`](https://github.com/Dannebicque/intranetV3/commit/a4f7a42dee5a5025cdc2b3439a6a0acd5e00ef53)
- Code Quality [`755a9f5`](https://github.com/Dannebicque/intranetV3/commit/755a9f5ddfcd192803a1785266bbb227e3370a1f)
- Questionnaire Qualite. [`e9af164`](https://github.com/Dannebicque/intranetV3/commit/e9af164e28a17100514bc17ade94cb7ba8cfc693)
- correctifs profils notes et absences [`ef5ca72`](https://github.com/Dannebicque/intranetV3/commit/ef5ca7272502cb0e60b3a5833745a61a549ee8a0)
- [QUALITE] : Mise en place des questionnaires étudiants [`0edda5f`](https://github.com/Dannebicque/intranetV3/commit/0edda5f4cfa3c8032bd58181b8448a07a862c3ee)
- Gestion des articles et des likes selon le type d'utilisateur [`f141964`](https://github.com/Dannebicque/intranetV3/commit/f141964ab6c9539705435b2556012f4f63c70e06)
- Génération des conventions stages + Restructuration UFR-&gt;Sites [`e5d43f7`](https://github.com/Dannebicque/intranetV3/commit/e5d43f7c194d122c3675dcad9bbcf0501282da94)
- Edt avec celcat pour etudiants et personnel. [`65c659a`](https://github.com/Dannebicque/intranetV3/commit/65c659ad9986694ec450b98fb5f8bc4e24014d77)
- INTRANET-2 Intégrer double authentification CAS + Symfony [`cac072e`](https://github.com/Dannebicque/intranetV3/commit/cac072eec74d6f97168ebf525f33d31176764b2f)
- Partie planning de rattrapage en admin [`955005f`](https://github.com/Dannebicque/intranetV3/commit/955005f6f7dae7c782a3afab9eb7e836d7ebb1a2)
- correctifs profil étudiant + details EDT [`863a235`](https://github.com/Dannebicque/intranetV3/commit/863a235fc51e1bc0d59a561dd1ff3655c3a574ee)
- Partie stage coté étudiant et permanent [`c3563cb`](https://github.com/Dannebicque/intranetV3/commit/c3563cb6f3f06b5834dccb0852eb9a5939c7c526)
- Questionnaire Qualite. [`707617e`](https://github.com/Dannebicque/intranetV3/commit/707617e285bc0f1f2e350aabcf6935f53af2618a)
- INTRANET-29 Imports etudiants depuis Apogée [`2255191`](https://github.com/Dannebicque/intranetV3/commit/2255191615bc37c9e6200c2651f73d5c755c1cfb)
- Divers correctifs + qualité de code [`7525658`](https://github.com/Dannebicque/intranetV3/commit/752565833cbb0d20bead6531021f3b1c5b9de0e4)
- Security avec Guard + Année Universitaire [`9772140`](https://github.com/Dannebicque/intranetV3/commit/9772140a31e5b40ae7f93ba9375e5495135d4087)
- Correctifs synchro Apogee (gestion du bac, date de bac). [`f17d852`](https://github.com/Dannebicque/intranetV3/commit/f17d8528e3922b1152a3768e8694a947889be9ed)
- Finalisation de tous les CRUD du super-admin [`92bf51c`](https://github.com/Dannebicque/intranetV3/commit/92bf51cdaf28f87185b0f6de2cd6f1995e3e70bc)
- INTRANET-27 Saisie du prévisionnel [`439f541`](https://github.com/Dannebicque/intranetV3/commit/439f54115f9501b2789b0aa5e3855e0108868ad7)
- transformation des eventDispatch avec le nouveau format. [`a6a2d00`](https://github.com/Dannebicque/intranetV3/commit/a6a2d0022bc2f99dc753d1a25ac07b73a5554b53)
- Correctifs qualité. [`9f43705`](https://github.com/Dannebicque/intranetV3/commit/9f43705a484f725c904efc2d3e38083cfe1c0a52)
- Remise au propre des codes en lien avec Apogée. [`e7fc967`](https://github.com/Dannebicque/intranetV3/commit/e7fc96751d27005c8e7a8fb66d87a93ad83736ff)
- Corrections de bugs et qualité (scrutinizer) [`0cb3382`](https://github.com/Dannebicque/intranetV3/commit/0cb3382ba7c9ed2534a11acd15d893bfeae76424)
- Saisie d'une évaluation depuis l'administration [`9f9d47b`](https://github.com/Dannebicque/intranetV3/commit/9f9d47b2dfb702b2d783683de89bea0d64f86327)
- LDAP/Groupes [`14a6603`](https://github.com/Dannebicque/intranetV3/commit/14a660320305bb542cca5bca6a5d6a11a6d1a47f)
- Traitement des todos [`3433c54`](https://github.com/Dannebicque/intranetV3/commit/3433c5474c99ea1dac2ea3697d789aef6103953d)
- Modification gestion qualite. [`fb755e6`](https://github.com/Dannebicque/intranetV3/commit/fb755e6d31e8f21ff189454db9d46b87ef4110ec)
- Init Docker [`ae3d278`](https://github.com/Dannebicque/intranetV3/commit/ae3d2784d78715a4b72a12650306131b209e14d2)
- INTRANET-29 Imports etudiants depuis Apogée [`aad1ccc`](https://github.com/Dannebicque/intranetV3/commit/aad1ccc35efcc1a60611301729a725c8d12305eb)
- Correctifs [`29ee30e`](https://github.com/Dannebicque/intranetV3/commit/29ee30e227f999ff665f7bb42e0ac1762ea410cc)
- Début de mise en place de l'EDT depuis fichiers (pas celcat) [`0b8c5cc`](https://github.com/Dannebicque/intranetV3/commit/0b8c5cc65bcd2521436ae61775d4026899177acc)
- INTRANET-58 [Arbo] Fusionner Super-Admin et Administratif, et jouer avec des "sous-roles" [`4613e52`](https://github.com/Dannebicque/intranetV3/commit/4613e522d1e84fe242e077c9c59791191896194c)
- Partie progression pédagogique [`7784592`](https://github.com/Dannebicque/intranetV3/commit/7784592415312f9c8ca6ebea50ad7cb39c0dcf1e)
- Optimisation qualité de code [`9820090`](https://github.com/Dannebicque/intranetV3/commit/9820090969840f51b609d113ebb26a7ff196f5de)
- Partie qualité [`79fcfc7`](https://github.com/Dannebicque/intranetV3/commit/79fcfc7633085e836d6589bb728f246d6caaebfc)
- Partie progression pédagogique [`fab9d28`](https://github.com/Dannebicque/intranetV3/commit/fab9d28840ab415d27ac9ec45066f8b1b53b4f4c)
- update [`957a04a`](https://github.com/Dannebicque/intranetV3/commit/957a04adff3511a74761664c4d192ecb61fb6872)
- Fix Migrations [`e7d58b9`](https://github.com/Dannebicque/intranetV3/commit/e7d58b94c0c0a4facd8ee4582997cc055401534c)
- Divers correctifs [`bc5e882`](https://github.com/Dannebicque/intranetV3/commit/bc5e882d6c966748fcd9cddc18de8c43ec1c4626)
- quelques bugs [`0885315`](https://github.com/Dannebicque/intranetV3/commit/0885315a27ef73e2e636ad7a0ddc6f0ffb80f7ce)
- Edt + Edit (enregistrer et index) [`bef2d61`](https://github.com/Dannebicque/intranetV3/commit/bef2d6125c5129ede0a5f7a1f67a57c3862780bc)
- INTRANET-10 Finaliser les traductions [`9a611e3`](https://github.com/Dannebicque/intranetV3/commit/9a611e3725dbf72fa18f4d08722d5b2238fdd521)
- Accessibilité [`a3c5043`](https://github.com/Dannebicque/intranetV3/commit/a3c504391ee5d1f41be7ee19e4f9e7092ff34ad3)
- Synchro Celcat [`246bb68`](https://github.com/Dannebicque/intranetV3/commit/246bb68fd5a38fbef7eb311d7a82e6a12df6effd)
- Partie Bornes EDT + messages [`57d2e42`](https://github.com/Dannebicque/intranetV3/commit/57d2e423b288dcbb08f744440de308d090ab428a)
- Bug suppression groupe dans le profil [`11364b1`](https://github.com/Dannebicque/intranetV3/commit/11364b157a1bdae1ea33f9272f635c33e2ea09f8)
- Code Quality [`d85c8e3`](https://github.com/Dannebicque/intranetV3/commit/d85c8e3645131b81a0a588b81d0713d337d1587d)
- Correctifs profil + CSS [`707e3db`](https://github.com/Dannebicque/intranetV3/commit/707e3db39c3f41cf212a31b1784768db6af34c15)
- Partie alternance pour étudiant et RP. (manque export) [`0c18c4e`](https://github.com/Dannebicque/intranetV3/commit/0c18c4ef5bc10ed8de83e598e42853dfecb80197)
- Correctifs + qualité de code [`ee95766`](https://github.com/Dannebicque/intranetV3/commit/ee957668105ca230dc28ca47422e766116caea07)
- Questionnaires [`02136ca`](https://github.com/Dannebicque/intranetV3/commit/02136ca19f5ea9252704d2cff97422340fd38604)
- INTRANET-2 Intégrer double authentification CAS + Symfony [`9cc7879`](https://github.com/Dannebicque/intranetV3/commit/9cc7879216d22e3eb2dcd3f4c353a40cfca5e964)
- Partie alternance/stage en application. [`5f8310f`](https://github.com/Dannebicque/intranetV3/commit/5f8310fcae1be16b7a62a9b9e307910b8dda3734)
- INTRANET-12 Finalisation globale de la partie stage [`0d7545c`](https://github.com/Dannebicque/intranetV3/commit/0d7545c1eec597fe738d3907894c9a91d568bd19)
- INTRANET-6 Proposer ICAL aux étudiants [`24e1362`](https://github.com/Dannebicque/intranetV3/commit/24e1362244678537e55b3f34e2fee675be099d6a)
- Activer/désactiver la structure de la formation [`6929571`](https://github.com/Dannebicque/intranetV3/commit/6929571e55baf5466529b0bc688782f0aff92126)
- Partie message. Refonte de l'interface [`7f0e029`](https://github.com/Dannebicque/intranetV3/commit/7f0e0293b49694ed277894ac3417c1e9e6c417b4)
- Fixtures (utilisateurs, formation, sites, bacs). [`929fb6d`](https://github.com/Dannebicque/intranetV3/commit/929fb6d48ee21ed37c0bef62e29d2d2d0c1e54c0)
- Partie import des groupes en super admin [`a3165c5`](https://github.com/Dannebicque/intranetV3/commit/a3165c50d2b38e4698cd20e5371c25766289cb50)
- Affiche EDT en admin avec filtre salle, prof et matière [`2b075e9`](https://github.com/Dannebicque/intranetV3/commit/2b075e9cba9eef0afaa97ea32cfe3c74b7892a09)
- Génération Excel pour l'enquête qualité [`16149c0`](https://github.com/Dannebicque/intranetV3/commit/16149c0f86f720ee9af3deb257941f5faf0fda12)
- Installation de Liip Imagine Bundle pour le resize d'image [`2fdcdcb`](https://github.com/Dannebicque/intranetV3/commit/2fdcdcb04774dcc488c88e094e1bdfdeac1cd74a)
- INTRANET-1 Intégrer Apogée [`330cddc`](https://github.com/Dannebicque/intranetV3/commit/330cddc0eabc2cf31efc384c9345ab4e22248d67)
- Correctif enquête [`003d550`](https://github.com/Dannebicque/intranetV3/commit/003d5503175ad3ea4c1ae2a79d7468d6b0715f53)
- Fonctionnalités du profil et de l'onglet actions (changement semestre, groupes, département). [`0322912`](https://github.com/Dannebicque/intranetV3/commit/032291292f904d32429dac35a012b23780866a8d)
- Gestion des absences et justification dans profil [`caec462`](https://github.com/Dannebicque/intranetV3/commit/caec4629a6a3b9ddf42003852f60d89ed012f496)
- Finalisation de tous les CRUD du super-admin [`839b6bb`](https://github.com/Dannebicque/intranetV3/commit/839b6bb5c16a9567f4d73547ee7787efc24a70e1)
- INTRANET-14 Remplacer année universitaire int par la objet année universitaire. [`d8a9fc9`](https://github.com/Dannebicque/intranetV3/commit/d8a9fc9694485737d564b3f5632035c938da88be)
- INTRANET-55 [Emprunts] :gestion matériel et emprunts [`c2edc31`](https://github.com/Dannebicque/intranetV3/commit/c2edc317a73a34f71ceafd10e3a3ab45850a43a0)
- Edt et prévisionnels [`ca3d9a3`](https://github.com/Dannebicque/intranetV3/commit/ca3d9a35cdb9ffb70fb66020721f341b507e5843)
- suppression répertoire assets [`aab193e`](https://github.com/Dannebicque/intranetV3/commit/aab193e3708216f132884552dfec09d222ad9d39)
- Partie export EDT [`bb4f871`](https://github.com/Dannebicque/intranetV3/commit/bb4f871c94cd11425900ec814c582faf17e8391a)
- Sous commission export [`e6b0e0b`](https://github.com/Dannebicque/intranetV3/commit/e6b0e0b51da7db6e78215e64c86b3b3e06961b74)
- Divers correctifs [`728b7d1`](https://github.com/Dannebicque/intranetV3/commit/728b7d1720e430991658f13d7d1ec0be064100a9)
- Groupes et quelques todos en JS [`1b59532`](https://github.com/Dannebicque/intranetV3/commit/1b59532cd62a6c9558ead5acb5caac881278cad9)
- Correctif CSS [`b1efd0c`](https://github.com/Dannebicque/intranetV3/commit/b1efd0c579cae53e6ed1fafc89ccc06c40f12df6)
- Correctifs sur Absence + transformation des eventDispatch avec le nouveau format. [`c05ac72`](https://github.com/Dannebicque/intranetV3/commit/c05ac7214a2e40228b4832694ce2cde1a3a0dbbe)
- [Structure] : Possibilité de Marquer une UE en "bonification" [`751616c`](https://github.com/Dannebicque/intranetV3/commit/751616cda5e2813b9944f37d22d1626779e986a1)
- Correctifs fiches de suivi des alternants en Admin [`cb1fa62`](https://github.com/Dannebicque/intranetV3/commit/cb1fa6287b71d88ffbe138e55a963bcc05868bbb)
- Partie EDT semestre [`3d53785`](https://github.com/Dannebicque/intranetV3/commit/3d537854ee3c950b549aa0e74812355643994b93)
- Import d'une liste de matière [`5ce2652`](https://github.com/Dannebicque/intranetV3/commit/5ce265220c696298395262ef075e4860a5d18135)
- Correctifs qualité. [`4dd360c`](https://github.com/Dannebicque/intranetV3/commit/4dd360ce54e9f5fe9a8bed518ae82e2d0e083e27)
- Correctifs qualité [`9dc7824`](https://github.com/Dannebicque/intranetV3/commit/9dc782479e4de20227af756f5e34a51869e86c62)
- Quelques bugs [`fb9020b`](https://github.com/Dannebicque/intranetV3/commit/fb9020b5beee6afaf30165e8d4d03d5a24b62890)
- Correctifs partie Matieres en administration [`0831a55`](https://github.com/Dannebicque/intranetV3/commit/0831a55fb7eb0a580fed6b71388c1a874ee7526f)
- Possibilité de supprimer un créneau d'EDT [`083d1a3`](https://github.com/Dannebicque/intranetV3/commit/083d1a396f56b0260e70c32d7eb9e55b27ec1774)
- Trombinoscope PDF [`9b92c46`](https://github.com/Dannebicque/intranetV3/commit/9b92c465f89ad160619b983f999544745c140756)
- Partie messagerie [`7fbdf19`](https://github.com/Dannebicque/intranetV3/commit/7fbdf1936657587e46accf8912b3457ef5e5c9eb)
- Messagerie / Notifications [`8af5e97`](https://github.com/Dannebicque/intranetV3/commit/8af5e9742048f24880ebea9f9e08693e3032f006)
- Correctifs (+ editable) sur prévisionnel [`5f26ca1`](https://github.com/Dannebicque/intranetV3/commit/5f26ca18fb64563759bcde59395a6f2f28d450a1)
- Correctifs partie structure [`d8490c2`](https://github.com/Dannebicque/intranetV3/commit/d8490c2511e618650298fe740ac1450c41171eba)
- Sous commission live (sans export) [`e914cd4`](https://github.com/Dannebicque/intranetV3/commit/e914cd495d32a15835596e7d2474e4b11ae093a8)
- Correctif enquête [`c96bc8f`](https://github.com/Dannebicque/intranetV3/commit/c96bc8f1d077df0359d6703fd5e1a457801a1a07)
- Amélioration de l'accessibilité. [`2521d9e`](https://github.com/Dannebicque/intranetV3/commit/2521d9eb0f8a9d390231760786fb223e35801f73)
- Correctifs partie période de stage [`7281528`](https://github.com/Dannebicque/intranetV3/commit/72815287d4475795d2910b75dc47ead368ab17da)
- Quizz/Qualite en SuperAdmin [`534f8a6`](https://github.com/Dannebicque/intranetV3/commit/534f8a682935510c528482caddd1e48df422984f)
- Correctifs saisie des notes et évaluations. [`3e0a2e9`](https://github.com/Dannebicque/intranetV3/commit/3e0a2e9c47f3f74d0a2c0cd842302035b7f74b9d)
- Partie étudiant: applications + justificatifs + emprunt [`1ead76f`](https://github.com/Dannebicque/intranetV3/commit/1ead76f364fbe60dc541c95814c6e6db835d466c)
- Affichage EDT Prof et etudiant avec créneaux de cours hors 1h30 [`4ae5066`](https://github.com/Dannebicque/intranetV3/commit/4ae506657549554ff47547b15dccd90b9e6dedd6)
- Génération Excel pour l'enquête qualité [`64cec4b`](https://github.com/Dannebicque/intranetV3/commit/64cec4b148ebd6449f05c6220154c8206b14ad2a)
- Initialisation des évaluations [`f13dad5`](https://github.com/Dannebicque/intranetV3/commit/f13dad5c81af6aeeeac0365f057304b36700e81f)
- Afficher/masquer dans borne [`427d251`](https://github.com/Dannebicque/intranetV3/commit/427d251dcb903663d0b64a29df389eaf46d6e24d)
- CAS + Double authentification [`d904d4e`](https://github.com/Dannebicque/intranetV3/commit/d904d4ef5c50e83a1061fa0363e4df25e040e2bc)
- Ajout adresse lieu alternance [`0aef334`](https://github.com/Dannebicque/intranetV3/commit/0aef334fbaf891af208dba570791239213e01d53)
- INTRANET-2 Intégrer double authentification CAS + Symfony [`8517736`](https://github.com/Dannebicque/intranetV3/commit/85177369262eacb4f8e18e3602afd07df1ddda80)
- INTRANET-51 [messagerie] Divers correctifs sur la messagerie. [`6b3d23e`](https://github.com/Dannebicque/intranetV3/commit/6b3d23eb4aeb73b47de8d21618dd716bcffe43bc)
- Partie import des groupes en super admin [`48904c6`](https://github.com/Dannebicque/intranetV3/commit/48904c6387fd9c41455e603aeb23c463f01643aa)
- INTRANET-58 [Arbo] Fusionner Super-Admin et Administratif, et jouer avec des "sous-roles" [`f304581`](https://github.com/Dannebicque/intranetV3/commit/f3045817d5be6d133e14bc63f9cd6e2a8d0264ab)
- Correctifs + Accessibilité [`422295f`](https://github.com/Dannebicque/intranetV3/commit/422295fbfcb30d4991ee96330e9a0793ce963269)
- Modification intervenant prévisionnel [`56d6554`](https://github.com/Dannebicque/intranetV3/commit/56d655400555255e8b8c63a82edfd1cdf7308387)
- Correctifs qualité [`0c64877`](https://github.com/Dannebicque/intranetV3/commit/0c64877315fe407c4f9ddfb2e272f0582b575de5)
- Bugs et optimisations pour la partie questionnaire-qualité [`5864f91`](https://github.com/Dannebicque/intranetV3/commit/5864f91c8fa9a21deb6ac83021ddba0d5f62aeb0)
- Correctifs saisie des notes et évaluations. [`fe9133a`](https://github.com/Dannebicque/intranetV3/commit/fe9133a31a9c666cb47f51316340675684e7be1d)
- INTRANET-45 [ADMIN. GROUPES] : synchro celcat [`c320d20`](https://github.com/Dannebicque/intranetV3/commit/c320d20c335ffa5b1121ea80774565ad44f02024)
- Partie message. Refonte de l'interface [`2dac757`](https://github.com/Dannebicque/intranetV3/commit/2dac757b066fd06c90d16c656c32f6009d474a3b)
- Partie Calendrier [`0dfd1d9`](https://github.com/Dannebicque/intranetV3/commit/0dfd1d96bb38a26beb24ec76d6fa6ef6af57dab9)
- Correctifs : Annee universitaire, import celcat et bac. [`58bf4b8`](https://github.com/Dannebicque/intranetV3/commit/58bf4b82e447470acf57d2dc8ae154cccef1b07b)
- Traitement des todos [`6644649`](https://github.com/Dannebicque/intranetV3/commit/664464987534a069508127c996e166fb19692c03)
- INTRANET-29 Imports etudiants depuis Apogée [`883364f`](https://github.com/Dannebicque/intranetV3/commit/883364f6e24c4a3fbe3a97420af8e2bc82116f1d)
- Modification super-administration en administratif [`8fd6af0`](https://github.com/Dannebicque/intranetV3/commit/8fd6af017ed5ba300d38a6cca42a83ee1ac5d461)
- Correctifs + mail [`b7d6ceb`](https://github.com/Dannebicque/intranetV3/commit/b7d6ceba52d7b92053d037d146f3047d1ddf3ee5)
- INTRANET-53 [Absences] Format heures dans les exports [`ab8a023`](https://github.com/Dannebicque/intranetV3/commit/ab8a023ed8ad7edaff9031651a0026f412a7ecf8)
- gestion des catégories en BO [`728a2f0`](https://github.com/Dannebicque/intranetV3/commit/728a2f0496236d34cc94e33bf5784425bdf6f86b)
- Slug generator Article et Utilisateur [`3b4e827`](https://github.com/Dannebicque/intranetV3/commit/3b4e8274b06f1a1ca120aafdbacffac38fb9160f)
- Correcitfs + Gestion RP=&gt; Formation. [`5fe7bc3`](https://github.com/Dannebicque/intranetV3/commit/5fe7bc3ee3b4ed2db20839410fe7e9662d12a744)
- Optimisation des requêtes [`b50c46f`](https://github.com/Dannebicque/intranetV3/commit/b50c46f65b36d1cde166d001d7e8a4fee638d359)
- Fixture User [`20ae8be`](https://github.com/Dannebicque/intranetV3/commit/20ae8be8bfa1688f359b5230d15d5aeff16c460e)
- INTRANET-2 Intégrer double authentification CAS + Symfony [`6047a67`](https://github.com/Dannebicque/intranetV3/commit/6047a672c91b94f7f2e41bf86398b5f2d102f3d4)
- [Structure] : Possibilité de Marquer une UE en "bonification" [`4b07836`](https://github.com/Dannebicque/intranetV3/commit/4b0783681d952d0a9461fd9d4d510b6b5e6c7a52)
- Correctifs qualité. [`7728b1f`](https://github.com/Dannebicque/intranetV3/commit/7728b1f9c62079625654b9a4e9ba8393078b1713)
- Js/CSS + avatar [`e1655e9`](https://github.com/Dannebicque/intranetV3/commit/e1655e963eb1306c697cb326564e4f65bf51ad12)
- INTRANET-2 Intégrer double authentification CAS + Symfony [`280b2ec`](https://github.com/Dannebicque/intranetV3/commit/280b2ec14e6e35eaddb018360bc994601ec2e534)
- INTRANET-2 Intégrer double authentification CAS + Symfony [`c4eb94d`](https://github.com/Dannebicque/intranetV3/commit/c4eb94db47b115ddcf04465ed4d5aea360a187fe)
- Partie Stage, exports [`3b292d5`](https://github.com/Dannebicque/intranetV3/commit/3b292d57c50513683e81b722111bb44745563997)
- Correctifs ajout de HRS [`67fe6d8`](https://github.com/Dannebicque/intranetV3/commit/67fe6d81f9fc79c9eebe8b7acf96cec33f0bf66d)
- ajustement partie administration navigation par semestre [`6d5c2ab`](https://github.com/Dannebicque/intranetV3/commit/6d5c2abb392823a4c68e621238f19446a6227d21)
- Correctifs sur la partie stage [`bc919a7`](https://github.com/Dannebicque/intranetV3/commit/bc919a78963c4219de797baac118253d1e543d76)
- Département par défaut + changement de département temporaire [`dbfe70f`](https://github.com/Dannebicque/intranetV3/commit/dbfe70f5cba3ed242417b1b15a045fb67d3651c0)
- Partie emprunt de matériel [`c93c04e`](https://github.com/Dannebicque/intranetV3/commit/c93c04e3e68789f873542ce09ceebbbd82152941)
- INTRANET-2 Intégrer double authentification CAS + Symfony [`86c370f`](https://github.com/Dannebicque/intranetV3/commit/86c370fbbaaa380862b6b74f9acea93185cff2d7)
- INTRANET-29 Imports etudiants depuis Apogée [`ac682c8`](https://github.com/Dannebicque/intranetV3/commit/ac682c86a3b216aa15b6bbe7f01fc287188c9010)
- Création d'une classe abstraite pour les abscences et éviter la duplication de code [`7f2e1c5`](https://github.com/Dannebicque/intranetV3/commit/7f2e1c57134cf7bcd91b253b9001bf8fce7f5d43)
- INTRANET-2 Intégrer double authentification CAS + Symfony [`5a97c8c`](https://github.com/Dannebicque/intranetV3/commit/5a97c8c5beda8db6c64cd64fe9aaa3083539df3e)
- INTRANET-44 [ADMIN. GROUPES] : bouton héritage et synchro celcat [`ee09b65`](https://github.com/Dannebicque/intranetV3/commit/ee09b65451fbf8d30246f1cb93978e6670ede2b4)
- Sync recipes [`0abde15`](https://github.com/Dannebicque/intranetV3/commit/0abde157837e54163ee1bb14606f5523b36bc4a6)
- Fixtures (utilisateurs, formation, sites, bacs). [`9cf21fa`](https://github.com/Dannebicque/intranetV3/commit/9cf21fa103f5c661de6e79ae78037345c90d606d)
- Correctifs ajout de HRS [`2398c54`](https://github.com/Dannebicque/intranetV3/commit/2398c54f6fadde9a94b31c50438d9d156048d0ca)
- Stage edition courrier [`27f5a8b`](https://github.com/Dannebicque/intranetV3/commit/27f5a8bf4a0c9190fa5b624f17687fa47c111a55)
- INTRANET-46 [Configuration] : type de variable [`63e8f83`](https://github.com/Dannebicque/intranetV3/commit/63e8f836d65a7a1eba16553ee313adb85f9aa169)
- Gestion update d'un élément dans l'EDT [`a91fd88`](https://github.com/Dannebicque/intranetV3/commit/a91fd88145b1ada7a945eb9112d4432a74f2d7ca)
- responsive partie étudiant et edt [`3d05382`](https://github.com/Dannebicque/intranetV3/commit/3d0538294ef0302091482cd082d4e8e76d486ef2)
- Correctifs Justificatif Abence [`0129816`](https://github.com/Dannebicque/intranetV3/commit/0129816f8277916b4e6d2b2cff846fb95ffadc54)
- Correctifs sur la partie justificatifs d'absences [`eb0816b`](https://github.com/Dannebicque/intranetV3/commit/eb0816b1c65e60e52cd8fbf55c41cfb844e71101)
- Apogee. [`887cb09`](https://github.com/Dannebicque/intranetV3/commit/887cb0935f974f0a1d17b696ee7d45b1a3d7a1c1)
- Slug generator Article et Utilisateur [`e30604c`](https://github.com/Dannebicque/intranetV3/commit/e30604cb5b52fc7a8d83571e5dda5f819c6d5231)
- Optimisation JS [`199af1d`](https://github.com/Dannebicque/intranetV3/commit/199af1d52df4b2f275789106ca7bc7b20f131a3d)
- navigation edt étudiant [`163b508`](https://github.com/Dannebicque/intranetV3/commit/163b508c354ab246ccff470781011cf0c9a85515)
- Corrections URL structure en administration [`cd6411a`](https://github.com/Dannebicque/intranetV3/commit/cd6411a6978ce7e14b27dee92e5f0d90a5af1135)
- [Absence] : Nombre de jours maximum pour la saisie [`ce387c4`](https://github.com/Dannebicque/intranetV3/commit/ce387c44ad81518cf47fcce44117942629821f19)
- Exports depuis super-admin [`283bce9`](https://github.com/Dannebicque/intranetV3/commit/283bce9a581e1bd10800f1f862f3b185b85298a6)
- Correctifs + gestion des exports. [`b45aeac`](https://github.com/Dannebicque/intranetV3/commit/b45aeaccd5c880b7142cea3c00b00a6a18eb6149)
- correctif profil [`2dda2da`](https://github.com/Dannebicque/intranetV3/commit/2dda2dab6f6ed84c57f2e912b30839b537d73791)
- Remplacement des 'if' en filter pour la compatibilité Twig 3 [`8ae5188`](https://github.com/Dannebicque/intranetV3/commit/8ae51882af58c76817dd58db8f1a994bc254924a)
- Correctifs + qualité de code [`874c2c2`](https://github.com/Dannebicque/intranetV3/commit/874c2c278ee83b89ff5e73fea267ae4a54d1502c)
- INTRANET-3 configurer les logs de connexion sur 1an. [`552167c`](https://github.com/Dannebicque/intranetV3/commit/552167c3e987cd2c3966fbcc6d1b3db2992fded5)
- INTRANET-2 Intégrer double authentification CAS + Symfony [`ef02b3e`](https://github.com/Dannebicque/intranetV3/commit/ef02b3ef42c14f68683c0e5c1e817d7abce770f3)
- Bug accès + messages [`7be5236`](https://github.com/Dannebicque/intranetV3/commit/7be5236eafd73da34d9a2ef6cd3c09bde9f47091)
- Correctifs mineurs sur les bases [`8490798`](https://github.com/Dannebicque/intranetV3/commit/8490798f2c5729e53c6dfd50025977daf3c17596)
- Responsive dashboard + EDT [`fd282b2`](https://github.com/Dannebicque/intranetV3/commit/fd282b2ee1524c6e3d0e41fae523c4304d26943b)
- Bug Celcat [`40ee04c`](https://github.com/Dannebicque/intranetV3/commit/40ee04c3c0aa086207389462c3ffa2619efd6c7d)
- Bug suppression groupe dans le profil [`28eec6e`](https://github.com/Dannebicque/intranetV3/commit/28eec6ea09fcbb9cfe60df4c03c969205e889428)
- INTRANET-3 configurer les logs de connexion sur 1an. [`df06de7`](https://github.com/Dannebicque/intranetV3/commit/df06de7c64bf7cd2f065efcbfad14a40c61f7455)
- Corrections URL structure en administration [`f721dd6`](https://github.com/Dannebicque/intranetV3/commit/f721dd640933c6d742d828dbb251df43c7d0e488)
- LDAP/Groupes [`8ef1b8e`](https://github.com/Dannebicque/intranetV3/commit/8ef1b8e0849f7916a678a52bab8c0daa442ee0fa)
- Apogee. [`26af127`](https://github.com/Dannebicque/intranetV3/commit/26af12709c89d0636754a2248a3ca75de583497a)
- INTRANET-55 [Emprunts] :gestion matériel et emprunts [`d204527`](https://github.com/Dannebicque/intranetV3/commit/d2045270dbcff4036021e6d86ae55cea104bd79c)
- CAS + Double authentification [`2d4eac4`](https://github.com/Dannebicque/intranetV3/commit/2d4eac4172aeacca0fb0e432d59f01798e08af83)
- Correctifs EDT admin [`c17bf35`](https://github.com/Dannebicque/intranetV3/commit/c17bf353065bf0f884605369cec19513b09017d7)
- Commande console pour supprimer les notifications en tâche CRON [`cbe65ed`](https://github.com/Dannebicque/intranetV3/commit/cbe65ed4159ff8bdf962fc4629866cfbe0c7e68c)
- Lien documentation + logo [`b8c8360`](https://github.com/Dannebicque/intranetV3/commit/b8c83602c65d72f004a1f6ffde46b83f13647581)
- INTRANET-29 Imports etudiants depuis Apogée [`96951d6`](https://github.com/Dannebicque/intranetV3/commit/96951d6218114f881fb60c48a071bfd9a20b32fe)
- INTRANET-46 [Configuration] : type de variable [`7948484`](https://github.com/Dannebicque/intranetV3/commit/7948484fe7dd4336aa3142bdadc663a815d66f77)
- Correctifs qualité. [`0a438d2`](https://github.com/Dannebicque/intranetV3/commit/0a438d21a3c775a139b686666563bac0bcb653b0)
- Modification style EDT et tableau, cohérence avec le thème [`8ea4519`](https://github.com/Dannebicque/intranetV3/commit/8ea4519389ce59e5df35aff0e48c4291be8184e9)
- Bug import liste étudiants [`fede520`](https://github.com/Dannebicque/intranetV3/commit/fede520b3322746605affb82fa471c969edfcb33)
- Import d'un prévisonnel CSV depuis Intranet V2 [`8220360`](https://github.com/Dannebicque/intranetV3/commit/82203602f0c8e74f51a7baf35f5a175fb5c08212)
- Préparation structure sous-comm [`6fd9ba1`](https://github.com/Dannebicque/intranetV3/commit/6fd9ba1b7bac2f2b9e33772c0a4c85f9e0e78bb0)
- CAS + Double authentification [`67925d5`](https://github.com/Dannebicque/intranetV3/commit/67925d544d434d62486902ba7e8b01cefc16a1dd)
- INTRANET-43 [SA-RH] Bug datatable. [`8d9ed30`](https://github.com/Dannebicque/intranetV3/commit/8d9ed30ad215276ef38983a181312a47b3244b9e)
- Partie qualité [`9ef4a1b`](https://github.com/Dannebicque/intranetV3/commit/9ef4a1ba8c8510c0f361d90fe94944244ed25175)
- Correctifs sur la gestion des groupes/typesgroupes en admin. [`6650a02`](https://github.com/Dannebicque/intranetV3/commit/6650a025bf615ffab3b3133faa849a6a6b07ac5a)
- Security avec Guard + Année Universitaire [`bbc36ca`](https://github.com/Dannebicque/intranetV3/commit/bbc36ca50a136fd38e41324bc4014421b4f3c282)
- Ajout possibilité de sauvegarder et revenir à l'index  en admin [`025640c`](https://github.com/Dannebicque/intranetV3/commit/025640c1d629ea4f895875536ff0730e359f8072)
- Bug lien profil [`3036154`](https://github.com/Dannebicque/intranetV3/commit/3036154c3ff8c86b62c94d4337515dbebd33ed3a)
- INTRANET-2 Intégrer double authentification CAS + Symfony [`50685bf`](https://github.com/Dannebicque/intranetV3/commit/50685bf2d7d76cfffc2a34d733e93064ac68d24a)
- Security avec Guard + Année Universitaire [`d04d004`](https://github.com/Dannebicque/intranetV3/commit/d04d004b9f8dcf3cfd81e66655ed79ba34f350d3)
- Correctifs qualité. [`17d682b`](https://github.com/Dannebicque/intranetV3/commit/17d682b727a0eff030dc923eeba0d8f7ae3919d6)
- Bug Parcours [`5ee8777`](https://github.com/Dannebicque/intranetV3/commit/5ee877774ba197f353976169939ac93b970a9f8b)
- Correctifs qualité. [`1340015`](https://github.com/Dannebicque/intranetV3/commit/1340015f40a57af79167e979aee001ed2fe258ee)
- [SUPERADMIN] - Partie Super Administration [`764d29b`](https://github.com/Dannebicque/intranetV3/commit/764d29b1379dcd6dd5f7c5efce13251bc3186fa0)
- Correction du bug de connexion la première fois [`0dea4af`](https://github.com/Dannebicque/intranetV3/commit/0dea4af64b85efcbc91205be04fba5ca271f9e44)
- Correctif bug avatar recherche [`5a170ee`](https://github.com/Dannebicque/intranetV3/commit/5a170eea9116127eb2fdb5f0c0c78fe7b8540654)
- Correctifs gestion des documents [`c96ed71`](https://github.com/Dannebicque/intranetV3/commit/c96ed71731e0e2d9e8ec5bf0d9604019ea68c77c)
- Modification page retour lors d'un ajout de type de groupe [`d8fa42e`](https://github.com/Dannebicque/intranetV3/commit/d8fa42e5c971abe04c5528353f25daf6193753b6)
- LDAP [`deface3`](https://github.com/Dannebicque/intranetV3/commit/deface3d59949e0730a8b095468a9682e367c935)
- INTRANET-29 Imports etudiants depuis Apogée [`0faf627`](https://github.com/Dannebicque/intranetV3/commit/0faf627809189899eaeb6ed72feebda78743003a)
- INTRANET-29 Imports etudiants depuis Apogée [`dd35169`](https://github.com/Dannebicque/intranetV3/commit/dd35169708bf900c77adf30bc720214d57302763)
- Correctifs sur copie d'une UE/Semestre/Diplome/Annee/Parcour [`7f9def4`](https://github.com/Dannebicque/intranetV3/commit/7f9def4642625f71e5d38b36b07f1b2c240f3a7d)
- Nombre d'étudiants par diplôme [`7a0bd06`](https://github.com/Dannebicque/intranetV3/commit/7a0bd06e6c03c295c3f7fa621bbb547ffc8e0337)
- Apogee. [`8bc2206`](https://github.com/Dannebicque/intranetV3/commit/8bc2206c49aab93a72c140748e374f54ba770add)
- Correctifs qualité. [`2dfa9bb`](https://github.com/Dannebicque/intranetV3/commit/2dfa9bb2d2686d3b020055e37278ffec815ee7d2)
- Correctif EDT [`7b4b6df`](https://github.com/Dannebicque/intranetV3/commit/7b4b6df6ef00fbba29d27a7bd5e5826f31b8e63b)
- Correctifs qualité. [`5a9be73`](https://github.com/Dannebicque/intranetV3/commit/5a9be735fe04d6e24654a716c13c2e70af34c00c)
- Export des groupes [`c269d44`](https://github.com/Dannebicque/intranetV3/commit/c269d4431a985dfc7bc542f2ed0a5a78eafe364d)
- Import d'un prévisonnel CSV depuis Intranet V2 [`f43d3f2`](https://github.com/Dannebicque/intranetV3/commit/f43d3f25eacb750756b686d9bf24679ab5e6d389)
-  php7.2 [`00c1a21`](https://github.com/Dannebicque/intranetV3/commit/00c1a21b3c2dc81c7a16b37c7ecb6c7c4feb9b37)
- Edt + Edit (enregistrer et index) [`22681ce`](https://github.com/Dannebicque/intranetV3/commit/22681ce754ec2c75b7eacdebe10058b2e873c49c)
-  php7.2 [`dde6905`](https://github.com/Dannebicque/intranetV3/commit/dde69050e60a6d79ea641ef2458f35af6669633d)
- Correctifs sur la partie justificatifs d'absences [`7d081a1`](https://github.com/Dannebicque/intranetV3/commit/7d081a19b5e38167523e5b3f9a7d4f5415e7bf2e)
- Mise en place docker et bonne structure (MPIOT) [`67f6bcc`](https://github.com/Dannebicque/intranetV3/commit/67f6bcc20191329dced07c89019261bf434bcde0)
- Fixtures (utilisateurs, formation, sites, bacs). [`4578412`](https://github.com/Dannebicque/intranetV3/commit/4578412c5b6e67ab601849a887978cf5c1a7b465)
- [Structure] : Possibilité de Marquer une UE en "bonification" [`f668932`](https://github.com/Dannebicque/intranetV3/commit/f6689329d1bacb2fdc4e9864fe5401509a019688)
- Correctifs + Accessibilité [`af004cf`](https://github.com/Dannebicque/intranetV3/commit/af004cf1a7075da65e80f8cb61633bbc6b9287dd)
- Correction bug CSS pages spéciales [`f7a4fe0`](https://github.com/Dannebicque/intranetV3/commit/f7a4fe0ad660661d642491da3f34e43e0b51c0d7)
- Apogee [`2db9b65`](https://github.com/Dannebicque/intranetV3/commit/2db9b65489e65efb77655436309352ee86fbbe98)
- INTRANET-27 Saisie du prévisionnel [`cfc6f5c`](https://github.com/Dannebicque/intranetV3/commit/cfc6f5c03b95b88cf72c9ae48f6d6231978b9fbc)
- Fixture User [`4a4695d`](https://github.com/Dannebicque/intranetV3/commit/4a4695d2a771ff86b2b3d9e8c0e9948f00f24167)
- Correctifs gestion des documents [`39f6636`](https://github.com/Dannebicque/intranetV3/commit/39f6636274e3118fc60b2f00cb7f43f7fa9ce336)
- Divers correctifs [`9472cb5`](https://github.com/Dannebicque/intranetV3/commit/9472cb5309a814a357f10d3498c4f40339e3d710)
- Correctifs connexion [`97c6f53`](https://github.com/Dannebicque/intranetV3/commit/97c6f53f7db1f1ff3c10a82d0a8b14132e2ff41d)
- INTRANET-1 Intégrer Apogée [`b08c5e9`](https://github.com/Dannebicque/intranetV3/commit/b08c5e970fdf8e2de3aa40084f7cf9978e03a4d5)
- [Structure] : Possibilité de Marquer une UE en "bonification" [`2654586`](https://github.com/Dannebicque/intranetV3/commit/2654586ed1a0e7642db06cbe35a9b5e72c3181c0)
- gestion des langues + drapeaux [`a0802d4`](https://github.com/Dannebicque/intranetV3/commit/a0802d471f2bf9ac0147bb843b3dd4e185b91e67)
- Correctif enquête [`6a22d58`](https://github.com/Dannebicque/intranetV3/commit/6a22d587111ffdbdacd9f5f0d06fc1eb0a8745ce)
- Correctif enquête [`87fbefd`](https://github.com/Dannebicque/intranetV3/commit/87fbefdf888731fb44fcb7d3667560ed70e0c718)
- INTRANET-10 Finaliser les traductions [`297383e`](https://github.com/Dannebicque/intranetV3/commit/297383e317247300e1a156ca9e5e860110fd8f08)
- Traitement des todos [`7ad678f`](https://github.com/Dannebicque/intranetV3/commit/7ad678f60621388c7b6eb35b3bfbba3052fbd2bf)
- INTRANET-1 Intégrer Apogée [`9e27374`](https://github.com/Dannebicque/intranetV3/commit/9e27374948fa63003887ab42b13b9705dbda5372)
- Correctifs gestion des documents [`6d289ce`](https://github.com/Dannebicque/intranetV3/commit/6d289ce7ce0201707648de914dc3d1d7e8ad9906)
- INTRANET-55 [Emprunts] :gestion matériel et emprints [`192de23`](https://github.com/Dannebicque/intranetV3/commit/192de238fa397634c3e032313cde0c2a77671806)
- INTRANET-55 [Emprunts] :gestion matériel et emprints [`af3e6aa`](https://github.com/Dannebicque/intranetV3/commit/af3e6aaf60c897ebb106269843ed33b738dc0fcf)
- Partie offre de stage en étudiant. [`2b13020`](https://github.com/Dannebicque/intranetV3/commit/2b13020411452f7d8f47056ba49c7539ecd253fb)
- INTRANET-27 Saisie du prévisionnel [`4fdd63c`](https://github.com/Dannebicque/intranetV3/commit/4fdd63cb15380d14c528a4f3b690b202e56e19d5)
- LDAP/Groupes [`b1036ef`](https://github.com/Dannebicque/intranetV3/commit/b1036efdd57c12bac37a97c007d1225b6828a021)
- Partie EDT semestre [`61fe0ab`](https://github.com/Dannebicque/intranetV3/commit/61fe0ab3b26b97ed929e5171a6bcec2bf55af3b6)
- Modification gestion qualite. [`ff339ad`](https://github.com/Dannebicque/intranetV3/commit/ff339ad115aa29948f2349c3f99c521915e441f3)
- Partie qualité [`b27efeb`](https://github.com/Dannebicque/intranetV3/commit/b27efeb8d8bbc52cf4cd0b31e6714a4528be4b6b)
- CAS + Double authentification [`02a67bc`](https://github.com/Dannebicque/intranetV3/commit/02a67bc9d1f6aa37e8a08fb36f88889c6bffa2c8)
- INTRANET-1 Intégrer Apogée [`f1b68b0`](https://github.com/Dannebicque/intranetV3/commit/f1b68b0d4eb7a057bc4e30096233e9cb835b3c03)
- Optimisation qualité de code [`7ce2b07`](https://github.com/Dannebicque/intranetV3/commit/7ce2b0721c79381e3746b899cacc16e133bd34e5)
- export ics [`ca727d8`](https://github.com/Dannebicque/intranetV3/commit/ca727d8fbe0e477c5289da898542164e560bb935)
- Import étudiants en super Admin [`0adaf14`](https://github.com/Dannebicque/intranetV3/commit/0adaf14f4a0a7650454a937dffed8ba960a087b8)
- INTRANET-43 [SA-RH] Bug datatable [`ed3fa69`](https://github.com/Dannebicque/intranetV3/commit/ed3fa69c411ef7acf05ada6ba4ee29cc97296d9e)
- INTRANET-29 Imports etudiants depuis Apogée [`f2f3e16`](https://github.com/Dannebicque/intranetV3/commit/f2f3e162a23ca1633959a3c3dbf4eeeaf7487e97)
- Import Celcat Calendrier [`55906d9`](https://github.com/Dannebicque/intranetV3/commit/55906d963f56b2ec6d17477073169d5b5dcd2376)
- Code Quality [`c642337`](https://github.com/Dannebicque/intranetV3/commit/c642337d6bdb77fbefee9cac90d16b9a546ea46c)
- Import d'un prévisionnel, même si l'enseignant n'existe pas [`4893554`](https://github.com/Dannebicque/intranetV3/commit/4893554ed8c659fe6d1da18a3c73f220adf5b8d5)
- INTRANET-1 Intégrer Apogée [`2612203`](https://github.com/Dannebicque/intranetV3/commit/261220368eadcc6eea20936f9240f66182fb925b)
- INTRANET-55 [Emprunts] :gestion matériel et emprints [`cfb68f7`](https://github.com/Dannebicque/intranetV3/commit/cfb68f780280de7aae210f65a9deb242e9578bd1)
- Correctifs + Accessibilité [`4184aa2`](https://github.com/Dannebicque/intranetV3/commit/4184aa2147447e9e9bc03fb4427a9734d6c09161)
- Import d'un prévisionnel, même si l'enseignant n'existe pas [`d1d1915`](https://github.com/Dannebicque/intranetV3/commit/d1d1915f99e1a762a7f93be34604cb7ab2902508)
- Partie qualité [`a38775e`](https://github.com/Dannebicque/intranetV3/commit/a38775e0a6a003869ee18e84daee1a7f4331150f)
- celcat [`45c2370`](https://github.com/Dannebicque/intranetV3/commit/45c2370daa9397da75c51fda09915c923cfc3fce)
- INTRANET-29 Imports etudiants depuis Apogée [`727bb7a`](https://github.com/Dannebicque/intranetV3/commit/727bb7a7cd2c26cb350308da3e57f1f86f149374)
- Correctifs "aide" [`af9b914`](https://github.com/Dannebicque/intranetV3/commit/af9b91402fde84a130f8b4346225778e428202dc)
- Bug import liste étudiants [`3527209`](https://github.com/Dannebicque/intranetV3/commit/3527209720f0a145530ea17db073c29abe8c77f4)
- Correctifs + Accessibilité [`313a02d`](https://github.com/Dannebicque/intranetV3/commit/313a02de37a9b9950dbce5acc777b53e7caed7ba)
- LDAP/Groupes [`9cd61ed`](https://github.com/Dannebicque/intranetV3/commit/9cd61edeaa62bf16c6c32483ab6f85010c420070)
- INTRANET-2 Intégrer double authentification CAS + Symfony [`b0c201b`](https://github.com/Dannebicque/intranetV3/commit/b0c201ba8c096d942ec5e132e5c96eac054cf1ec)
- Code Quality [`80d7c74`](https://github.com/Dannebicque/intranetV3/commit/80d7c744bc91b01022a9831dd4c1c96b36ba6d2d)
- Bug Celcat [`d024d81`](https://github.com/Dannebicque/intranetV3/commit/d024d81992aef888b667c9ddedae2bd74d3abcd4)
- Bug accès + messages [`b5c6cf6`](https://github.com/Dannebicque/intranetV3/commit/b5c6cf696a5f5f5b3ac56d9682998df2c09fe068)
- Config [`d01adf9`](https://github.com/Dannebicque/intranetV3/commit/d01adf9705332f46a5f2c7110726fde9cc54b451)
- celcat: récupération EDT sur demande. [`18c3098`](https://github.com/Dannebicque/intranetV3/commit/18c3098c1a6918cd6f49155def8c787cae2099b2)
- INTRANET-2 Intégrer double authentification CAS + Symfony [`a0c3043`](https://github.com/Dannebicque/intranetV3/commit/a0c3043cd3026257adb284a54d41893c14c5434c)
- Correctif enquête [`014895a`](https://github.com/Dannebicque/intranetV3/commit/014895adc3bbbe4141aa162f7704d562da0facf6)
- Bug accès saisie des absences [`a1b2b8c`](https://github.com/Dannebicque/intranetV3/commit/a1b2b8c06525c951132192e800a56e47c9ccaaf7)
- correctifs avatar générique [`917dd54`](https://github.com/Dannebicque/intranetV3/commit/917dd54330bccc85f4b9f153b81d5c5f8dc855fb)
- Modification gestion qualite. [`9b90e2f`](https://github.com/Dannebicque/intranetV3/commit/9b90e2fdda3337ffb807dc3b898ca412571ace90)
- LDAP [`690e1df`](https://github.com/Dannebicque/intranetV3/commit/690e1df5e6e82b9c17480daa264a1acf0ad44ea9)
- [SUPERADMIN] - Partie Super Administration [`d5271ba`](https://github.com/Dannebicque/intranetV3/commit/d5271ba6870381b991669cc250c9d4a83e78b697)
- celcat [`181c45f`](https://github.com/Dannebicque/intranetV3/commit/181c45f7d6307c6d97a6644effa197efede56167)
- INTRANET-2 Intégrer double authentification CAS + Symfony [`d944fe3`](https://github.com/Dannebicque/intranetV3/commit/d944fe35f8f4198c0e71e1a6a9777353ae3f3887)
- Fixture User [`5bc9fad`](https://github.com/Dannebicque/intranetV3/commit/5bc9fad930bb5fae95fe6944f6a3ae9445500d12)
- Correctif enquête [`652260c`](https://github.com/Dannebicque/intranetV3/commit/652260c3d7f32710c37688807ff453bdddaf25f0)
- Bug Celcat [`85fe32b`](https://github.com/Dannebicque/intranetV3/commit/85fe32b3b5e85ed9231127ca3d8228a986d0d514)
- LDAP/Groupes [`5c54d18`](https://github.com/Dannebicque/intranetV3/commit/5c54d18a6867dcae241cfe78cd5f8d15328b0c38)
- INTRANET-55 [Emprunts] :gestion matériel et emprints [`487b109`](https://github.com/Dannebicque/intranetV3/commit/487b10939153702ff8cc801d692b904d8637c7d0)
- INTRANET-2 Intégrer double authentification CAS + Symfony [`d21d525`](https://github.com/Dannebicque/intranetV3/commit/d21d5258daf3cddcd71a59517ea0de7f7298c1ff)
- INTRANET-2 Intégrer double authentification CAS + Symfony [`1f9754d`](https://github.com/Dannebicque/intranetV3/commit/1f9754d43575a1dc693a1b7e1c8c164c6765831d)
- INTRANET-2 Intégrer double authentification CAS + Symfony [`e189d85`](https://github.com/Dannebicque/intranetV3/commit/e189d85d406ce7be912187b4264834c73e6ca9b5)
- Apogee [`0418431`](https://github.com/Dannebicque/intranetV3/commit/041843115c821069cb5aded86d325c89a6231db9)
- INTRANET-27 Saisie du prévisionnel [`f84f12a`](https://github.com/Dannebicque/intranetV3/commit/f84f12a7480d125b8a60a509259b2903c94b2cc8)
- Tests apogee [`45c8c33`](https://github.com/Dannebicque/intranetV3/commit/45c8c337c96ee590189a3790f04c9392a2430fce)
- Config [`f714438`](https://github.com/Dannebicque/intranetV3/commit/f7144382d95b176319aa341c9369cc3eba2e3cb2)
- [QUALITE] : Mise en place des questionnaires étudiants [`0f02e72`](https://github.com/Dannebicque/intranetV3/commit/0f02e7241f8810391adf263892dcd1652f481d96)
- INTRANET-55 [Emprunts] :gestion matériel et emprints [`9186184`](https://github.com/Dannebicque/intranetV3/commit/9186184499b81cc08a1d9e2c5cff751c7906f2de)
- INTRANET-2 Intégrer double authentification CAS + Symfony [`4eef13a`](https://github.com/Dannebicque/intranetV3/commit/4eef13a5d8b543f133e7ed29b331c5f79b8a2c97)
- INTRANET-2 Intégrer double authentification CAS + Symfony [`22b0e70`](https://github.com/Dannebicque/intranetV3/commit/22b0e70a56ade003d0aad3a7cf0ebeae8a635cd7)
- INTRANET-29 Imports etudiants depuis Apogée [`6d7785c`](https://github.com/Dannebicque/intranetV3/commit/6d7785cbb51d5edb75a57a4c0d8fae171737304b)
- Fixture User [`55a32a0`](https://github.com/Dannebicque/intranetV3/commit/55a32a0a2e4e09d4e73f6f89578eb8e6b4494028)
- Correctifs gestion des documents [`6543282`](https://github.com/Dannebicque/intranetV3/commit/6543282726ce1fb02b47ed88b392b7fa808a1527)
- Correctifs EDT [`b996dea`](https://github.com/Dannebicque/intranetV3/commit/b996dea3eb669b83a3ff2f948a4f354344af029b)
- Correctifs + Accessibilité [`63c5999`](https://github.com/Dannebicque/intranetV3/commit/63c59994b8ed3fe616d2861ffe2e56788e791849)
- Correctifs + Accessibilité [`42d55dd`](https://github.com/Dannebicque/intranetV3/commit/42d55ddaa70133274ce47a1cb0771c9d89b5eeca)
- Quelques bugs [`a0e0785`](https://github.com/Dannebicque/intranetV3/commit/a0e07855004e3bd04d0a1dda554dfe33c4567f92)
- Partie qualité [`4b7c7af`](https://github.com/Dannebicque/intranetV3/commit/4b7c7af84d3f0de4fa2aadc76d03e2fd96c3d0d3)
- Partie offre de stage en étudiant. [`78f2da8`](https://github.com/Dannebicque/intranetV3/commit/78f2da8f4466970d275d699b6741045c8592ff61)
- INTRANET-2 Intégrer double authentification CAS + Symfony [`6f6313d`](https://github.com/Dannebicque/intranetV3/commit/6f6313d138834fc930d5a55b9ab5a0e5ceab21fd)
- INTRANET-2 Intégrer double authentification CAS + Symfony [`6b8353a`](https://github.com/Dannebicque/intranetV3/commit/6b8353af5f3e1a31abd632e1e291b5e0c3250d3e)
- INTRANET-2 Intégrer double authentification CAS + Symfony [`041b38e`](https://github.com/Dannebicque/intranetV3/commit/041b38e21e8a8c37cc87331379cf32076837b6a3)
- INTRANET-2 Intégrer double authentification CAS + Symfony [`4c33e5d`](https://github.com/Dannebicque/intranetV3/commit/4c33e5dbdf50b3426c7b8e13ad16b09d587e0ff5)
- INTRANET-2 Intégrer double authentification CAS + Symfony [`8e31bbe`](https://github.com/Dannebicque/intranetV3/commit/8e31bbe16774ba644cee81dd342b481bc9e4ad3b)
- INTRANET-2 Intégrer double authentification CAS + Symfony [`4c36779`](https://github.com/Dannebicque/intranetV3/commit/4c36779ad8661abb8f538da7d459c28e2fbd2104)
- INTRANET-2 Intégrer double authentification CAS + Symfony [`78a3b7e`](https://github.com/Dannebicque/intranetV3/commit/78a3b7e28de2ad66c3aa72d572329c36163f6e98)
- INTRANET-29 Imports etudiants depuis Apogée [`24f324f`](https://github.com/Dannebicque/intranetV3/commit/24f324f508498cf301f2faa071b5fc730ae63760)
- Apogee [`2cead67`](https://github.com/Dannebicque/intranetV3/commit/2cead67962a8dd05f4d4d09fb73ec176bb948855)
- Tests apogee [`83482d4`](https://github.com/Dannebicque/intranetV3/commit/83482d4a47f26904fa88288dc41050ff162fca3d)
- CAS + Double authentification [`2a2f580`](https://github.com/Dannebicque/intranetV3/commit/2a2f580f910e90b5e756551ac670ab2f249877e6)
- CAS + Double authentification [`1fa73f2`](https://github.com/Dannebicque/intranetV3/commit/1fa73f2355b0c1f91521d3d0b8a3cfab6669695b)
- Correctifs qualité. [`9adc329`](https://github.com/Dannebicque/intranetV3/commit/9adc329e61b5ad069fc49194496e95f39dc8a472)
- INTRANET-55 [Emprunts] :gestion matériel et emprints [`8eec874`](https://github.com/Dannebicque/intranetV3/commit/8eec874db54d6ba79ada8576b0e8c983a299f891)
- INTRANET-55 [Emprunts] :gestion matériel et emprints [`4bfcd82`](https://github.com/Dannebicque/intranetV3/commit/4bfcd8280153d6598f74e47d0580288f2a544127)
- INTRANET-2 Intégrer double authentification CAS + Symfony [`3ffa671`](https://github.com/Dannebicque/intranetV3/commit/3ffa6714a46c9b33554409c3f081f9231f582081)
- INTRANET-2 Intégrer double authentification CAS + Symfony [`5074d29`](https://github.com/Dannebicque/intranetV3/commit/5074d29248725873d343062f088bc93ef857a2fa)
- INTRANET-2 Intégrer double authentification CAS + Symfony [`23d8c4b`](https://github.com/Dannebicque/intranetV3/commit/23d8c4b07e176c35e9214d9cbfe823f853c53b28)
- INTRANET-2 Intégrer double authentification CAS + Symfony [`851476e`](https://github.com/Dannebicque/intranetV3/commit/851476e5901d555854c26fda646e69f38b9f1ce0)
- INTRANET-2 Intégrer double authentification CAS + Symfony [`4deb817`](https://github.com/Dannebicque/intranetV3/commit/4deb81739ab9efd7f0d7054ef1bd83455136d235)
- INTRANET-29 Imports etudiants depuis Apogée [`8ab471d`](https://github.com/Dannebicque/intranetV3/commit/8ab471d05c86473d509b86c2711906baacaa7ea7)
- INTRANET-27 Saisie du prévisionnel [`b1ce73f`](https://github.com/Dannebicque/intranetV3/commit/b1ce73fa6e1c3549e94c3e63c8c3a79a037e38dd)
- Correctifs connexion [`46d9d1b`](https://github.com/Dannebicque/intranetV3/commit/46d9d1ba1dd815df5458bf827740a580d1dccccb)
- Import étudiants en super Admin [`b5cfe9d`](https://github.com/Dannebicque/intranetV3/commit/b5cfe9d9da1b38daa7cb8c1172a54e262de11e4d)
- Divers correctifs [`dc54d7c`](https://github.com/Dannebicque/intranetV3/commit/dc54d7c6d04a29c3bb176b4860b82113f876247d)
- Edt + Edit (enregistrer et index) [`afefe28`](https://github.com/Dannebicque/intranetV3/commit/afefe28bc92c4b6d11909747dddb0e12f21e702b)
- LDAP/Groupes [`6fd16e5`](https://github.com/Dannebicque/intranetV3/commit/6fd16e51602c0a54dd2cdf37d3eec0685bad8aa9)
- celcat [`d437000`](https://github.com/Dannebicque/intranetV3/commit/d4370000aefca7aa0767cc788ef0d9c185c7aad0)
- INTRANET-55 [Emprunts] :gestion matériel et emprints [`961a1f8`](https://github.com/Dannebicque/intranetV3/commit/961a1f86b81dc49fc27b71dca91b128c8b8485bc)
- INTRANET-55 [Emprunts] :gestion matériel et emprints [`fca6a46`](https://github.com/Dannebicque/intranetV3/commit/fca6a462432f238998c6165dbc32f017dfeae287)
- INTRANET-55 [Emprunts] :gestion matériel et emprints [`164016e`](https://github.com/Dannebicque/intranetV3/commit/164016ee5e67f5044a372c563b8991257627a7f6)
- INTRANET-2 Intégrer double authentification CAS + Symfony [`b35dd3f`](https://github.com/Dannebicque/intranetV3/commit/b35dd3f566a97011ee7e0a1a6cab82cb2a5fffdf)
- INTRANET-2 Intégrer double authentification CAS + Symfony [`46fa1cf`](https://github.com/Dannebicque/intranetV3/commit/46fa1cf02b86f9906d025d65d405ed4746c2e15d)
- INTRANET-2 Intégrer double authentification CAS + Symfony [`fb3926b`](https://github.com/Dannebicque/intranetV3/commit/fb3926b42b5f773d09629fa98928307af964a845)
- INTRANET-2 Intégrer double authentification CAS + Symfony [`eb83621`](https://github.com/Dannebicque/intranetV3/commit/eb83621bcf70f7b6edd573f22febd855e619f4ab)
- INTRANET-2 Intégrer double authentification CAS + Symfony [`c7e3bd5`](https://github.com/Dannebicque/intranetV3/commit/c7e3bd59500c0e98c963be6f56ca9e9418097e46)
- INTRANET-2 Intégrer double authentification CAS + Symfony [`84cca7a`](https://github.com/Dannebicque/intranetV3/commit/84cca7ab9d1e48a4cd8e56afc1769a6bdd3582a4)
- INTRANET-29 Imports etudiants depuis Apogée [`2e6d33f`](https://github.com/Dannebicque/intranetV3/commit/2e6d33f9db36fe955a00bdc77d8d7504a78547ae)
- INTRANET-29 Imports etudiants depuis Apogée [`0c36750`](https://github.com/Dannebicque/intranetV3/commit/0c367508c0d9059b0e43f621b697bad513c1e320)
- Apogee [`7f60225`](https://github.com/Dannebicque/intranetV3/commit/7f60225702a3410f91b304e62c719d91b81668ce)
- Apogee [`3adbad5`](https://github.com/Dannebicque/intranetV3/commit/3adbad5c4f98422320f15c4287bb8d07ae595384)
- Traitement des todos [`f50af51`](https://github.com/Dannebicque/intranetV3/commit/f50af51af73e0cf9adb056877f132ffd4fc8fe5f)
- Security avec CAS [`aeb8882`](https://github.com/Dannebicque/intranetV3/commit/aeb88822d4a8a65e08b6274907173411457cbb1a)
- CAS + Double authentification [`2212aef`](https://github.com/Dannebicque/intranetV3/commit/2212aeff005a94824bc84dcdcfca4869c6678af8)
- remove start [`510ee94`](https://github.com/Dannebicque/intranetV3/commit/510ee94f340fcc3367a919b5b8918344cd514038)
- Correctifs qualité. [`36be72c`](https://github.com/Dannebicque/intranetV3/commit/36be72c5424f649648314d5cbb3cfc41d37ea12b)
- Correctifs qualité. [`74f81a0`](https://github.com/Dannebicque/intranetV3/commit/74f81a07c88f57016fd9626006d17e479e0fe760)
- [SUPERADMIN] - Partie Super Administration [`fd566b1`](https://github.com/Dannebicque/intranetV3/commit/fd566b1d56cae8d8290180518ad66c5ba48f3f11)
- INTRANET-55 [Emprunts] :gestion matériel et emprints [`08cffb3`](https://github.com/Dannebicque/intranetV3/commit/08cffb3c05dbb3d77d3a4406bdd54dc63f85fc93)
- INTRANET-27 Saisie du prévisionnel [`1697cf6`](https://github.com/Dannebicque/intranetV3/commit/1697cf68d93b3e89c501386206210565f81367f4)
- Partie planning de rattrapage en admin [`4a73744`](https://github.com/Dannebicque/intranetV3/commit/4a737444a5875be658d939288d9011a63ecdcfc3)
- Add specific php requirements [`826c26c`](https://github.com/Dannebicque/intranetV3/commit/826c26cc5dc05de41516bd9652f2b07847b0e000)
- Correctif enquête [`f530c76`](https://github.com/Dannebicque/intranetV3/commit/f530c761134bba6cbc5cb62b506d5fcbf3558b62)
- Bug affichage EDT depuis Celcat [`3f7440f`](https://github.com/Dannebicque/intranetV3/commit/3f7440f911011f341fc78194c05738930be00d79)
- Suppression références GEDMO [`353e4e7`](https://github.com/Dannebicque/intranetV3/commit/353e4e7e09861d1e89a383d8696c55cacdcd657a)
- Import d'un prévisionnel, même si l'enseignant n'existe pas [`6cfa629`](https://github.com/Dannebicque/intranetV3/commit/6cfa62988979dcc7fb6ff577885d9f21a6aede85)
- Correctifs partie absence [`a66d0f3`](https://github.com/Dannebicque/intranetV3/commit/a66d0f34d909c62abd348c361f1929274af0fd39)
- Partie qualité [`cda6321`](https://github.com/Dannebicque/intranetV3/commit/cda63211e8c3769dbc0417d9dfa75a656c79c6ba)
- CAS + Double authentification [`a732bb3`](https://github.com/Dannebicque/intranetV3/commit/a732bb363ae29f738eb6772022f870fa270ced31)
- Apogee. [`b1315ed`](https://github.com/Dannebicque/intranetV3/commit/b1315eddefa710ceb21e4f3358a4be403b00ac40)
- Correctifs qualité. [`bb97328`](https://github.com/Dannebicque/intranetV3/commit/bb9732800a34aa595c9779e177a44de727840d37)
- INTRANET-1 Intégrer Apogée [`927fd2f`](https://github.com/Dannebicque/intranetV3/commit/927fd2ff0029cac7fb689c7f9c83dcad0122e33e)
- INTRANET-2 Intégrer double authentification CAS + Symfony [`dd94d63`](https://github.com/Dannebicque/intranetV3/commit/dd94d637f0f3b72a85b33a9c24d1df05f9cf7314)
- Correctif enquête [`516b072`](https://github.com/Dannebicque/intranetV3/commit/516b07260523046fd45e3d76757fd368f8f43ad2)
- Bugs et optimisations pour la partie questionnaire-qualité [`7e99f0f`](https://github.com/Dannebicque/intranetV3/commit/7e99f0f9853c350308e9f15826c5d7aeee31fe3f)
- Bug Parcours [`cce7042`](https://github.com/Dannebicque/intranetV3/commit/cce70424b9bf493b3001f16b1820e09a16b7e47d)
- CAS + Double authentification [`368d566`](https://github.com/Dannebicque/intranetV3/commit/368d566ade5a8af389089590aae5105c71365eab)
- Edt + Edit (enregistrer et index) [`1bb9846`](https://github.com/Dannebicque/intranetV3/commit/1bb98469477cab34ad709af3017ce83652ff62fa)
- LDAP/Groupes [`2faa677`](https://github.com/Dannebicque/intranetV3/commit/2faa677e506f6e4fbb4ad50c505b00959a63507c)
- Apogee. [`49c8e22`](https://github.com/Dannebicque/intranetV3/commit/49c8e221e1007380a476092b24d8c8b2a37e0676)
- Correctifs qualité. [`2515f48`](https://github.com/Dannebicque/intranetV3/commit/2515f486f834e31126756825cde366bf3914f6d4)
- Mise à jour 4.4 + suppression deprecated [`13255e9`](https://github.com/Dannebicque/intranetV3/commit/13255e9e8edfdfd54b29e9d64d14949c02037c93)
- Correctif enquête [`28eaff9`](https://github.com/Dannebicque/intranetV3/commit/28eaff907c6107abf1e56d770de54be96cd21c40)
- Bug affichage EDT depuis Celcat [`1f9cd17`](https://github.com/Dannebicque/intranetV3/commit/1f9cd17870d8cc2fb926b38b9caacdc4f4643e60)
- Bug demande de rattrapage en etudiant [`5d21576`](https://github.com/Dannebicque/intranetV3/commit/5d215761aeec1f0c9c09d5bc27439ffc3b689ef3)
- Correctifs gestion des documents [`2dcdaeb`](https://github.com/Dannebicque/intranetV3/commit/2dcdaebdbccfd922b25030fb63b84b8199e5d269)
- Partie qualité [`66df480`](https://github.com/Dannebicque/intranetV3/commit/66df480a46710862dc61b45f4409f735636a8031)
- CAS + Double authentification [`ea9f09d`](https://github.com/Dannebicque/intranetV3/commit/ea9f09d38ad1d258cf608c048f7f9f462ace9780)
- CAS + Double authentification [`9ad8a3b`](https://github.com/Dannebicque/intranetV3/commit/9ad8a3b1e246472b5e1e7f4e5c437a9aa926e154)
- CAS + Double authentification [`9592227`](https://github.com/Dannebicque/intranetV3/commit/9592227ef35a4a3f71f48b60569cff09865136d3)
- CAS + Double authentification [`f63bae5`](https://github.com/Dannebicque/intranetV3/commit/f63bae59163996888b008234d7995a99160a71b5)
- CAS + Double authentification [`095562a`](https://github.com/Dannebicque/intranetV3/commit/095562a70e3a6240f24cfe77075270673ba9eaa5)
- Update support [`1c82542`](https://github.com/Dannebicque/intranetV3/commit/1c8254260337d1c2132daedc4bc263e08be9c6c1)
- LDAP/Groupes [`e90c4ae`](https://github.com/Dannebicque/intranetV3/commit/e90c4aec505b9ec0a072afbb6e725c4d1467ffe0)
- LDAP/Groupes [`7357fdb`](https://github.com/Dannebicque/intranetV3/commit/7357fdb58d6e9e5379057d70f5b27e9b255cd1c0)
- LDAP/Groupes [`3b27e95`](https://github.com/Dannebicque/intranetV3/commit/3b27e95a8c54a8584c1e77b6a61d5038db917efd)
- LDAP/Groupes [`2c1c74e`](https://github.com/Dannebicque/intranetV3/commit/2c1c74ebf8abb0ffa80144a8e1c432c11dcfd2d0)
- LDAP [`18dfdf4`](https://github.com/Dannebicque/intranetV3/commit/18dfdf4cfb4967b316abb0634d60b08b7bc65e13)
- Apogee. [`51fd03f`](https://github.com/Dannebicque/intranetV3/commit/51fd03feaa958ca6a8b613d4abf1e7f37bf258ce)
- Apogee. [`9d79d80`](https://github.com/Dannebicque/intranetV3/commit/9d79d806da13b7a7f6c0af2d784872e355991611)
- Correctifs qualité. [`2b12af7`](https://github.com/Dannebicque/intranetV3/commit/2b12af7cbcde7db821dbc29b6db36a88925e2467)
- Added shebang to deploy script [`2412e09`](https://github.com/Dannebicque/intranetV3/commit/2412e094a7c8e945ec00eecad4c48cdda1908a42)
- CAS + Double authentification [`74c75dc`](https://github.com/Dannebicque/intranetV3/commit/74c75dcc82bceb914435e79a0540f48283dbe040)
- LDAP/Groupes [`bc9576a`](https://github.com/Dannebicque/intranetV3/commit/bc9576a89e4c89d2b5a131175fe24a99ba06e5b6)
- Apogee. [`5b3b7e8`](https://github.com/Dannebicque/intranetV3/commit/5b3b7e811d3455109ed0a07cceefa30c1e8a94fe)
- Traitement des todos [`e61d71b`](https://github.com/Dannebicque/intranetV3/commit/e61d71b0c5d0d1ef1c5a8dd3bb61455834149603)
- Fixture User [`bf3e282`](https://github.com/Dannebicque/intranetV3/commit/bf3e282b1390491e39e8c66565a76394bcc1e89d)
- Fixture User [`343750a`](https://github.com/Dannebicque/intranetV3/commit/343750af6029b2bd790608e1f50ca45924fcd6fe)
- Fixture User [`b4e55ae`](https://github.com/Dannebicque/intranetV3/commit/b4e55aec0f41f8e3c4440905122e0e828406681f)
